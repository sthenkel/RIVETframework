#!/bin/python
import os,re,glob

path = os.environ['PWD']
Output_merge = "Output_merged.root"

inputDir = ""

#inputDirectoriesContainingFiles = glob.glob(str(inputDir)+'*_Rivet/')
inputDirectoriesContainingFiles = glob.glob(str(inputDir)+'user.sthenkel.Boo13TeVwXsec2.410007.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_allhad.EVNT.e4135_Rivet/')

for item in inputDirectoriesContainingFiles:
    print 'Merging root files in ' + item
    if os.path.isfile(path+"/"+item+"/"+Output_merge):
        print 'WARNING: Already merged the root files ->  will delete '+str(Output_merge)
        os.system("rm "+path+"/"+item+"/"+Output_merge)

    files = glob.glob(str(item)+'*')
    command = "hadd "+Output_merge+" "
    os.chdir(path+"/"+item)
    for rootFile in files:
        command += path+"/"+rootFile + " "
    print '####################################'
    print '####################################'
    print 'MERGING root files with : \n'+command
    os.system(  command  )
    del command
    os.chdir(path)

