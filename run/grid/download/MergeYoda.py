#!/bin/python
import os,re,glob

path = os.environ['PWD']
Output_merge = "Output_merged.yoda"

inputDir = ""

inputDirectoriesContainingFiles = glob.glob(str(inputDir)+'*_EXT0/')
#inputDirectoriesContainingFiles = glob.glob(str("user.pbutti.13TeVtopTag.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.EVNT.e3698_EXT0/"))

for item in inputDirectoriesContainingFiles:
    print 'Merging yoda files in ' + item
    if os.path.isfile(path+"/"+item+"/"+Output_merge):
        print 'WARNING: Already merged the yoda files ->  will delete '+str(Output_merge)
        os.system("rm "+path+"/"+item+"/"+Output_merge)

    files = glob.glob(str(item)+'*')
    command = "yodamerge -o "+Output_merge+" "
    os.chdir(path+"/"+item)
    for yodaFile in files:
        if "yoda.2" in yodaFile:
            print 'Replaces yoda extension'
            print '#######################'
            os.system("mv " + path+"/"+yodaFile + " "+path+"/"+str(yodaFile).rsplit(".2",1)[0])
            yodaFile = str(yodaFile).rsplit(".2",1)[0]
        command += path+"/"+yodaFile + " "
    print '####################################'
    print '####################################'
    print 'MERGING yoda files with : \n'+command
    os.system(  command  )
    del command
    os.chdir(path)

