#!/bin/python

import os, glob, re
path = os.environ['PWD']

Output_merge = "Output_merged.yoda"
inputFileDir = "../download/Boosted/16022016-BoostedSelection/"
#inputFileDir = "../download/Boosted/closureAmcAtNloHerwigpp/"
#inputFileDir = "../download/Boosted/closurePowhegPythia8/"

inputFiles = sorted(glob.glob(str(inputFileDir)+'*_EXT0/'))
#RegEx = re.compile( '(\w*)\.(\w*)\.(\w*)\.(\d*)\.(\w*)\.(\w*)/.(\w*).(\w*)' )
RegEx = re.compile( '(\w*)\.(\w*)\.(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)/.(\w*).(\w*)' )
toPlot = {}
plottingOptions="'LineWidth=0.01':'LineStyle=solid'"



#LineColor=gray
#ErrorBandOpacity=0.3
#DotSize=0.001
#Type=Scatter2D
#:'ErrorBands=1':'ErrorBandColor=lightgray'
linewidth=0.01
dotsize=0.15

for item in inputFiles:
    print item+Output_merge
    if os.path.exists(item+Output_merge):
        print 'merged file exists \n'
        # cuts inputFileDir
        Match = RegEx.match( str(item+Output_merge).rsplit(inputFileDir,1)[1])
        nom = str(Match.group(5).split('_')[0])
        syst = str(Match.group(5).split('_')[1])
        print 'nom > '+str(nom) + "  syst > :"+str(syst)
        if "Data" in nom:
            title = "Data"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=black':'PolyMarker=o*':'DotSize="+str(dotsize)+"'"
        if "PowhegPythiaFullSim" in nom:
            title = "POWHEG+PYTHIA-unf (FS)"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=red':'PolyMarker=pentagon*':'DotSize="+str(dotsize)+"'"
        if "PowhegPythiaAFII" in nom:
            title = "POWHEG+PYTHIA-unf (AFII)"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=red':'PolyMarker=pentagon':'DotSize="+str(dotsize)+"'"
        if "PowhegPythiaEvtGen" in nom:
            title = "POWHEG+PYTHIA"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor={[RGB]{114,142,203}}':'PolyMarker=o*':'DotSize="+str(dotsize)+"'"
        if ( "PowhegPythia" in nom and "P2012radHi" in syst ):
            title = "POWHEG+PYTHIA (radHi)"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=dashed':'LineColor={[RGB]{114,142,203}}':'PolyMarker=oplus':'DotSize="+str(dotsize)+"'"
        if ( "PowhegPythia" in nom and "P2012radLo" in syst ):
            title = "POWHEG+PYTHIA (radLo)"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=dashed':'LineColor={[RGB]{114,142,203}}':'PolyMarker=otimes':'DotSize="+str(dotsize)+"'"
        if "PowhegPythia8EvtGen" in nom:
            title = "POWHEG+PYTHIA8"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor={[RGB]{78,75,212}}':'PolyMarker=o*':'DotSize="+str(dotsize)+"'"
        if "PowhegPythia8Unf" in nom:
            title = "POWHEG+PYTHIA8-unf"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=red':'PolyMarker=pentagon*':'DotSize="+str(dotsize)+"'"
        if "PowhegHerwigpp" in nom:
            title = "POWHEG+HERWIG++"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor={[RGB]{271,177,124}}':'PolyMarker=+':'DotSize="+str(dotsize)+"'"
        if "PowhegHerwigppUnf" in nom:
            title = "POWHEG+HERWIG++-unf"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=red':'PolyMarker=pentagon*':'DotSize="+str(dotsize)+"'"
        if "Sherpa" in nom:
            title = "SHERPA"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid'"
            continue
        if "aMcAtNloHerwigpp" in nom:
            title="aMC@NLO+Herwig++"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor={[RGB]{271,177,124}}':'PolyMarker=triangle*':'DotSize="+str(dotsize)+"'"
        if "aMcAtNloHerwigppUnf" in nom:
            title="aMC@NLO+Herwig++-unf"
            plottingOptions="'LineWidth="+str(linewidth)+"':'LineStyle=solid':'LineColor=red':'PolyMarker=pentagon*':'DotSize="+str(dotsize)+"'"

        toPlot[str(item+Output_merge)] = "'Title="+title+" ("+Match.group(4)+")'"+":"+plottingOptions

    else:
        print 'WARNING: yoda file does not exist for '+item+' -> check if yoda files are already merged'
plot_command = 'rivet-mkhtml --mc-errs '
#for key in mapping:
for key in sorted(toPlot):
    print key
    plot_command += key+":"+toPlot[key]+" "
print 'command to plot : '+plot_command
os.system( plot_command )
