## BEGIN PLOT /MC_TTbar_TestSel/*
NormalizeToSum=1
#RatioPlotErrorBandColor={[rgb]{0.2,0.6,0.8}}
RatioPlotErrorBandColor=yellow
LegendXPos=0.57
LegendYPos=0.93
## END PLOT


## BEGIN PLOT /MC_TTbar_TestSel/njet_25
XLabel=Njets
YLabel=bin sum.
Title= N jets with pt$>$25 GeV
LogY=1
XMin=-0.5
XMax=10.5
RatioPlotYMin=0.43
RatioPlotYMax=1.47
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/bjet_pT
XLabel=b-jet pt[GeV]
YLabel=bin sum.
Title=b-jet pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/bjet_eta
XLabel=b-jet #eta
YLabel=bin sum.
Title= b-jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/bjet_phi
XLabel=b-jet #phi
YLabel=bin sum.
Title= b-jet #phi
LogY=0
XMin=0.
XMax=6.3
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.55
LegendYPos=0.35
Rebin=5
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/ljet_pT
XLabel=light-jet pt[GeV]
YLabel=bin sum.
Title=light-jet pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/ljet_eta
XLabel=light-jet #eta
YLabel=bin sum.
Title=light-jet #eta
LogY=0
XMin=-3.0
XMax=3.0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/ljet_phi
XLabel=light-jet #phi
YLabel=bin sum.
Title= light-jet #phi
LogY=0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.55
LegendYPos=0.35
XMin=0.
XMax=6.3
Rebin=5
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/jet_pT
XLabel=jet pt[GeV]
YLabel=bin sum.
Title=jet pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/jet_eta
XLabel=jet #eta
YLabel=bin sum.
Title= jet #eta
LogY=0
XMin=-3.0
XMax=3.0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/jet_phi
XLabel=jet #phi
YLabel=bin sum.
Title= jet #phi
LogY=0
XMin=0.
XMax=6.3
Rebin=5
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.55
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/bjet_1_pT
XLabel=leading b-jet pt[GeV]
YLabel=bin sum.
Title= leading b-jet pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/elec_N
XLabel=number of electrons
YLabel=bin sum.
Title=number of electrons
LogY=0
XMin=-0.5
XMax=3.5
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/mu_N
XLabel=number of muons
YLabel=bin sum.
Title=number of	muons
LogY=0
XMin=-0.5
XMax=3.5
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/elec_pT
XLabel=electron pt [GeV]
YLabel=bin sum.
Title= leading elec pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/elec_eta
XLabel=electron #eta
YLabel=bin sum.
Title= leading elec #eta
LogY=0
XMin=-3.0
XMax=3.0
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.30
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/elec_phi
XLabel=electron phi
YLabel=bin sum.
Title= leading elec phi
LogY=0
XMin=0.
XMax=6.3
Rebin=5
RatioPlotYMin=0.83
RatioPlotYMax=1.17
LegendXPos=0.30
LegendYPos=0.35
## END PLOT





























## BEGIN PLOT /MC_TTbar_TestSel/mu_pT
XLabel=muon pt [GeV]
YLabel=bin sum.
Title= leading mu pt
LogY=1
XMin=0
XMax=350
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/mu_eta
XLabel=muon #eta
YLabel=bin sum.
Title= leading mu #eta
LogY=0
XMin=-3.0
XMax=3.0
RatioPlotYMin=0.83
RatioPlotYMax=1.17
LegendXPos=0.35
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/mu_phi
XLabel=muon phi
YLabel=bin sum.
Title= leading mu phi
LogY=0
XMin=0.
XMax=6.3
Rebin=5
RatioPlotYMin=0.73
RatioPlotYMax=1.27
LegendXPos=0.35
LegendYPos=0.35
## END PLOT

## BEGIN PLOT /MC_TTbar_TestSel/MET
XLabel=MET [GeV]
YLabel=bin sum.
Title= MET
LogY=1
XMin=0
XMax=800
RatioPlotYMin=0.73
RatioPlotYMax=1.27
## END PLOT

