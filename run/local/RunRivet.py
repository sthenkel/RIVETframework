#include("GeneratorUtils/StdEvgenSetup.py")
theApp.EvtMax = 5000
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.MessageSvc.OutputLevel = INFO

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()

#SetPrintInterval=500

Xsec = 451.64568
InputFolder = "/afs/cern.ch/work/s/sthenkel/public/forRIVETTutorial/"
NumberOfFiles = 6

#InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/atlascerngroupdisk/phys-top/topreco/MCValidation/SamplesForRivetValidation_13TeV/LeptonPlusJets_mc15b/"
#NumberOfFiles = 30

OutputRoot  = "Output_test.root"
OutputYoda  = "Output_test.yoda"
FileList = os.listdir(InputFolder)
startfile = 0
for file in FileList[startfile:NumberOfFiles+startfile]:
#    if not "1.pool.root" in file:
#        continue
    inputFile = InputFolder+file
    print 'DEBUG :: : '+str(inputFile)
    if os.path.exists(inputFile):
        svcMgr.EventSelector.InputCollections.append(inputFile)

from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
nentries = af.fileinfos['nentries']
streamname = af.fileinfos['stream_names']
print 'DEBUG :: all info  >', af.fileinfos
print 'INFO :: nentries   > ', nentries
print 'INFO :: streamname > ', streamname

rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']

#rivet.Analyses += [ 'ATLAS_2014_I1304688']
#rivet.Analyses += [ 'ATLAS_2013_I1243871']
#rivet.Analyses += [ 'ATLAS_2014_I1304289']
rivet.Analyses += [ 'MC_TTbar_TestSel' ]


#Top-fiducial
#rivet.Analyses += [ 'ATLAS_2015_I1345452']



rivet.HistoFile = OutputYoda
#rivet.MCEventKey
rivet.CrossSection = Xsec
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]


#svcMgr += CfgMgr.AthenaEventLoopMgr(EventPrintoutInterval=SetPrintInterval)
