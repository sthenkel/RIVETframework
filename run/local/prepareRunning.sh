#!/bin/bash
#RIVETBASE is set in the quickSetup.sh in the first folder
if ([ "$#" != 1 ]); then
    echo "Usage: source prepareRunning.sh <RIVET Analysis> ";
    echo "Example: source prepareRunning.sh MC_TTbar_TestSel";
    kill -SIGINT $$;
  fi

echo "INFO :: Set global variables needed by RIVET ... "
export RIVET_REF_PATH=$PWD
export RIVET_DATA_PATH=$PWD
export RIVET_PLOT_PATH=$PWD

export RIVET_ANALYSIS_PATH=$PWD
echo "INFO :: Check if analysis was recongnized by RIVET ... "
rivet --show-analysis $1