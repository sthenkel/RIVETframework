#!/bin/python

import os, glob, re
path = os.environ['PWD']

Output_merge = "Output_merged.yoda"
inputFileDir = "../BatchJobOutput/jobs/8nh/"
inputFiles = glob.glob(str(inputFileDir)+'*_OUT/')
#RegEx = re.compile( '(\w*)\.(\w*)\.(\w*)\.(\d*)\.(\w*)\.(\w*)\.(\w*)/.(\w*).(\w*)' )
toPlot = {"../routines/ATLAS_2014_I1304289.yoda": "'Title=Data':'ErrorBars=1'"}
#plottingOptions="'LineWidth=0.06':'LineStyle=solid'"
plottingOptions="'PolyMarker=|':'LineStyle=dashed'"

for item in inputFiles:
    print '$$$$ > '+item+Output_merge
    if os.path.exists(item+Output_merge):
        print 'merged file exists \n'
        # cuts inputFileDir
        Match =  str(item+Output_merge).rsplit(inputFileDir,1)[1]
        print '############### > '+Match
        samplename = re.split(r"(^[^_]+_[^_]+)_",Match)[1]
        print '>>> > : '+samplename
#        toPlot[str(item+Output_merge)] = "'Title="+str(samplename)+"'"+":"+plottingOptions
        toPlot.update( { str(item+Output_merge):"'Title="+str(samplename)+"'"+":"+plottingOptions} )

    else:
        print 'WARNING: yoda file does not exist for '+item+' -> check if yoda files are already merged'
#plot_command = 'rivet-mkhtml --pdf --mc-errs --minion '
plot_command = 'rivet-mkhtml --mc-errs '

#for key in mapping:
for key in toPlot:
    plot_command += " "+key+":"+toPlot[key]+" "
print 'command to plot : '+plot_command
os.system( plot_command )

