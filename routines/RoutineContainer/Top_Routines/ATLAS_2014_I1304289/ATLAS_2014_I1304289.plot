# BEGIN PLOT /ATLAS_2014_I1304289/d0[1234]-x01-y01
GofLegend=0
LegendYPos=0.90
LegendXPos=0.45
DataTitle=\shortstack[l]{ATLAS Data, \\ Phys.Rev.D90 (2014) 072004} 
# END PLOT


# BEGIN PLOT /ATLAS_2014_I1304289/d01-x01-y01
Title=Transverse momenta of parton-level top quarks
XLabel=$\mathrm{p}_{\mathrm{T}}^t$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_{\mathrm{T}}^t$ [GeV$^{-1}$]
LegendXPos=0.46
GofLegend=0
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d02-x01-y01
Title=Invariant mass of parton-level $t\bar{t}$ system
XLabel=$\mathrm{m}_{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{m}_{t\bar{t}}$ [GeV$^{-1}$]
GofLegend=0
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d03-x01-y01
Title=Transverse momentum of parton-level $t\bar{t}$ system
XLabel=$\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma\, d\sigma / d\mathrm{p}_\mathrm{T}^{t\bar{t}}$ [GeV$^{-1}$]
GofLegend=0
#RatioPlotMode=mcdata
# END PLOT

# BEGIN PLOT /ATLAS_2014_I1304289/d04-x01-y01
Title=Rapidity of parton-level $t\bar{t}$ system
XLabel=$|y_{t\bar{t}}|$
YLabel=$1/\sigma\,d\sigma / d|y_{t\bar{t}}|$
GofLegend=0
RatioPlotYMin=0.9
RatioPlotYMax=1.15
#RatioPlotMode=mcdata
# END PLOT
