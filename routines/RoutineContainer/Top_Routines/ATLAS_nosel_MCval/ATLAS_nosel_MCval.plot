## BEGIN PLOT /ATLAS_nosel_MCval/*
NormalizeToSum=1
RatioPlotErrorBandColor={[rgb]{0.2,0.6,0.8}}
LegendXPos=0.75
LegendYPos=0.90
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_10
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$10 GeV
LogY=1
XMin=0
XMax=20
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_25
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$25 GeV
LogY=1
XMin=0
XMax=12
RatioPlotYMin=0.43
RatioPlotYMax=2.47
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_40
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$40 GeV
LogY=1
XMin=0
XMax=7
RatioPlotYMin=0.43
RatioPlotYMax=1.67
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_60
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$60 GeV
LogY=1
XMin=0
XMax=6
RatioPlotYMin=0.43
RatioPlotYMax=1.57
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_80
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$80 GeV
LogY=1
XMin=0
XMax=6
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_100
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$100 GeV
LogY=1
XMin=0
XMax=4
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/njet_160
XLabel=Jet multiplicity
YLabel=bin sum.
Title=N jets with pt$>$160 GeV
LogY=1
XMin=0
XMax=3
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_1_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=leading jet pt
LogY=1
XMin=20
XMax=260
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=10
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_1_eta
XLabel=#eta
YLabel=bin sum.
Title=leading jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_2_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=2nd leading jet pt
LogY=1
XMin=20
XMax=150
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=5
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_2_eta
XLabel=#eta
YLabel=bin sum.
Title=2nd leading jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_3_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=3rd leading jet pt
LogY=1
XMin=20
XMax=100
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=3
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_3_eta
XLabel=#eta
YLabel=bin sum.
Title=3rd leading jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_4_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=4th leading jet pt
LogY=1
XMin=20
XMax=80
RatioPlotYMin=0.63
RatioPlotYMax=1.37
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_4_eta
XLabel=#eta
YLabel=bin sum.
Title=4th leading jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_5_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=5th leading jet pt
LogY=1
XMin=20
XMax=80
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=4
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/jet_5_eta
XLabel=#eta
YLabel=bin sum.
Title=5th leading jet #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=4
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/elec_1_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=leading elec pt
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/elec_1_eta
XLabel=#eta
YLabel=bin sum.
Title=leading elec #eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/elec_1_phi
XLabel=#phi
YLabel=bin sum.
Title=leading elec phi
LogY=0
XMin=0.
XMax=6.3
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/mu_1_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=leading mu pt
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/mu_1_eta
XLabel=#eta
YLabel=bin sum.
Title=leading mu eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/mu_1_phi
XLabel=#phi
YLabel=bin sum.
Title=leading mu phi
LogY=0
XMin=0.
XMax=6.3
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/gamma_1_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=leading mu pt
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/gamma_1_eta
XLabel=#eta
YLabel=bin sum.
Title=leading mu eta
LogY=0
XMin=-2.4
XMax=2.4
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/gamma_1_phi
XLabel=#phi
YLabel=bin sum.
Title=leading mu phi
LogY=0
XMin=0.
XMax=6.3
RatioPlotYMin=0.83
RatioPlotYMax=1.17
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/MET
XLabel=[GeV]
YLabel=bin sum.
Title=MET
LogY=1
XMin=20
XMax=160
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=8
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/charged_N
XLabel=N (pt$>$0.25 GeV)
YLabel=bin sum.
Title=charged particles N
LogY=1
XMin=0.0
XMax=350.0
RatioPlotYMin=0.07
RatioPlotYMax=1.47
Rebin=1
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/charged_eta
XLabel=#eta (pt$>$0.25 GeV)
YLabel=bin sum.
Title=charged particles #eta
LogY=0
XMin=-4.9
XMax=4.9
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=20
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/charged_phi
XLabel=#phi (pt$>$0.25 GeV)
YLabel=bin sum.
Title=charged particles #phi
LogY=0
XMin=0.0
XMax=6.3
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=20
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/charged_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=charged particles pt
LogY=1
XMin=0
XMax=80
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=2
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/elec_N
XLabel=Electron multiplicity
YLabel=bin sum.
Title=N electrons
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/mu_N
XLabel=Muon multiplicity
YLabel=bin sum.
Title=N muons
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/tau_N
XLabel=Tau multiplicity
YLabel=bin sum.
Title=N taus
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT

## BEGIN PLOT /ATLAS_nosel_MCval/gamma_N
XLabel=Tau multiplicity
YLabel=bin sum.
Title=N gamma
LogY=1
XMin=20
XMax=120
RatioPlotYMin=0.73
RatioPlotYMax=1.27
Rebin=6
## END PLOT