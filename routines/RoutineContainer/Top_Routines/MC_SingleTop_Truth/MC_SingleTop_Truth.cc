// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"

#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/Sphericity.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Math/LorentzTrans.hh"

// Vetoed Jet inputs
#include "Rivet/Projections/VetoedFinalState.hh"

// event record specific
#include "Rivet/Particle.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"

#include "HepMC/GenEvent.h"

#include <math.h>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;

namespace patch
{
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

namespace Rivet {

  class MC_SingleTop_Truth : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    MC_SingleTop_Truth()
      : Analysis("MC_SingleTop_Truth")
    {
      setNeedsCrossSection(false);
    }
  public:

    //=====================================================================
    // Set up projections and book histograms
    //=====================================================================
    void init() {

      // FinalState projector to select all particles in |eta| < 4.5
      const FinalState fs(-4.5, 4.5);
      addProjection(fs, "fs");

      // projection to find the electrons (selection on the final state projector)
      IdentifiedFinalState electrons(-2.47, +2.47, 25.0*GeV);
      electrons.acceptIdPair(PID::ELECTRON);
      addProjection(electrons, "electrons");

      // projection to find the muons (selection on the final state projector)
      IdentifiedFinalState muons(-2.50, +2.50, 25.0*GeV);
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "muons");

      // projection to find the taus (selection on the final state projector)
      UnstableFinalState taus(-2.50, +2.50, 25.0*GeV);
      addProjection(taus, "taus");

      // projection to find the neutrinos (selection on the final state projector)
      //      IdentifiedFinalState neutrinos(-4.5, 4.5, 25.0*GeV);
     
      // test A.Knue:
      IdentifiedFinalState neutrinos(-4.5, 4.5, 0.0*GeV);       
      neutrinos.acceptNeutrinos();
      addProjection(neutrinos, "neutrinos");

      // FinalState used as input for jet-finding (including everything except muons and neutrinos)
      VetoedFinalState jet_input(fs);
      jet_input.vetoNeutrinos();
      jet_input.addVetoPairId(PID::MUON);
      addProjection(jet_input, "jet_input");
      FastJets jets(jet_input, FastJets::ANTIKT, 0.4);
      addProjection(jets, "jets");

      // ChargedFinalState (charged final particles)
      ChargedFinalState cfs(-4.5, 4.5, 5.0*GeV);
      addProjection(cfs, "CFS");

      // ChargedLeptons: input for the jets including muons and neutrinos
      // A FinalState used to select particles within |eta| < 4.5 and with pT
      // > 30 GeV (if needed), out of which the ChargedLeptons projection picks only the
      // electrons and muons, to be accessed later as "LFS".
      // ChargedLeptons lfs(FinalState(-4.5, 4.5, 25.0*GeV));
      // addProjection(lfs, "LFS");

      // book histograms
      BookHistograms();
    }

    //=====================================================================
    // Perform the per-event analysis
    //=====================================================================
    void analyze(const Event& event) {

      // bool _debug=false;
      _weight = event.weight();
      _h_weight->fill(_weight);
      MSG_DEBUG("mc weight: " << _weight);

      // fill charged particles
      const ChargedFinalState& ChargedParticleCollection = applyProjection<ChargedFinalState>(event, "CFS");
      fillPropertiesFromCollection(ChargedParticleCollection);
      
      // fill electron collection
      const FinalState& ElectronCollection = applyProjection<FinalState>(event, "electrons");
      vector<Particle> good_electrons = fillPropertiesFromCollection(ElectronCollection, _h_electrons);
      // PrintFourMomentum(good_electrons);

      // fill muon collection
      const FinalState& MuonCollection = applyProjection<FinalState>(event, "muons");
      vector<Particle> good_muons = fillPropertiesFromCollection(MuonCollection, _h_muons);
      // PrintFourMomentum(good_muons);

      // merge electron and muon collection
      vector<Particle> good_leptons = good_electrons + good_muons;
      // PrintFourMomentum(good_leptons);

      // fill neutrino collection
      const FinalState& NeutrinoCollection = applyProjection<FinalState>(event, "neutrinos");
      vector<Particle> good_neutrinos = fillPropertiesFromCollection(NeutrinoCollection, _h_neutrinos);
      // PrintFourMomentum(good_neutrinos);

      // fill leading lepton properties
      fillLeadingParticleProperties(good_electrons, _h_electron_1);
      fillLeadingParticleProperties(good_muons, _h_muon_1);
      fillLeadingParticleProperties(good_neutrinos, _h_neutrino_1);

      // -----------------------------------------------------------------------------------------------

      // fill tau collection (pdg id)
      const UnstableFinalState& TauCollection = applyProjection<UnstableFinalState>(event, "taus");
      vector<Particle> good_taus = fillPropertiesFromCollection(TauCollection, _h_taus, 15);
      // PrintFourMomentum(good_taus);

      // -----------------------------------------------------------------------------------------------

      // fill MET
      FourMomentum _p_met(0., 0., 0., 0.);
      foreach(const Particle& _p, NeutrinoCollection.particlesByPt()) { _p_met = _p_met + _p.momentum(); }
      _h_MET.at(0)->fill(_p_met.pT()/GeV, _weight);
      _h_MET.at(1)->fill(_p_met.eta(), _weight);
      _h_MET.at(2)->fill(_p_met.phi(), _weight);
      // MSG_INFO(_p_met.pT()/GeV << "\t" << _p_met.phi());

      // -----------------------------------------------------------------------------------------------
      
      // fill jets collection (eta cut, pt cut)
      const FastJets& JetCollection = applyProjection<FastJets>(event, "jets");

      // fill all jets with pT > 7 GeV (ATLAS standard jet collection) (no histos are filled)
      Jets all_jets = fillPropertiesFromCollection(JetCollection, 5.0, 7.0);
      // PrintFourMomentum("jet", all_jets);
      
      // fill good jets (|eta|<2.5, pT > 25.0 GeV) (h_jets are filled)
      Jets good_jets = fillPropertiesFromCollection(JetCollection, good_electrons, 4.5, 25.0);
      // PrintFourMomentum("good jet", good_jets);

      // fill B-hadrons collection (pt cut)
      vector<HepMC::GenParticle*> B_hadrons = fillBHadrons(event, 4.5, 5.0);
      // PrintFourMomentum(B_hadrons);

      // fill ligh-jets (select b-jets as those containing a b-hadron) (deltaR cut)
      std::vector<Jets> good_jets_type = fillJetsFlavours(good_jets, all_jets, B_hadrons, 0.3);
      // PrintFourMomentum("good light jet", good_jets_type[0]);
      // PrintFourMomentum("good b-jet", good_jets_type[1]);

      // -----------------------------------------------------------------------------------------------

      // veto event if there are neither leptons nor neutrinos
      if (good_leptons.size()!=1 || good_neutrinos.size()!=1) { vetoEvent; }

      // PrintFourMomentum(good_leptons);
      // PrintFourMomentum(good_neutrinos);
      // PrintFourMomentum(good_taus);
      // PrintFourMomentum("standard jet", all_jets);
      // PrintFourMomentum("good jet", good_jets);
      // PrintFourMomentum("good light jet", good_jets_type[0]);
      // PrintFourMomentum("good b-jet", good_jets_type[1]);
      // PrintFourMomentum(B_hadrons);

      // fill W boson
      Particle _W_boson = fillWboson(good_leptons, good_neutrinos);   
      // MSG_INFO(good_leptons.size() << "\t" << good_neutrinos.size() << "\t" << good_jets_type[1].size());
      // PrintFourMomentum(0, _W_boson);

      // if(fabs(_W_boson.mass() - 80.399) > 30.0){ vetoEvent; }

      // veto event if there are no good b-jets
      if (good_jets_type[1].size()==0) { vetoEvent; }

      // fill top quark
      Particle _top_quark = fillTopQuark(_W_boson, good_jets_type[1]);
      // PrintFourMomentum(0, _top_quark);
      // MSG_INFO("\t");

      // veto event if there are neither W boson or top quark
      // if (_W_boson==0 || _top_quark==0) { vetoEvent; }

      // veto event if there are no good non b-jets
      if (good_jets_type[0].size()==0) { vetoEvent; }

      /*
      int index = 0;
      foreach(const Jet& j, good_jets_type[0]) {
	if (fabs(j.momentum().eta()) >= 2) {
	  PrintFourMomentum(index, "spectator jet", j.momentum());
	  index++;
	}
      }
      */

      // select the spectator quark candidate
      _spectjet_from_top_quark = (good_jets_type[0])[0];

      // get here the 4-momenta for the calc. of cos theta star
      FillAngularDistributions(_top_quark.momentum(), _W_boson.momentum());

      //MSG_INFO("");
    } // end of analyze()


    //=====================================================================
    // FillAngularDistributions
    //=====================================================================
    void FillAngularDistributions(FourMomentum top, FourMomentum W) {
      // need to go into the W rest frame here

      // PrintFourMomentum(0, "spectator jet", _spectjet_from_top_quark);
      // PrintFourMomentum(0, "top quark", top);
      // PrintFourMomentum(0, "W boson",   W);
      // PrintFourMomentum(0, "lepton from W boson", _lepton_from_W_boson);
      // PrintFourMomentum(0, "b-jet from W boson", _bjet_from_top_quark);

      /*
      const Vector3 boostVec(W.px(), W.py(), W.pz());

      LorentzTransform boostTrafo; 
      boostTrafo.setBoost(-W.boostVector());

      const FourMomentum boostW   = boostTrafo.transform(W);
      //PrintFourMomentum(0,   "boosteW",   boostW);
      const FourMomentum boostTop = boostTrafo.transform(top);
      //PrintFourMomentum(0,  "BoostedTop", boostTop);
      const FourMomentum boostLep = boostTrafo.transform(lep); 
      //PrintFourMomentum(0, "BoostedLep",  boostLep);

      double cosThetaStar = cos(boostLep.angle(-boostTop));
      // MSG_INFO("cos(theta*): " << cosThetaStar);
      */

      // boost to top rest frame
      LorentzTransform boostTraf_Top;
      boostTraf_Top.setBoost(-top.boostVector());
      const FourMomentum SpectatorJetBoostedToTopCM = boostTraf_Top.transform(_spectjet_from_top_quark); // boost spectator jet to top rest frame
      const FourMomentum WBoostedToTopCM = boostTraf_Top.transform(W); // boost W boson to top rest frame
      const FourMomentum LeptonBoostedToTopCM = boostTraf_Top.transform(_lepton_from_W_boson); // boost lepton to top rest frame
      const FourMomentum bBoostedToTopCM = boostTraf_Top.transform(_bjet_from_top_quark); // boost b-jet to top rest frame

      // boost to W rest frame
      LorentzTransform boostTraf_W;
      boostTraf_W.setBoost(-WBoostedToTopCM.boostVector());
      const FourMomentum bJetBoostedToWCM = boostTraf_W.transform(bBoostedToTopCM); // boost b-jet from Top rest frame to W rest frame
      const FourMomentum LeptonBoostedToWCM = boostTraf_W.transform(LeptonBoostedToTopCM); // boost lepton from Top rest frame to W rest frame

      // angular distribution to extract the W boson helicity
      double cosTheta_S = cos(WBoostedToTopCM.angle(LeptonBoostedToWCM));
      // MSG_INFO("cos(theta*): " << cosTheta_S);
      _h_cosTheta_S->fill(cosTheta_S, _weight);

      // define the Normal (N) and Transverse (T) direction
      // N = s x q (q ... W momentum direction)
      // T = q x N (s ... spectator jet mom direction)
      const Vector3 S = SpectatorJetBoostedToTopCM.vector3();
      const Vector3 q = WBoostedToTopCM.vector3();
      const Vector3 leptonInTopCM = LeptonBoostedToTopCM.vector3();
      const Vector3 l = LeptonBoostedToWCM.vector3();
      const Vector3 N = S.cross(q);
      const Vector3 T = q.cross(N);
      double cosTheta_N = cos(l.angle(N));
      double cosTheta_T = cos(l.angle(T));
      // MSG_INFO("cos(theta)_N: " << cosTheta_N);
      // MSG_INFO("cos(theta)_T: " << cosTheta_T);
      _h_cosTheta_N->fill(cosTheta_N, _weight);
      _h_cosTheta_T->fill(cosTheta_T, _weight);
      
      // phi (from the two/three angle analyses)
      double Phi_S = atan2(cosTheta_N, -cosTheta_T);
      // MSG_INFO("Phi_S: " << Phi_S << " rad");
      while (Phi_S<0.) { Phi_S += 2*pi; }
      while (Phi_S>twopi) { Phi_S -= 2*pi; }
      Phi_S = Phi_S/pi;
      // MSG_INFO("Phi_S: " << Phi_S << " pi");
      _h_Phi_S->fill(Phi_S, _weight);

      // cos(theta) (from the two/three angle analyses)
      double cosTheta = cos(S.angle(q));
      // MSG_INFO("cos(theta): " << cosTheta);
      _h_cosTheta->fill(cosTheta, _weight);
      
      // angular distribution to extract the spin analyser times the top polarization, i.e. alpha_X�P.
      // If one considers the lepton in the top rest frame as the "spin analyser" then alpha_X=1
      double cosTheta_X = cos(S.angle(leptonInTopCM));
      // MSG_INFO("cos(theta)_X: " << cosTheta_X);
      _h_cosTheta_X->fill(cosTheta_X, _weight);
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(string jetname, Jets _jets) {
      int index = 0;
      foreach(const Jet& j, _jets) {
	PrintFourMomentum(index, jetname, j.momentum());
	index++;
      }
    }
    
    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(vector<HepMC::GenParticle*> _partvect) {
      int index = 0;
      foreach (HepMC::GenParticle* _p, _partvect) {
	PrintFourMomentum(index, Particle(*_p));
	index++;
      }
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(vector<Particle> _partvect) {
      int index = 0;
      foreach (Particle _p, _partvect) { 
	PrintFourMomentum(index, _p);
	index++;
      }
    }

    //=====================================================================
    // Mass
    //=====================================================================
    float Mass(Particle _p) { return Mass(_p.momentum()); }

    //=====================================================================
    // Mass
    //=====================================================================
    float Mass(FourMomentum FourMom) {
      float mass = sqrt(FourMom.mass2())/GeV;
      if (Rivet::isZero(mass, 1E-9)) { mass = 0.; }
      if (std::isnan(mass)) { mass = 0.; }
      return mass;
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(int index, Particle _p) {

      string partname = "";
      if (fabs(_p.pdgId())==11) { partname = "electron"; }
      else if (fabs(_p.pdgId())==13) { partname = "muon"; }
      else if (fabs(_p.pdgId())==15) { partname = "tau"; }
      else if (fabs(_p.pdgId())==12 || fabs(_p.pdgId())==14 || fabs(_p.pdgId())==16) { partname = "neutrino"; }
      else if (fabs(_p.pdgId())==24) { partname = "W boson"; }
      else if (fabs(_p.pdgId())==5) { partname = "b quark"; }
      else if (fabs(_p.pdgId())==6) { partname = "top quark"; }
      else { partname = patch::to_string(_p.pdgId()); }

      // don't use _p.momentum().mass(), use this instead:      
      PrintFourMomentum(index, partname, _p.momentum());
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(int index, string partname, FourMomentum FourMom) {
      MSG_INFO(index << ". (" << partname << ") " <<
 	       "pT = " << FourMom.pT()/GeV << " GeV, " <<
 	       "m = " << Mass(FourMom) << " GeV, " <<
	       "E = " << FourMom.E()/GeV << " GeV, " <<
 	       "ET = " << FourMom.Et()/GeV << " GeV, " <<
	       "p = [" << FourMom.px()/GeV << ", " << FourMom.py()/GeV << ", " << FourMom.pz()/GeV << "] GeV, " <<
 	       "\u03B7 = " << FourMom.eta() << ", "
 	       "\u0278 = " << FourMom.phi()<< " rad");
    }

    //=====================================================================
    // FillFourMomentum
    //=====================================================================
    void FillFourMomentum(Particle _p, vector<Histo1DPtr> _histo, int index=1, bool fillMass=false) {
      _histo.at(index)->fill(_p.momentum().E()/GeV, _weight);
      _histo.at(index+1)->fill(_p.momentum().Et()/GeV, _weight);
      _histo.at(index+2)->fill(_p.momentum().pT()/GeV, _weight);
      _histo.at(index+3)->fill(_p.momentum().pz()/GeV, _weight);
      _histo.at(index+4)->fill(_p.momentum().eta(), _weight);
      _histo.at(index+5)->fill(_p.momentum().phi(), _weight);
      if (fillMass) { _histo.at(index+6)->fill(Mass(_p), _weight); }
    }

    //=====================================================================
    // FillFourMomentum
    //=====================================================================
    void FillFourMomentum(const Jet& _j, vector<Histo1DPtr> _histo, int index=1, bool fillMass=false) {
      _histo.at(index)->fill(_j.momentum().E()/GeV, _weight);
      _histo.at(index+1)->fill(_j.momentum().Et()/GeV, _weight);
      _histo.at(index+2)->fill(_j.momentum().pT()/GeV, _weight);
      _histo.at(index+3)->fill(_j.momentum().pz()/GeV, _weight);
      _histo.at(index+4)->fill(_j.momentum().eta(), _weight);
      _histo.at(index+5)->fill(_j.momentum().phi(), _weight);
      if (fillMass) {
	// MSG_INFO("jet mass = " << _j.momentum().mass() << " GeV");
	_histo.at(index+6)->fill(Mass(_j.momentum()), _weight);
      }
    }

    //=====================================================================
    // fill properties from Collection - ChargedFinalState
    //=====================================================================
    vector<FourMomentum> fillPropertiesFromCollection(const ChargedFinalState& Collection, float etaCut=5.0, float ptCut=0.0) {
      vector<FourMomentum> _charged;
      foreach(const Particle& _p, Collection.particlesByPt()) {
	FourMomentum FourMom(0., 0., 0., 0.);
	FourMom = FourMom + _p.momentum();
	if (FourMom.pT() > ptCut && fabs(FourMom.eta()) < etaCut) {
	  _charged.push_back(FourMom);
	  _h_charged.at(1)->fill(FourMom.E()/GeV, _weight);
	  _h_charged.at(2)->fill(FourMom.Et()/GeV, _weight);
	  _h_charged.at(3)->fill(FourMom.pT()/GeV, _weight);
	  _h_charged.at(4)->fill(FourMom.pz()/GeV, _weight);
	  _h_charged.at(5)->fill(FourMom.eta(), _weight);
	  _h_charged.at(6)->fill(FourMom.phi(), _weight);
	  _h_charged.at(7)->fill(Mass(FourMom), _weight);
	  // _h_charged.at(8)->fill(sqrt(FourMom.mass2()+pow(FourMom.px(),2)+pow(FourMom.py(),2)), _weight);
	}
      }
      MSG_DEBUG("Number of charged particles = " << _charged.size());
      _h_charged.at(0)->fill(Collection.size(),_weight);
      // _h_charged.at(0)->fill(_charged.size(), _weight);
      return _charged;
    }

    //=====================================================================
    // fill properties from Collection - FinalState
    //=====================================================================
    vector<Particle> fillPropertiesFromCollection(const FinalState& Collection, vector<Histo1DPtr> _histo, float etaCut=5.0, float ptCut=0.0) {
      vector<Particle> _partvect;
      foreach(const Particle& _p, Collection.particlesByPt()) {
	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {

	  // exclude the transition region for electrons
	  if (fabs(_p.pdgId())==11 && (fabs(_p.momentum().eta()) <= 1.52 && fabs(_p.momentum().eta()) >= 1.37)) { continue; }

	  _partvect.push_back(_p);
	  FillFourMomentum(_p, _histo);
	  // PrintFourMomentum(_partvect.size(), _p);
	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      return _partvect;
    }

    //=====================================================================
    // fill properties from Collection - UnstableFinalState
    //=====================================================================
    vector<Particle> fillPropertiesFromCollection(const UnstableFinalState& Collection, vector<Histo1DPtr> _histo, int pdfId, float etaCut=5.0, float ptCut=0.0) {
      vector<Particle> _partvect;
      foreach(const Particle& _p, Collection.particlesByPt()) {
	if(abs(_p.pdgId())!=pdfId) continue;
	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {
	  _partvect.push_back(_p);
	  FillFourMomentum(_p, _histo);
	  // PrintFourMomentum(_partvect.size(), _p);
	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      return _partvect;
    }

    //=====================================================================
    // fill properties of the leading particle - vector<Particle>
    //=====================================================================
    void fillLeadingParticleProperties(vector<Particle> _partvect, vector<Histo1DPtr> _histo) {
      MSG_DEBUG("Number of charged particles = " << _partvect.size());
      if (_partvect.size()>0) {
	FillFourMomentum(_partvect[0], _histo, 0);
	// PrintFourMomentum(0, _partvect[0]);
      }
    }
    
    //=====================================================================
    // fill properties from Collection - FastJets
    //=====================================================================
    Jets fillPropertiesFromCollection(const FastJets& Collection, float etaCut, float ptCut) {
      Jets _jets;
      foreach(const Jet& _jet, Collection.jetsByPt(ptCut*GeV)) {
      	if (fabs(_jet.eta()) >= etaCut) { continue; }
	_jets.push_back(_jet);
      }
      return _jets;
    }

    //=====================================================================
    // fill properties from Collection - FastJets
    //=====================================================================
    Jets fillPropertiesFromCollection(const FastJets& Collection, vector<Particle> _goodElectrons, float etaCut, float ptCut) {
      Jets _jets;
      foreach(const Jet& _jet, Collection.jetsByPt(ptCut*GeV)) {
      	if (fabs(_jet.eta()) >= etaCut) { continue; }

	bool isElectron = 0;
	foreach (Particle _e, _goodElectrons) {
	  const double elec_jet_dR = deltaR(_e.momentum(), _jet.momentum());
	  if (elec_jet_dR < 0.2) { isElectron = true; break; }
	}
	// Remove jets too close to an electron
	if (!isElectron) {
	  _jets.push_back(_jet);
	  FillFourMomentum(_jet, _h_jets, 1, true);
	}
      }
      _h_jets.at(0)->fill(_jets.size(), _weight);
      return _jets;
    }
   
    //=====================================================================
    // fill B-Hadrons
    //=====================================================================
    vector<HepMC::GenParticle*> fillBHadrons(const Event& event, float etaCut=4.5, float ptCut=5.0) {
      vector<HepMC::GenParticle*> B_hadrons;
      // Get the B-Hadrons with pT > ptCut GeV, to add to the final-state particles used for jet clustering.
      vector<HepMC::GenParticle*> allParticles = particles(event.genEvent());
      for (size_t i = 0; i < allParticles.size(); i++) {
        // GenParticle* _p = allParticles[i];
	HepMC::GenParticle* _p = allParticles.at(i);

        // If the particle is a B-hadron and has pT > ptCut GeV, add it to the B-hadrons vector
        if(PID::isHadron(_p->pdg_id()) && PID::hasBottom(_p->pdg_id()) && fabs(_p->momentum().eta()) < etaCut && _p->momentum().perp() > ptCut) {
          Particle B_hadron = Particle(*_p);
	  FillFourMomentum(B_hadron, _h_B_hadrons, 1, true);
	  _h_B_hadrons.at(8)->fill(sqrt(B_hadron.momentum().mass2()+pow(B_hadron.momentum().px(), 2)+pow(B_hadron.momentum().py(),2))/GeV, _weight);
          B_hadrons.push_back(_p);
        }
      }
      _h_B_hadrons.at(0)->fill(B_hadrons.size(), _weight);
      return B_hadrons;
    }

    //=====================================================================
    // fill jets flavours
    //=====================================================================
    std::vector<Jets> fillJetsFlavours(Jets _good_jets, Jets _all_jets, vector<HepMC::GenParticle*> _B_hadrons, float dRCut) {
      Jets _l_jets, _b_jets;

      int indexLJet = 0;
      int indexBJet = 0;
      foreach(const Jet& _j, _good_jets) {
	bool isbJet = false;
         foreach (HepMC::GenParticle* _b, _B_hadrons) {
           /// @todo Use direct momentum accessor / delta functions
           const FourMomentum hadron = _b->momentum();
           const double hadron_jet_dR = deltaR(_j.momentum(), hadron);
           if (hadron_jet_dR < dRCut) { isbJet = true; break; }
         }

         // Check if the given jet is overlapped to any other jet
	 bool isOverlapped = false;

	 foreach(const Jet& _k, _all_jets) {
           if (_j.momentum() == _k.momentum()) continue;
           double dRjj = deltaR(_j.momentum(), _k.momentum());
           if (dRjj < 0.8) { isOverlapped = true; break; }
	 }
         if (isbJet && !isOverlapped) {
	   _b_jets.push_back(_j);
	   FillFourMomentum(_j, _h_bjets, 1, true);
	   // PrintFourMomentum(indexBJet, "b-jet", _j.momentum());
	   indexBJet++;
	 }
	 else if (!isbJet && !isOverlapped) {
	   _l_jets.push_back(_j);
	   FillFourMomentum(_j, _h_ljets, 1, true);
	   // PrintFourMomentum(indexLJet, "light jet", _j.momentum());
	   indexLJet++;
	 }
      }
      // MSG_INFO(_l_jets.size() << " light jets selected");
      // MSG_INFO(_b_jets.size() << " b-jets selected");
      _h_ljets.at(0)->fill(_l_jets.size(), _weight);
      _h_bjets.at(0)->fill(_b_jets.size(), _weight);

      std::vector<Jets> _jetsVector; _jetsVector.clear();
      _jetsVector.push_back(_l_jets);
      _jetsVector.push_back(_b_jets);

      return _jetsVector;
    }

    //=====================================================================
    // fill W boson
    //=====================================================================
    Particle fillWboson(vector<Particle> _leptons,  vector<Particle> _neutrinos) {
      FourMomentum _lepton_selected(0, 0, 0, 0);
      FourMomentum _neutrino_selected(0, 0, 0, 0);
      FourMomentum _Wboson_selected(0, 0, 0, 0);

      int index = 0;
      float Wboson_mass = 80.399*GeV; // in GeV
      foreach (Particle _neutrino, _neutrinos) {
	foreach (Particle _lepton, _leptons) {

	  // MSG_ERROR("lepton (" << _lepton.pdgId() << ")" << PID::charge(_lepton.pdgId()));
	  // MSG_ERROR("neutrino (" << _neutrino.pdgId() << ")" << PID::charge(_neutrino.pdgId()));
	  
	  FourMomentum _lepton_candidate   = _lepton.momentum();
	  FourMomentum _neutrino_candidate = _neutrino.momentum();
	  FourMomentum _Wboson_candidate   = _lepton.momentum() + _neutrino.momentum();

	  // PrintFourMomentum(index, "lepton [candidate]", _lepton_candidate);
	  // PrintFourMomentum(index, "neutrino [candidate]", _neutrino_candidate);
	  // PrintFourMomentum(index, "W boson [candidate]", _Wboson_candidate);

	  if(index  == 0) {
	    _lepton_selected   = _lepton_candidate;
	    _neutrino_selected = _neutrino_candidate;
	    _Wboson_selected   = _Wboson_candidate;
	  }
	  else if(fabs(_Wboson_candidate.mass() - Wboson_mass) < fabs(_Wboson_selected.mass() - Wboson_mass)) {	    
	    _lepton_selected   = _lepton_candidate;
	    _neutrino_selected = _neutrino_candidate;
	    _Wboson_selected   = _Wboson_candidate;	    
	    // if(fabs(_Wboson_selected.mass() - 80.399) > 50.0){
	    //  PrintFourMomentum(index, "lepton [candidate]", _lepton_candidate);
	    //  PrintFourMomentum(index, "neutrino [candidate]", _neutrino_candidate);
	    //  PrintFourMomentum(index, "W boson [candidate]", _Wboson_candidate);
	    //  MSG_INFO("\t");
	    // }
	  }
	  index++;
	}
      }
      
      _lepton_from_W_boson = _lepton_selected; 
      // PrintFourMomentum(0, "lepton from W boson [selected]", _lepton_selected);
      // PrintFourMomentum(0, "lepton from W boson [selected]", _lepton_from_W_boson);

      Particle _Wboson(24, _Wboson_selected);
      // PrintFourMomentum(0, "W boson [selected]", _Wboson_selected);
      FillFourMomentum(_Wboson, _h_W, 0, true);

      // fill mt(W)
      _h_W.at(7)->fill(sqrt(2*_lepton_selected.pT()/GeV*_neutrino_selected.pT()/GeV*(1-cos(_lepton_selected.phi()-_neutrino_selected.phi()))), _weight);
      
      // if(fabs(_Wboson.mass()  - 80.399) > 50.0)
      // MSG_INFO(_Wboson.mass());

      return _Wboson;
    }

    //=====================================================================
    // fill top quark
    //=====================================================================
    Particle fillTopQuark(Particle _Wboson,  Jets _bjets) {
      FourMomentum _bjet_selected(0, 0, 0, 0);
      FourMomentum _topquark_selected(0, 0, 0, 0);
      
      int index = 0;
      float topquark_mass = 172.5; // in GeV
      foreach(const Jet& _bjet, _bjets) {

	FourMomentum _bjet_candidate = _bjet.momentum();
	FourMomentum _topquark_candidate = _Wboson.momentum() + _bjet.momentum();

	if(index == 0){
	  _topquark_selected = _topquark_candidate;
	  _bjet_selected = _bjet_candidate;
	}
	// PrintFourMomentum(index, "b-jet [candidate]", _bjet_candidate);
	// PrintFourMomentum(index, "top quark boson [candidate]", _topquark_candidate);

	// MSG_INFO(index << "\t" << fabs(_topquark_candidate.mass() - topquark_mass*GeV) << "\t" <<  fabs(_topquark_selected.mass() - topquark_mass*GeV));
	if (fabs(_topquark_candidate.mass() - topquark_mass*GeV) < fabs(_topquark_selected.mass() - topquark_mass*GeV)) {
	  _bjet_selected = _bjet_candidate;
	  _topquark_selected = _topquark_candidate;
	  // PrintFourMomentum(index, "top quark boson [selected]", _topquark_selected);     
	}
	index++;
      }
      
      // use the highest pt b-jet
      // _topquark_selected = _Wboson.momentum() + _bjets[0].momentum();

      _bjet_from_top_quark = _bjet_selected; 
      // PrintFourMomentum(0, "b-jet from top quark [selected]", _bjet_selected);

      Particle _topquark(6, _topquark_selected);
      // PrintFourMomentum(0, "top quark boson [selected]", _topquark_selected);
      FillFourMomentum(_topquark, _h_t, 0, true);
      _h_t.at(7)->fill(sqrt(2*_Wboson.momentum().pT()/GeV*_bjet_selected.pT()/GeV*(1-cos(_Wboson.momentum().phi()-_bjet_selected.phi()))), _weight);
      return _topquark;
    }

    //=====================================================================
    // fill N jets from Collection
    //=====================================================================
    void fillNjetsFromCollection(const FastJets& Collection, vector<HepMC::GenParticle*> _B_hadrons, int index, float ptCut) {
      Jets _jets, _ljets, _bjets;
      float etaCut = 2.5;
      foreach (const Jet& _jet, Collection.jetsByPt(ptCut*GeV)) {
      	if (fabs(_jet.eta()) < etaCut) {
      	  _jets.push_back(_jet);
	  
      	  // jet B-hadron matching
	  float _B_hadrons_ptCut = 25.0;
      	  foreach(HepMC::GenParticle* _b, _B_hadrons) {
	    if(_b->momentum().perp()*GeV < _B_hadrons_ptCut*GeV) { continue; }
      	    FourMomentum _hadronfm = _b->momentum();
      	    double _hadron_jet_dR = deltaR(_jet.momentum(), _hadronfm);
      	    if(_hadron_jet_dR < 0.3) { _bjets.push_back(_jet); }
      	    continue;
      	  }
      	  if (_B_hadrons.size()==0) { _ljets.push_back(_jet); }
      	}
      }
      // _h_Njets.at(index)->fill(_jets.size(),_weight);
      // _h_Nljets.at(index)->fill(_ljets.size(),_weight);
      // _h_Nbjets.at(index)->fill(_bjets.size(),_weight);
    }


    //=====================================================================
    // fill properties of the leading particle - FastJets
    //=====================================================================
    void fillLeadingParticleProperties(const FastJets& Collection, vector<Histo1DPtr> _histo, int leadership, float ptCut, float etaCut) {
      if (Collection.size() > 0) {
	FourMomentum FourMom(0., 0., 0., 0.);
	FourMom = Collection.jetsByPt(ptCut*GeV)[leadership].momentum();
	if (fabs(FourMom.eta()) < etaCut) {
	  _histo.at(0)->fill(FourMom.E()/GeV, _weight); 
	  _histo.at(1)->fill(FourMom.Et()/GeV, _weight);  
	  _histo.at(2)->fill(FourMom.pT()/GeV, _weight);  
	  _histo.at(3)->fill(FourMom.pz()/GeV, _weight);  
	  _histo.at(4)->fill(FourMom.eta(), _weight);
	  _histo.at(5)->fill(FourMom.phi(), _weight);
	}
      }
    }

    //=====================================================================
    // BookHistograms
    //=====================================================================
    void BookHistograms() {
      // mc weight
      _h_weight = bookHisto1D("weight", 20, -10.5, 9.5);

      // stable charged particles pT distribution
      _h_charged.push_back(bookHisto1D("charged_N", 31, 0, 30));
      _h_charged.push_back(bookHisto1D("charged_E", 50, 0, 250));
      _h_charged.push_back(bookHisto1D("charged_Et", 50, 0, 250));
      _h_charged.push_back(bookHisto1D("charged_pt", 50, 0, 250));
      _h_charged.push_back(bookHisto1D("charged_pz", 50, 0, 250));
      _h_charged.push_back(bookHisto1D("charged_eta", 40, -5.0, 5.0));
      _h_charged.push_back(bookHisto1D("charged_phi", 32, 0.0, twopi));
      _h_charged.push_back(bookHisto1D("charged_m", 50, 0, 250));
      // _h_charged.push_back(bookHisto1D("charged_mt", 50, 0, 250));

      // electrons
      _h_electrons.push_back(bookHisto1D("electrons_N", 6., 0., 5.));
      _h_electrons.push_back(bookHisto1D("electrons_E", 50, 0, 250));
      _h_electrons.push_back(bookHisto1D("electrons_Et", 50, 0, 250));
      _h_electrons.push_back(bookHisto1D("electrons_pt", 50, 0, 250));
      _h_electrons.push_back(bookHisto1D("electrons_pz", 50, 0, 250));
      _h_electrons.push_back(bookHisto1D("electrons_eta", 40, -5.0, 5.0));
      _h_electrons.push_back(bookHisto1D("electrons_phi", 32, 0.0, twopi));

      // muons
      _h_muons.push_back(bookHisto1D("muons_N", 6., 0., 5.));
      _h_muons.push_back(bookHisto1D("muons_E", 50, 0, 250));
      _h_muons.push_back(bookHisto1D("muons_Et", 50, 0, 250));
      _h_muons.push_back(bookHisto1D("muons_pt", 50, 0, 250));
      _h_muons.push_back(bookHisto1D("muons_pz", 50, 0, 250));
      _h_muons.push_back(bookHisto1D("muons_eta", 40, -5.0, 5.0));
      _h_muons.push_back(bookHisto1D("muons_phi", 32, 0.0, twopi));

      // neutrinos
      _h_neutrinos.push_back(bookHisto1D("neutrinos_N", 6., 0., 5.));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_E", 50, 0, 250));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_Et", 50, 0, 250));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_pt", 50, 0, 250));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_pz", 50, 0, 250));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_eta", 40, -5.0, 5.0));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_phi", 32, 0.0, twopi));

      // leading electron
      _h_electron_1.push_back(bookHisto1D("electron_1_E", 50, 0, 250));
      _h_electron_1.push_back(bookHisto1D("electron_1_Et", 50, 0, 250));
      _h_electron_1.push_back(bookHisto1D("electron_1_pt", 50, 0, 250));
      _h_electron_1.push_back(bookHisto1D("electron_1_pz", 50, 0, 250));
      _h_electron_1.push_back(bookHisto1D("electron_1_eta", 40, -5.0, 5.0));
      _h_electron_1.push_back(bookHisto1D("electron_1_phi", 32, 0.0, twopi));

      // leading muon
      _h_muon_1.push_back(bookHisto1D("muon_1_E", 50, 0, 250));
      _h_muon_1.push_back(bookHisto1D("muon_1_Et", 50, 0, 250));
      _h_muon_1.push_back(bookHisto1D("muon_1_pt", 50, 0, 250));
      _h_muon_1.push_back(bookHisto1D("muon_1_pz", 50, 0, 250));
      _h_muon_1.push_back(bookHisto1D("muon_1_eta", 40, -5.0, 5.0));
      _h_muon_1.push_back(bookHisto1D("muon_1_phi", 32, 0.0, twopi));

      // leading neutrino
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_E", 50, 0, 250));
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_Et", 50, 0, 250));
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_pt", 50, 0, 250));
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_pz", 50, 0, 250));
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_eta", 40, -5.0, 5.0));
      _h_neutrino_1.push_back(bookHisto1D("neutrino_1_phi", 32, 0.0, twopi));

      // taus
      _h_taus.push_back(bookHisto1D("taus_N", 6., 0., 5.));
      _h_taus.push_back(bookHisto1D("taus_E", 50, 0, 250));
      _h_taus.push_back(bookHisto1D("taus_Et", 50, 0, 250));
      _h_taus.push_back(bookHisto1D("taus_pt", 50, 0, 250));
      _h_taus.push_back(bookHisto1D("taus_pz", 50, 0, 250));
      _h_taus.push_back(bookHisto1D("taus_eta", 40, -5.0, 5.0));
      _h_taus.push_back(bookHisto1D("taus_phi", 32, 0.0, twopi));

      // MET
      _h_MET.push_back(bookHisto1D("MET", 50, 0, 250));
      _h_MET.push_back(bookHisto1D("MET_eta", 40, -5.0, 5.0));
      _h_MET.push_back(bookHisto1D("MET_phi", 32, 0.0, twopi));

      // "good" jets
      _h_jets.push_back(bookHisto1D("jets_N", 6, 0, 5));
      _h_jets.push_back(bookHisto1D("jets_E", 50, 0, 250));
      _h_jets.push_back(bookHisto1D("jets_Et", 50, 0, 250));
      _h_jets.push_back(bookHisto1D("jets_pt", 50, 0, 250));
      _h_jets.push_back(bookHisto1D("jets_pz", 50, 0, 250));
      _h_jets.push_back(bookHisto1D("jets_eta", 40, -5.0, 5.0));
      _h_jets.push_back(bookHisto1D("jets_phi", 32, 0.0, twopi));
      _h_jets.push_back(bookHisto1D("jets_m", 50, 0, 250));
      // _h_jets.push_back(bookHisto1D("jets_mt", 50, 0, 250));

      // B-hadrons
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_N", 31, 0, 30));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_E", 50, 0, 250));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_Et", 50, 0, 250));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_pt", 50, 0, 250));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_pz", 50, 0, 250));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_eta", 16, -5.0, 5.0));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_phi", 32, 0.0, twopi));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_m", 50, 0, 250));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_mt", 50, 0, 250));

      // light jets
      _h_ljets.push_back(bookHisto1D("ljets_N", 6, 0, 5));
      _h_ljets.push_back(bookHisto1D("ljets_E", 50, 0, 250));
      _h_ljets.push_back(bookHisto1D("ljets_Et", 50, 0, 250));
      _h_ljets.push_back(bookHisto1D("ljets_pt", 50, 0, 250));
      _h_ljets.push_back(bookHisto1D("ljets_pz", 50, 0, 250));
      _h_ljets.push_back(bookHisto1D("ljets_eta", 40, -5.0, 5.0));
      _h_ljets.push_back(bookHisto1D("ljets_phi", 32, 0.0, twopi));
      _h_ljets.push_back(bookHisto1D("ljets_m", 50, 0, 250));
      // _h_ljets.push_back(bookHisto1D("ljets_mt", 50, 0, 250));

      // b-jets
      _h_bjets.push_back(bookHisto1D("bjets_N", 6, 0, 5));
      _h_bjets.push_back(bookHisto1D("bjets_E", 50, 0, 250));
      _h_bjets.push_back(bookHisto1D("bjets_Et", 50, 0, 250));
      _h_bjets.push_back(bookHisto1D("bjets_pt", 50, 0, 250));
      _h_bjets.push_back(bookHisto1D("bjets_pz", 50, 0, 250));
      _h_bjets.push_back(bookHisto1D("bjets_eta", 40, -5.0, 5.0));
      _h_bjets.push_back(bookHisto1D("bjets_phi", 32, 0.0, twopi));
      _h_bjets.push_back(bookHisto1D("bjets_m", 50, 0, 250));
      // _h_bjets.push_back(bookHisto1D("bjets_mt", 50, 0, 250));

      // leading Jet
      // _h_jet_1.push_back(bookHisto1D("jet_1_E", 50, 0, 250));
      // _h_jet_1.push_back(bookHisto1D("jet_1_Et", 50, 0, 250));
      // _h_jet_1.push_back(bookHisto1D("jet_1_pt", 50, 0, 250));
      // _h_jet_1.push_back(bookHisto1D("jet_1_pz", 50, 0, 250));
      // _h_jet_1.push_back(bookHisto1D("jet_1_eta", 40, -5.0, 5.0));
      // _h_jet_1.push_back(bookHisto1D("jet_1_phi", 32, 0.0, twopi));
      // _h_jet_1.push_back(bookHisto1D("jet_1_m", 50, 0, 250));
      // _h_jet_1.push_back(bookHisto1D("jet_1_mt", 50, 0, 250));

      // 2nd leading Jet
      // _h_jet_2.push_back(bookHisto1D("jet_2_E", 50, 0, 250));
      // _h_jet_2.push_back(bookHisto1D("jet_2_Et", 50, 0, 250));
      // _h_jet_2.push_back(bookHisto1D("jet_2_pt", 50, 0, 250));
      // _h_jet_2.push_back(bookHisto1D("jet_2_pz", 50, 0, 250));
      // _h_jet_2.push_back(bookHisto1D("jet_2_eta", 40, -5.0, 5.0));
      // _h_jet_2.push_back(bookHisto1D("jet_2_phi", 32, 0.0, twopi));
      // _h_jet_2.push_back(bookHisto1D("jet_2_m", 50, 0, 250));
      // _h_jet_2.push_back(bookHisto1D("jet_2_mt", 50, 0, 250));

      // 3rd leading Jet
      // _h_jet_3.push_back(bookHisto1D("jet_3_E", 50, 0, 250));
      // _h_jet_3.push_back(bookHisto1D("jet_3_Et", 50, 0, 250));
      // _h_jet_3.push_back(bookHisto1D("jet_3_pt", 50, 0, 250));
      // _h_jet_3.push_back(bookHisto1D("jet_3_pz", 50, 0, 250));
      // _h_jet_3.push_back(bookHisto1D("jet_3_eta", 40, -5.0, 5.0));
      // _h_jet_3.push_back(bookHisto1D("jet_3_phi", 32, 0.0, twopi));
      // _h_jet_3.push_back(bookHisto1D("jet_3_m", 50, 0, 250));
      // _h_jet_3.push_back(bookHisto1D("jet_3_mt", 50, 0, 250));

      // leading light Jet
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_E", 50, 0, 250));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_Et", 50, 0, 250));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_pt", 50, 0, 250));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_pz", 50, 0, 250));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_eta", 40, -5.0, 5.0));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_phi", 32, 0.0, twopi));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_m", 50, 0, 250));
      // _h_ljet_1.push_back(bookHisto1D("ljet_1_mt", 50, 0, 250));

      // 2nd leading light Jet
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_E", 50, 0, 250));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_Et", 50, 0, 250));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_pt", 50, 0, 250));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_pz", 50, 0, 250));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_eta", 40, -5.0, 5.0));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_phi", 32, 0.0, twopi));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_m", 50, 0, 250));
      // _h_ljet_2.push_back(bookHisto1D("ljet_2_mt", 50, 0, 250));

      // 3rd leading light Jet
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_E", 50, 0, 250));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_Et", 50, 0, 250));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_pt", 50, 0, 250));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_pz", 50, 0, 250));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_eta", 40, -5.0, 5.0));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_phi", 32, 0.0, twopi));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_m", 50, 0, 250));
      // _h_ljet_3.push_back(bookHisto1D("ljet_3_mt", 50, 0, 250));

      // leading b-Jet
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_E", 50, 0, 250));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_Et", 50, 0, 250));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_pt", 50, 0, 250));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_pz", 50, 0, 250));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_eta", 40, -5.0, 5.0));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_phi", 32, 0.0, twopi));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_m", 50, 0, 250));
      // _h_bjet_1.push_back(bookHisto1D("bjet_1_mt", 50, 0, 250));

      // 2nd leading b-Jet
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_E", 50, 0, 250));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_Et", 50, 0, 250));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_pt", 50, 0, 250));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_pz", 50, 0, 250));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_eta", 40, -5.0, 5.0));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_phi", 32, 0.0, twopi));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_m", 50, 0, 250));
      // _h_bjet_2.push_back(bookHisto1D("bjet_2_mt", 50, 0, 250));

      // 3rd leading b-Jet
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_E", 50, 0, 250));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_Et", 50, 0, 250));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_pt", 50, 0, 250));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_pz", 50, 0, 250));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_eta", 40, -5.0, 5.0));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_phi", 32, 0.0, twopi));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_m", 50, 0, 250));
      // _h_bjet_3.push_back(bookHisto1D("bjet_3_mt", 50, 0, 250));

      // N jets with different pt cuts
      // _h_Njets.push_back(bookHisto1D("Njets_10", 16., 0., 15.));
      // _h_Njets.push_back(bookHisto1D("Njets_25", 16., 0., 15.));
      // _h_Njets.push_back(bookHisto1D("Njets_40", 16., 0., 15.));
      // _h_Njets.push_back(bookHisto1D("Njets_80", 16., 0., 15.));
      // _h_Njets.push_back(bookHisto1D("Njets_100", 16., 0., 15.));

      // N light jets with different pt cuts
      // _h_Nljets.push_back(bookHisto1D("Nljets_10", 16., 0., 15.));
      // _h_Nljets.push_back(bookHisto1D("Nljets_25", 16., 0., 15.));
      // _h_Nljets.push_back(bookHisto1D("Nljets_40", 16., 0., 15.));
      // _h_Nljets.push_back(bookHisto1D("Nljets_80", 16., 0., 15.));
      // _h_Nljets.push_back(bookHisto1D("Nljets_100", 16., 0., 15.));

      // N b-jets with different pt cuts
      // _h_Nbjets.push_back(bookHisto1D("Nbjets_10", 16., 0., 15.));
      // _h_Nbjets.push_back(bookHisto1D("Nbjets_25", 16., 0., 15.));
      // _h_Nbjets.push_back(bookHisto1D("Nbjets_40", 16., 0., 15.));
      // _h_Nbjets.push_back(bookHisto1D("Nbjets_80", 16., 0., 15.));
      // _h_Nbjets.push_back(bookHisto1D("Nbjets_100", 16., 0., 15.));

      // jets HT
      // _h_jet_HT = bookHisto1D("jet_HT", logspace(50, 100.0, 2000.0));

      // W boson
      _h_W.push_back(bookHisto1D("W_E", 150, 0, 500));
      _h_W.push_back(bookHisto1D("W_Et", 50, 0, 250));
      _h_W.push_back(bookHisto1D("W_pt", 50, 0, 250));
      _h_W.push_back(bookHisto1D("W_pz", 50, 0, 250));
      _h_W.push_back(bookHisto1D("W_eta", 40, -5.0, 5.0));
      _h_W.push_back(bookHisto1D("W_phi", 32, 0.0, twopi));
      _h_W.push_back(bookHisto1D("W_m", 150, 0, 150));
      _h_W.push_back(bookHisto1D("W_mt", 150, 0, 150));

      // top quark
      _h_t.push_back(bookHisto1D("t_E", 150, 0, 500));
      _h_t.push_back(bookHisto1D("t_Et", 50, 0, 250));
      _h_t.push_back(bookHisto1D("t_pt", 50, 0, 250));
      _h_t.push_back(bookHisto1D("t_pz", 50, 0, 250));
      _h_t.push_back(bookHisto1D("t_eta", 40, -5.0, 5.0));
      _h_t.push_back(bookHisto1D("t_phi", 32, 0.0, twopi));
      _h_t.push_back(bookHisto1D("t_m", 250, 0, 250));
      _h_t.push_back(bookHisto1D("t_mt", 250, 0, 250));

      // angular distributions
      _h_cosTheta_S = bookHisto1D("CosThetaS", 32, -1.0, 1.0);
      _h_cosTheta_N = bookHisto1D("CosThetaN", 32, -1.0, 1.0);
      _h_cosTheta_T = bookHisto1D("CosThetaT", 32, -1.0, 1.0);
      _h_cosTheta_X = bookHisto1D("CosThetaX", 32, -1.0, 1.0);
      _h_cosTheta = bookHisto1D("CosThetaW", 32, -1.0, 1.0);
      _h_Phi_S = bookHisto1D("PhiS", 32, 0.0, 2.0);
    }

    //@}

  private:

    double _weight;

    FourMomentum _lepton_from_W_boson;
    FourMomentum _bjet_from_top_quark;
    FourMomentum _spectjet_from_top_quark;

    /// @name Histograms
    //@{

    Histo1DPtr _h_weight;

    vector<Histo1DPtr> _h_charged;

    vector<Histo1DPtr> _h_B_hadrons;

    vector<Histo1DPtr> _h_jets;
    vector<Histo1DPtr> _h_ljets;
    vector<Histo1DPtr> _h_bjets;

    // vector<Histo1DPtr> _h_jet_1;
    // vector<Histo1DPtr> _h_jet_2;
    // vector<Histo1DPtr> _h_jet_3;

    // vector<Histo1DPtr> _h_ljet_1;
    // vector<Histo1DPtr> _h_ljet_2;
    // vector<Histo1DPtr> _h_ljet_3;

    // vector<Histo1DPtr> _h_bjet_1;
    // vector<Histo1DPtr> _h_bjet_2;
    // vector<Histo1DPtr> _h_bjet_3;

    // vector<Histo1DPtr> _h_Njets;
    // vector<Histo1DPtr> _h_Nljets;
    // vector<Histo1DPtr> _h_Nbjets;

    // Histo1DPtr _h_jet_HT;

    vector<Histo1DPtr> _h_electrons;
    vector<Histo1DPtr> _h_muons;
    vector<Histo1DPtr> _h_taus;
    vector<Histo1DPtr> _h_neutrinos;

    vector<Histo1DPtr> _h_electron_1;
    vector<Histo1DPtr> _h_muon_1;
    vector<Histo1DPtr> _h_neutrino_1;

    vector<Histo1DPtr> _h_t;
    vector<Histo1DPtr> _h_W;

    vector<Histo1DPtr> _h_MET;

    Histo1DPtr _h_cosTheta_S;
    Histo1DPtr _h_cosTheta_N;
    Histo1DPtr _h_cosTheta_T;
    Histo1DPtr _h_cosTheta_X;
    Histo1DPtr _h_cosTheta;
    Histo1DPtr _h_Phi_S;

    //@}
  };

  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_SingleTop_Truth> plugin_MC_SingleTop_Truth;

}

