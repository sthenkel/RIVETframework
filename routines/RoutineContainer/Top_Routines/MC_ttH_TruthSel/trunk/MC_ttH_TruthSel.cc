#include "Rivet/Analysis.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/AnalysisLoader.hh"
#include <map>
//#include <boost/lexical_cast.hpp>

// adapted from code written by the IPPP Durham group

namespace Rivet {

  using namespace Cuts;

  class MC_ttH_TruthSel : public Analysis {
  public:
    MC_ttH_TruthSel() : 
      Analysis("MC_ttH_TruthSel"),
      _pt_el(25.), _eta_el(2.47), _pt_mu(25.), _eta_mu(2.5), _eta_C(5.),
      _pt_lep(min(_pt_el,_pt_mu)), _eta_lep(max(_eta_el,_eta_mu)),
      _pt_isoT_mu(0.05), _R_isoT_mu(10.0), _pt_isoT_el(1), _R_isoT_el(0.3),
      _pt_isoC_mu(4.), _R_isoC_mu(0.2), _pt_isoC_el(2.), _R_isoC_el(0.2),
      _ETmiss_el(20.), _ETmissMW_el(60.), _ETmiss_mu(20.), _ETmissMW_mu(60.),
      _pt_j(25.), _eta_j(2.5), _R_j(0.4), _R_j_mu(0.4), 
      _MT(172.), _MW(80.), _dR_e_j(0.4), _dR_l_j(0.4),
      _reco_sig_MTlep(11.08), _reco_sig_MThad(9.864), _reco_sig_MW(10.57)
    {}

    void init() {
      
      // Eta ranges
      Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
      Cut eta_lep  = etaIn(-2.5, 2.5);
      Cut _eta_C   = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);

      // All final state particles

      FinalState fs(eta_full);

      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);
      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");
      DressedLeptons dressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 30.0*GeV), true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons vetodressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
      addProjection(vetodressedelectrons, "vetodressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, true, eta_full, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");
      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");
      vector<pair<double, double> > eta_muon;
      DressedLeptons dressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 30.0*GeV), true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons vetodressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
      addProjection(vetodressedmuons, "vetodressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, true, eta_full, true);
      addProjection(ewdressedmuons, "ewdressedmuons");
      
      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");
      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4);
      jets.useInvisibles();
      addProjection(jets, "jets");
      
      IdentifiedFinalState taus;
      taus.acceptIdPair(PID::TAU);
      addProjection(taus, "Taus");
      
      
      inithistos();
    }
    
    void analyze(const Event& event) {

      const double weight  = event.weight();
      const int    counter = 1;

      _histos["weights"]  -> fill(0, weight);
      _histos["counterH"] -> fill(0, 1.0);

      //      std::cout << "Hier 1    " << weight  << std::endl;

      const ParticleVector& allleptons = (applyProjection<DressedLeptons>(event, "dressedelectrons")).particlesByPt();

	//sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());

	//applyProjection<ChargedLeptons>(event, "dressedelectrons").particlesByPt();  //+ applyProjection<ChargedLeptons>(event, "dressedmuons")).particlesByPt();
      
      //      std::cout << "Hier 1a" << std::endl;


      const ParticleVector& visibles = allleptons;

      //      std::cout << "Hier 1b" << std::endl;

      const ParticleVector&  charged = allleptons; 

      //      std::cout << "Hier 1c" << std::endl;

      //      const MissingMomentum& metvec = 
      //	applyProjection<MissingMomentum>(event, "MissingET");

      //      std::cout << "Hier 1d" << std::endl;

      const Jets alljets = 
	applyProjection<FastJets>(event, "jets").jetsByPt();

      //      std::cout << "Hier 1e" << std::endl;

      const ParticleVector& neutrinos = 
	applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();


      //      std::cout << "Hier 1f 2" << std::endl;

      FourMomentum p_miss(0.,0.,0.,0.);
      foreach(const Particle& p, neutrinos) p_miss+=p.momentum();

      //      std::cout << "Hier 1f" << std::endl;
      

      const vector<DressedLepton>& dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());  
	//	applyProjection<Dre>(event, "DRESSEDELECTRONS").clusteredLeptons();

      //      std::cout << "Hier 2" << std::endl;


      // Get the selected objects, using the projections.
      /*      _dressedelectrons     = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
      _vetodressedelectrons = applyProjection<DressedLeptons>(event, "vetodressedelectrons").dressedLeptons();
      _dressedmuons         = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
      _vetodressedmuons     = applyProjection<DressedLeptons>(event, "vetodressedmuons").dressedLeptons();
      _neutrinos            = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();
      const Jets alljets_10 = applyProjection<FastJets>(event, "jets").jetsByPt(10.0, MAXDOUBLE, -2.5, 2.5);
      */


      ParticleVector leptons, isoleptons; 
      Jets bjets, ljets, njets, isojets;
      _pmiss = p_miss;  // -metvec.visibleMomentum();

      FillNumLep(allleptons, std::string("0") , weight);
      FilterLeptons(allleptons,leptons,visibles,charged,dressedelectrons);

      SetJetPt(alljets, njets);
   
      // cut on number of bjets, light jets and exactly 1 lepton 
      if (!FilterJets(njets, bjets, ljets, isojets,leptons,isoleptons, weight))
	vetoEvent;

      //      std::cout << "weights = " << weight << std::endl;

      _histos["weights"]->fill(1,weight);
      _histos["counterH"]->fill(1,1.0);

      _plep=isoleptons[0].momentum();
     
      // ETmiss and triangular cut
      if (!PassQCDCuts(isoleptons[0].pdgId())) vetoEvent;
     
      _histos["weights"]->fill(2,weight);
      _histos["counterH"]->fill(2,1.0);

      std::string qualifier;

      // cut on 2-4 light jets, 2-4 b-jets

      if (ljets.size()>1 && ljets.size()<5 && bjets.size()>1 && bjets.size()<5){
	int lj=ljets.size();
	int bj=bjets.size();
	qualifier = 
	  boost::lexical_cast<string>(lj)+"l_"+
	  boost::lexical_cast<string>(bj)+"b";

	//	std::cout << "Reconstruct TTbar" << std::endl;

	if (!ReconstructTTSystem(bjets, ljets, weight,qualifier)) vetoEvent;

	//	std::cout << "after Reconstruct TTbar" << std::endl;


	_histos["weights"]->fill(3,weight);
	_histos["counterH"]->fill(3,1.0);

 	FillHTs(ljets, bjets, qualifier, weight);
	FillTops(qualifier, weight);
	FillOthers(ljets, bjets, isojets, weight, qualifier);
      }
      else{

	//	std::cout << "veto event = " << ljets.size() << "\t" << bjets.size() << std::endl;

	vetoEvent;
      }
      FillNumLep(isoleptons, std::string("1"), weight);

      _histos["weights"]->fill(4,weight);
      _histos["counterH"]->fill(4,counter);

    }

  private:
    double _pt_el, _eta_el, _pt_mu, _eta_mu, _eta_C, _pt_lep, _eta_lep;
    double _pt_isoT_mu, _R_isoT_mu, _pt_isoT_el, _R_isoT_el;
    double _pt_isoC_mu, _R_isoC_mu, _pt_isoC_el, _R_isoC_el;
    double _ETmiss_el, _ETmissMW_el, _ETmiss_mu, _ETmissMW_mu;
    double _pt_j, _eta_j, _R_j, _R_j_mu;
    double _MT, _MW, _dR_e_j, _dR_l_j;
    double _reco_sig_MTlep, _reco_sig_MThad, _reco_sig_MW;

    // added by Andrea
    double MTW;

    size_t       _b_thad, _b_tlep, _l_W1, _l_W2, _take;
    FourMomentum _plep, _pmiss, _Wlep[2], _Wlepwin, _Whad, _tlep, _thad;
    FourMomentum _pneus[2];

    std::map<std::string, Histo1DPtr > _histos;

    void inithistos() {
      _histos["counterH"]         = bookHisto1D("counterH",11, -.75, 4.75); // Nevents passing cuts
      _histos["weights"]          = bookHisto1D("weights",11,  -.75, 4.75); // weights passing cuts
      _histos["n_elec_0"]         = bookHisto1D("n_elec_0",6, -0.5, 5.5);  // initial number of electrons
      _histos["n_elec_1"]         = bookHisto1D("n_elec_1",6, -0.5, 5.5);  // final number of electrons
      _histos["n_muon_0"]         = bookHisto1D("n_muon_0",6, -0.5, 5.5);  // initial number of muons
      _histos["n_muon_1"]         = bookHisto1D("n_muon_1",6, -0.5, 5.5);  // final number of muons
      _histos["n_lep_0"]          = bookHisto1D("n_lep_0",6, -0.5, 5.5);   // initial number of leptons
      _histos["n_lep_1"]          = bookHisto1D("n_lep_1",6, -0.5, 5.5);   // final number of leptons
      _histos["chi2_hadW"]        = bookHisto1D("chi2_hadW",20, 0., 50.);  // abs(MW^2-(reconstructed MW)^2)/sig_MW^2
      _histos["chi2_hadT"]        = bookHisto1D("chi2_hadT",20, 0., 50.);  // abs(Mt^2-(reconstructed Mt)^2)/sig_had_Mt^2
      _histos["chi2_lepT"]        = bookHisto1D("chi2_lepT",20, 0., 50.);  // abs(Mt^2-(reconstructed Mt)^2)/sig_lep_Mt^2
      _histos["M_hadW"]           = bookHisto1D("M_hadW",20, 30., 140.);   // mass of hadronic W
      _histos["M_hadT"]           = bookHisto1D("M_hadT",28, 100., 240.);  // mass of hadronic t
      _histos["M_lepT"]           = bookHisto1D("M_lepT",28, 100., 240.);  // mass of leptonic t
      _histos["n_lj"]             = bookHisto1D("n_lj",6, -0.5, 5.5);      // number of light jets
      _histos["n_bj"]             = bookHisto1D("n_bj",6, -0.5, 5.5);      // number of b jets
      _histos["n_j"]              = bookHisto1D("n_j",10, -0.5, 9.5);       // number of jets
      _histos["M_TW"]             = bookHisto1D("M_TW",30, 0., 300.);      // transverse W mass
      // rap of hardest non-t jet - (rap of hadronic t + rap of leptonic t)/2
      _histos["tt_j_rap"]         = bookHisto1D("tt_j_rap",10, -2.5, 2.5); 
      _histos["ETmiss"]           = bookHisto1D("ETmiss",50, 0., 1000.);    // missing ET
      _histos["pt_ttH"]           = bookHisto1D("pt_ttH",50,0.,500.);      // pT of (tt and hardest 2 non-t jets)
      _histos["ETmiss_MTW"]       = bookHisto1D("ETmiss_MTW",60, 0., 1200.);// missing ET + W transverse mass
      _histos["HTjets"]           = bookHisto1D("HTjets",36, 200., 2000.); // HT of jets
      _histos["HTobjs"]           = bookHisto1D("HTobjs",36, 200., 2000.); // HT of leptons, jets, missing ET
      _histos["HTvis"]            = bookHisto1D("HTvis",36, 200., 2000.);  // HT of leptons, jets
      _histos["pt_lep"]           = bookHisto1D("pt_lep", 20, 25., 425.);  // HT of leptons, jets 
      _histos["eta_lep"]          = bookHisto1D("eta_lep", 10, -2.5, 2.5);  // HT of leptons, jets
      _histos["dphi_missET_j"]    = bookHisto1D("dphi_missET_j", 10, 0., M_PI); // missing ET and closest jet
      _histos["dphi_missET_l"]    = bookHisto1D("dphi_missET_l", 10, 0., M_PI); // missing energy and lepton   
      _histos["dphi_missET_lorj"] = bookHisto1D("dphi_missET_lorj", 10, 0., M_PI); // missing ET and closest visible object 
      _histos["dphi_lj"]          = bookHisto1D("dphi_lj", 10, 0., M_PI);  // lepton and closest jet
      std::string name, qualifier;

      // histograms divided up by jet multiplicity
      for (size_t lj=2;lj<=4;lj++) {   // light jets
	for (size_t bj=2;bj<=4;bj++) { // b jets
	  qualifier = 
	    boost::lexical_cast<string>(lj)+"l_"+
	    boost::lexical_cast<string>(bj)+"b";
	  name = "HTjets_"+qualifier;    // HT jets
	  _histos[name] = bookHisto1D(name,36, 200., 2000.);
	  name = "HTobjs_"+qualifier;    // HT jets, leptons, missing ET
	  _histos[name] = bookHisto1D(name,36, 200., 2000.);
	  name = "HTvis_"+qualifier;     // HT jets, leptons 
	  _histos[name] = bookHisto1D(name,36, 200., 2000.);
	  name = "ETmiss_"+qualifier;    // missing ET
	  _histos[name] = bookHisto1D(name,50, 0., 1000.);
	  name = "pt_toplep_"+qualifier; // pT of leptonic t
	  _histos[name] = bookHisto1D(name,20, 0., 400.);
	  name = "pt_lep_"+qualifier;    // pT of lepton
	  _histos[name] = bookHisto1D(name,20, 25., 425.);
	  name = "M_hadW_"+qualifier;    // mass of hadronic W
	  _histos[name] = bookHisto1D(name,20, 40., 120.);
	  name = "M_hadT_"+qualifier;    // mass of hadronic t
	  _histos[name] = bookHisto1D(name,20, 120., 220.);
	  name = "y_toplep_"+qualifier;  // rapidity of leptonic t
	  _histos[name] = bookHisto1D(name,10, -2.5, 2.5);
	  name = "pt_tophad_"+qualifier; // pT of hadronic t
	  _histos[name] = bookHisto1D(name,20, 0., 200.);
	  name = "y_tophad_"+qualifier;  // rapidity of hadronic t
	  _histos[name] = bookHisto1D(name,10, -2.5, 2.5);
	  name = "pt_tt_"+qualifier;     // pt of tt
	  _histos[name] = bookHisto1D(name,20, 0., 400.);
	  if (lj+bj>=6){
	    name = "pt_ttH_"+qualifier;  // pt of (tt and hardest 2 non-t jets)
	    _histos[name] = bookHisto1D(name,50, 0., 500.);
	  }
	  name = "y_tt_"+qualifier;      // rapidity of tt
	  _histos[name] = bookHisto1D(name,10, -2.5, 2.5);
	  name = "dy_tt_"+qualifier;     // d(rap) tt
	  _histos[name] = bookHisto1D(name,10, 0., 5.);
	  name = "dphi_tt_"+qualifier;   // d(phi) tt
	  _histos[name] = bookHisto1D(name,20, 0., PI);
	  name = "dR_tt_"+qualifier;     // dR tt
	  _histos[name] = bookHisto1D(name,12, 0., 6.);
	  name = "M_TW_"+qualifier;      // transerse W mass
	  _histos[name] = bookHisto1D(name,20, 0., 200.);
	  name = "ETmiss_MTW_"+qualifier;// missing ET + transverse W mass
	  _histos[name] = bookHisto1D(name, 60, 0., 1200.);
	}
      }
      _histos["pt_allj1"]  = bookHisto1D("pt_allj1",20, 0., 400.);  // pT hardest jet
      _histos["pt_allj2"]  = bookHisto1D("pt_allj2",20, 0., 400.);  // 2nd
      _histos["pt_allj3"]  = bookHisto1D("pt_allj3",20, 0., 400.);  // 3rd
      _histos["pt_allj4"]  = bookHisto1D("pt_allj4",20, 0., 400.);  // 4th
      _histos["pt_allj5"]  = bookHisto1D("pt_allj5",20, 0., 400.);  // 5th
      _histos["pt_allj6"]  = bookHisto1D("pt_allj6",20, 0., 400.);  // 6th
      _histos["pt_b1"]     = bookHisto1D("pt_b1",20, 0., 400.);     // pT hardest b jet
      _histos["pt_b2"]     = bookHisto1D("pt_b2",20, 0., 400.);     // 2nd
      _histos["pt_b3"]     = bookHisto1D("pt_b3",20, 0., 400.);     // 3rd
      _histos["pt_b4"]     = bookHisto1D("pt_b4",20, 0., 400.);     // 4th
      _histos["eta_allj1"] = bookHisto1D("eta_allj1",10, -2.5, 2.5);// eta hardest jet
      _histos["eta_allj2"] = bookHisto1D("eta_allj2",10, -2.5, 2.5);// 2nd
      _histos["eta_allj3"] = bookHisto1D("eta_allj3",10, -2.5, 2.5);// 3rd
      _histos["eta_allj4"] = bookHisto1D("eta_allj4",10, -2.5, 2.5);// 4th
      _histos["eta_allj5"] = bookHisto1D("eta_allj5",10, -2.5, 2.5);// 5th
      _histos["eta_allj6"] = bookHisto1D("eta_allj6",10, -2.5, 2.5);// 6th
      _histos["eta_b1"]    = bookHisto1D("eta_b1",10, -2.5, 2.5);   // eta hardest b jet
      _histos["eta_b2"]    = bookHisto1D("eta_b2",10, -2.5, 2.5);   // 2nd
      _histos["eta_b3"]    = bookHisto1D("eta_b3",10, -2.5, 2.5);   // 3rd
      _histos["eta_b4"]    = bookHisto1D("eta_b4",10, -2.5, 2.5);   // 4th
      _histos["pt_jj"]     = bookHisto1D("pt_jj",20, 0., 400.);     // pT of (hardest 2 non-t jets)
      _histos["m_jj"]      = bookHisto1D("m_jj",50, 40., 540.);     // mass of (hardest 2 non-t jets)
      _histos["dR_jj"]     = bookHisto1D("dR_jj",20, 0., 10.);      // dR of (hardest 2 non-t jets)
      _histos["dy_jj"]     = bookHisto1D("dy_jj",10, 0., 5.);       // d(rap) of (hardest 2 non-t jets)
      _histos["dphi_jj"]   = bookHisto1D("dphi_jj",10, 0., M_PI);   // d(phi) of (hardest 2 non-t jets
      // d(phi) of (hadronic t + leptonic t) and (hardest 2 non-t jets)
      _histos["dphi_ttjj"] = bookHisto1D("dphi_ttjj",10, 0., M_PI);
      // pT of (hardest 2 non-t jets)/HT hardest 2 non-t jets
      _histos["ptjj_rel"]  = bookHisto1D("ptjj_rel",10, 0., 1.); 
      _histos["pt_blj"]    = bookHisto1D("pt_blj",20, 0., 400.);   // pT of (hardest non-t light jet + hardest non-t b jet)
      _histos["m_blj"]     = bookHisto1D("m_blj",50, 40., 540.);   // mass of (hardest non-t light jet + hardest non-t b jet)
      _histos["dR_blj"]    = bookHisto1D("dR_blj",20, 0., 10.);    // dR (hardest non-t light jet + hardest non-t b jet)
      _histos["pt_bb"]     = bookHisto1D("pt_bb",20, 0., 400.);    // pT (2 hardest non-t bs)
      _histos["m_bb"]      = bookHisto1D("m_bb", 54,  5., 545.);    // mass (2 hardest non-t bs)
      _histos["dR_bb"]     = bookHisto1D("dR_bb",20, 0., 10.);     // dR (2 hardest non-t bs)
    }

    void FillOthers(const Jets & ljets,const Jets & bjets, const Jets & isojets,
		    const double & weight, string & qualifier) {
      // fill remaining histograms
      size_t j1(ljets.size()+bjets.size()),j2(ljets.size()+bjets.size());
      size_t k1(ljets.size()+bjets.size()),k2(ljets.size()+bjets.size()),k3(ljets.size()+bjets.size()),k4(ljets.size()+bjets.size());
      FourMomentum jet1,jet2;
      bool extra(false);
      int ljet(0), bjet(0);
      FourMomentum p1,p2;
      std::string postfix=std::string("");
      if ((ljets.size()+bjets.size())>=6) {
	extra=true;

	// loop over light jets, count light jets that do not come from the W-boson decay
	for (size_t i=0;i<ljets.size();i++) {
	  if (i!=_l_W1 && i!=_l_W2) { k1=i; ljet++; break; }
	}

	// no idea what is done here...
	for (size_t i=0;i<ljets.size();i++) {
	  if (i!=_l_W1 && i!=_l_W2 && i!=k1) { k2=i; ljet++; break; }
	}
	
	// loop over b-jets, count b-jets that do not come from the top-quark decay
	for (size_t i=0;i<bjets.size();i++) {
	  if (i!=_b_tlep && i!=_b_thad) { k3=i; bjet++; break; }
	}

	// no idea what is done here...
	for (size_t i=0;i<bjets.size();i++) {
	  if (i!=_b_tlep && i!=_b_thad && i!=k3) { k4=i; bjet++; break; }
	}

	// if there is more than 1 extra b and one extra light jet: fill highest pt extra jet in jet1 and second highest pt extra jet in jet2

	if (bjet>1 && ljet>1){
	  if (ljets[k2].momentum().pT()>bjets[k3].momentum().pT()){
	    jet1 = ljets[k1].momentum();
	    jet2 = ljets[k2].momentum();
	  }
	  else if (bjets[k4].momentum().pT()>ljets[k1].momentum().pT()){
	    jet1 = bjets[k3].momentum();
	    jet2 = bjets[k4].momentum();
	  }
	  else if (ljets[k1].momentum().pT()>bjets[k3].momentum().pT()){
	    jet1 = ljets[k1].momentum();
	    jet2 = bjets[k3].momentum();
	  }
	  else if (ljets[k1].momentum().pT()<bjets[k3].momentum().pT()){
	    jet1 = bjets[k3].momentum();
	    jet2 = ljets[k1].momentum();
	  }
	}
	else if (ljet>1 && bjet==1){
	  if (bjets[k3].momentum().pT()>ljets[k1].momentum().pT()){
	    jet1 = bjets[k3].momentum();
	    jet2 = ljets[k1].momentum();
	  }
	  else if (bjets[k3].momentum().pT()>ljets[k2].momentum().pT()){
	    jet1 = ljets[k1].momentum();
	    jet2 = bjets[k3].momentum();
	  }
	  else{
	    jet1 = ljets[k1].momentum();
	    jet2 = ljets[k2].momentum();
	  }
	}
	else if (ljet==1 && bjet>1){
	  if (ljets[k1].momentum().pT()>bjets[k3].momentum().pT()){
	    jet1 = ljets[k1].momentum();
	    jet2 = bjets[k3].momentum();
	  }
	  else if (ljets[k1].momentum().pT()>bjets[k4].momentum().pT()){
	    jet1 = bjets[k3].momentum();
	    jet2 = ljets[k1].momentum();
	  }
	  else{
	    jet1 = bjets[k3].momentum();
	    jet2 = bjets[k4].momentum();
	  }
	}
	else if (ljet==1 && bjet==1){
	  if (ljets[k1].momentum().pT()>bjets[k3].momentum().pT()){
	    jet1 = ljets[k1].momentum();
	    jet2 = bjets[k3].momentum();
	  }
	  else{
	    jet1 = bjets[k3].momentum();
	    jet2 = ljets[k1].momentum();
	  }
	}
	else if (ljet>1){
	  jet1 = ljets[k1].momentum();
	  jet2 = ljets[k2].momentum();
	}
	else if (bjet>1){
	  jet1 = bjets[k3].momentum();
	  jet2 = bjets[k4].momentum();
	}
      }
      if (bjets.size()==3 && ljets.size()>=3) {
	postfix=std::string("blj");
	for (size_t i=0;i<ljets.size();i++) {
	  if (i!=_l_W1 && i!=_l_W2) { j1=i; break; }
	}
	for (size_t i=0;i<bjets.size();i++) {
	  if (i!=_b_thad && i!=_b_tlep) { j2=i; break; }
	}
	p1 = ljets[j1].momentum();
	p2 = bjets[j2].momentum();
      }
      else if (bjets.size()>=4 && ljets.size()>=2) {
	postfix=std::string("bb");
	for (size_t i=0;i<bjets.size();i++) {
	  if (i!=_l_W1 && i!=_l_W2) { j1=i; break; }
	}
	for (size_t i=0;i<bjets.size();i++) {
	  if (i!=_l_W1 && i!=_l_W2 && i!=j1) { j2=i; break; }
	}
	p1 = bjets[j1].momentum();
	p2 = bjets[j2].momentum();
      }
      _histos[std::string("n_lj")] -> fill(ljets.size(), weight);
      _histos[std::string("n_bj")] -> fill(bjets.size(), weight);
      _histos[std::string("n_j")]  -> fill(isojets.size(), weight);


      if (extra){
	 double ttjrap   = jet1.rapidity()-(_thad.rapidity()+_tlep.rapidity())/2.;
	 FourMomentum jj = (jet1+jet2);
	 FourMomentum tt = (_thad+_tlep);
	 double ptjj     = (jet1+jet2).pT();
	 double rel      = (jet1+jet2).pT()/(jet1.pT()+jet2.pT());
	 _histos[std::string("tt_j_rap")]  -> fill(ttjrap, weight);
	 _histos[std::string("pt_jj")]     -> fill(ptjj,weight);
	 _histos[std::string("m_jj")]      -> fill((jet1+jet2).mass(),weight);
	 _histos[std::string("dR_jj")]     -> fill(deltaR(jet1,jet2),weight);
	 _histos[std::string("dy_jj")]     -> fill(abs(jet1.rapidity()-jet2.rapidity()),weight);
	 _histos[std::string("dphi_jj")]   -> fill(deltaPhi(jet1,jet2),weight);
	 _histos[std::string("tt_j_rap")]  -> fill(ttjrap,weight);
	 _histos[std::string("dphi_ttjj")] -> fill(deltaPhi(tt,jj),weight);
	 _histos[std::string("ptjj_rel")]  -> fill(rel,weight);
	 string name="pt_ttH_"+qualifier;
	 _histos[name]->fill((_tlep+_thad+jet1+jet2).pT(),weight);
	 _histos["pt_ttH"]->fill((_tlep+_thad+jet1+jet2).pT(),weight);
       }
       if (postfix!=std::string("")) {
	 _histos[std::string("pt_")+postfix]->fill((p1+p2).pT(),weight);
	 _histos[std::string("m_")+postfix]->fill((p1+p2).mass(),weight);
	 _histos[std::string("dR_")+postfix]->fill(deltaR(p1,p2),weight);
       }
       double dphi_etj(4.);
       double dphi_lj(4.);
       foreach(const Jet & jet, isojets){
	 if (deltaPhi(_pmiss, jet)<dphi_etj)
	   dphi_etj=deltaPhi(_pmiss, jet);
	 if (deltaPhi(_plep, jet)<dphi_lj)
	   dphi_lj=deltaPhi(_plep, jet);
       }
       for (size_t i=1; i<=isojets.size(); i++){
	 if (i>6) break;
	 string name = "pt_allj"+boost::lexical_cast<string>(i);
	 _histos[name]   -> fill(isojets[i-1].momentum().pT(),weight);
	 name="eta_allj"+boost::lexical_cast<string>(i);
	 _histos[name]   -> fill(isojets[i-1].momentum().eta(),weight);
       }
       for (size_t i=1; i<=bjets.size(); i++){
	 if (i>4) break;
	 string name = "pt_b"+boost::lexical_cast<string>(i);
	 _histos[name]  -> fill(bjets[i-1].momentum().pT(),weight);
	 name="eta_b"+boost::lexical_cast<string>(i);
	 _histos[name]  -> fill(bjets[i-1].momentum().eta(),weight);
       }

       //       double MTW=sqrt(2.*_plep.pT()*_pneus[_take].pT()*(1-cos((_plep+_pneus[_take]).phi())));

       //       std::cout << "Hier 2 : " << MTW << "\t" << _pmiss.Et() << std::endl;

       _histos["pt_lep"]            -> fill(_plep.pT(),  weight);
       _histos["eta_lep"]           -> fill(_plep.eta(), weight);
       _histos["M_TW"]              -> fill(MTW,weight);
       _histos["ETmiss_MTW"]        -> fill(_pmiss.Et()+MTW,weight);
       _histos["ETmiss"]            -> fill(_pmiss.Et(),weight);
       string name="M_TW_"+qualifier;
       _histos[name]                -> fill(MTW,weight);
       name="ETmiss_"+qualifier;
       _histos[name]                -> fill(_pmiss.Et(),weight);
       name="ETmiss_MTW_"+qualifier;
       _histos[name]                -> fill(_pmiss.Et()+MTW,weight);
       name="pt_lep_"+qualifier;
       _histos[name]                -> fill(_plep.pT(),weight);
       if (dphi_etj<4.)
	 _histos["dphi_missET_j"]    -> fill(dphi_etj,weight);
       if (dphi_lj<4.)
	 _histos["dphi_lj"]          -> fill(dphi_lj, weight);
       if (dphi_etj<4. || dphi_lj<4.)
	 _histos["dphi_missET_lorj"] -> fill(min(dphi_lj, dphi_etj),weight);
       _histos["dphi_missET_l"]     -> fill(deltaPhi(_pmiss, _plep),weight);
     }
    
    void FillTops(const std::string & qualifier, const double & weight) {
      // fill histograms for tops
      std::string name;
      name = "pt_toplep_"+qualifier;
      _histos[name]->fill(_tlep.pT(),weight);
      name = "y_toplep_"+qualifier;
      _histos[name]->fill(_tlep.rapidity(),weight);
      name = "pt_tophad_"+qualifier;
      _histos[name]->fill(_thad.pT(),weight);
      name = "y_tophad_"+qualifier;
      _histos[name]->fill(_thad.rapidity(),weight);
      name = "pt_tt_"+qualifier;
      _histos[name]->fill((_tlep+_thad).pT(),weight);
      name = "y_tt_"+qualifier;
      _histos[name]->fill((_tlep+_thad).rapidity(),weight);
      name = "dy_tt_"+qualifier;
      _histos[name]->fill(fabs(_tlep.rapidity()-
			       _thad.rapidity()),weight);
       name = "dphi_tt_"+qualifier;
       _histos[name]->fill(deltaPhi(_tlep,_thad),weight);
       name = "dR_tt_"+qualifier;
       _histos[name]->fill(deltaR(_tlep,_thad),weight);
       name = "M_hadT_"+qualifier;
       _histos[name]->fill(_thad.mass(),weight);
    }
    
    void FillHTs(const Jets & ljets,const Jets & bjets,
		 const std::string & qualifier, const double & weight) {

      // fill histograms for HT
      double HTjets = 0.;
      double HTobjs = _tlep.pT()+_thad.pT();
      double HTvis  = _plep.pT();
      for (size_t i=0;i<ljets.size();i++) {

	HTjets += ljets[i].momentum().pT();
	HTvis  += ljets[i].momentum().pT();
	
	// add additional light jets to HT that are not coming from the W-boson
	if (i!=_l_W1 && i!=_l_W2)
	  HTobjs += ljets[i].momentum().pT();
      }

      for (size_t i=0;i<bjets.size();i++) {
	HTjets += bjets[i].momentum().pT();
	HTvis  += bjets[i].momentum().pT();

	// add additional b-jets to HT that are not coming from the top
	if (i!=_b_thad && i!=_b_tlep)
	  HTobjs += bjets[i].momentum().pT();
      }
      std::string name = "HTobjs_"+qualifier;
      _histos[name]->fill(HTobjs,weight);
      _histos["HTobjs"]->fill(HTobjs,weight);
      name = "HTjets_"+qualifier;
      _histos[name]->fill(HTjets,weight);
      _histos["HTjets"]->fill(HTjets,weight);
      name = "HTvis_"+qualifier;
      _histos[name]->fill(HTvis,weight);
      _histos["HTvis"]->fill(HTvis,weight);
    }
    
    bool ReconstructTTSystem(const Jets & bjets,const Jets & ljets,
			     const double & weight,string & qualifier) {
      if (!ReconstructLeptonicW() ||
	  !ReconstructHadronicW(ljets,weight,qualifier) ||
	  !ReconstructTops(bjets,weight,qualifier)) {
	return false;
      }
      return true;
    }
    
    bool ReconstructHadronicW(const Jets & ljets,const double & weight,string & qualifier) {
      if (ljets.size()<2) {
	return false;
      }
      double minchi2(1.e12),chi2;
      for (size_t i=0;i<ljets.size()-1;i++) {
	for (size_t j=i+1;j<ljets.size();j++) {
	  chi2 = 
	    fabs(_MW*_MW-sqr((ljets[i].momentum()+
			      ljets[j].momentum()).mass()))/
	    sqr(_reco_sig_MW);
	  if (chi2<minchi2) {
	    _l_W1 = i; _l_W2 = j; minchi2 = chi2;
	  }
	}
      }
      _Whad = ljets[_l_W1].momentum()+ljets[_l_W2].momentum();
      _histos["chi2_hadW"]->fill(minchi2,weight);
      _histos["M_hadW"]->fill(_Whad.mass(),weight);
      std::string name="M_hadW_"+qualifier;
      _histos[name]->fill(_Whad.mass(),weight);
      return true;
    }
    
    bool ReconstructTops(const Jets & bjets,const double & weight,string qualifier) {
      if (bjets.size()<2) {
	return false;
      }
      double minchi2(1.e12),chi2lep,chi2lep1,chi2lep2,chi2had;
      double minchi2lep(1.e12),minchi2had(1.e12);
      for (size_t i=0;i<bjets.size();i++) {
	for (size_t j=0;j<bjets.size();j++) {
	  if (i==j) continue;
	  chi2lep1 = 
	    fabs(_MT*_MT-sqr((bjets[i].momentum()+_Wlep[0]).mass()))/
	    sqr(_reco_sig_MTlep);
	  chi2lep2 = 
	    fabs(_MT*_MT-sqr((bjets[i].momentum()+_Wlep[1]).mass()))/
	    sqr(_reco_sig_MTlep);
	  if (chi2lep1<chi2lep2) {
	    chi2lep = chi2lep1; _take = 0;
	  }
	  else {
	    chi2lep = chi2lep2; _take = 1;
	  }
	  chi2had = 
	    fabs(_MT*_MT-sqr((bjets[j].momentum()+_Whad).mass()))/
	    sqr(_reco_sig_MThad); 
	  if (chi2had*chi2lep<minchi2) {
	    _b_tlep    = i; _b_thad = j; 
	    minchi2    = chi2lep*chi2had;
	    minchi2lep = chi2lep;
	    minchi2had = chi2had;
	    _Wlepwin   = _Wlep[_take];
	  }
	}
      }
      _tlep = bjets[_b_tlep].momentum()+_Wlepwin;
      _thad = bjets[_b_thad].momentum()+_Whad;
      if (bjets.size()>3){
	size_t Hb1(0);
	size_t Hb2(0);
	for (size_t i=0; i<bjets.size(); i++){
	  if (i!=_b_tlep && i!=_b_thad && Hb1==0)
	    Hb1=i;
	  if (i!=_b_tlep && i!=_b_thad && i!=Hb1 && Hb2==0)
	    Hb2=i;
	}
	_histos["m_bb"]->fill((bjets[Hb1].momentum()+bjets[Hb2].momentum()).mass(), weight);

      }
      _histos["chi2_lepT"]->fill(minchi2lep,weight);
      _histos["chi2_hadT"]->fill(minchi2had,weight);
      _histos["M_lepT"]->fill(_tlep.mass(),weight);
      _histos["M_hadT"]->fill(_thad.mass(),weight);
      string name="M_hadT_"+qualifier;
      _histos[name]->fill(_thad.mass(),weight);
      return true;
    }

    bool ReconstructLeptonicW() {
      double X    = _MW*_MW/2.+fabs(_plep.x()*_pmiss.x()+_plep.y()*_pmiss.y());
      double X2   = X*X;
      double ptn2 = _pmiss.pT()*_pmiss.pT();
      double ptl2 = _plep.pT()*_plep.pT(), pll2 = _plep.z()*_plep.z();
      double disc = X2*pll2-ptl2*pll2*ptn2;
      if (disc<0.) {
	return false;
      }
      double offs = X*_plep.E(), den = ptl2;
      double E1   = (offs+sqrt(disc))/den, pln1 = sqrt(E1*E1-ptn2);
      double E2   = (offs-sqrt(disc))/den, pln2 = sqrt(E2*E2-ptn2);
      FourMomentum pneu0(E1,_pmiss.x(),_pmiss.y(),pln1);
      FourMomentum pneu1(E2,_pmiss.x(),_pmiss.y(),pln2);
      if (fabs(2.*pneu0*_plep-_MW*_MW)>1.)  
	pneu0 = FourMomentum(E1,_pmiss.x(),_pmiss.y(),-pln1);
      if (fabs(2.*pneu1*_plep-_MW*_MW)>1.) 
	pneu1 = FourMomentum(E2,_pmiss.x(),_pmiss.y(),-pln2);
      _pneus[0]=pneu0;
      _pneus[1]=pneu1;
      _Wlep[0] = _plep+pneu0;
      _Wlep[1] = _plep+pneu1;
      return true;
    }

    bool PassQCDCuts(const PdgId & lepid) {
      double Etmiss(1.e12), EtmissMW(1.e12);
      if (abs(lepid)==11) {
	Etmiss   = _ETmiss_el;
	EtmissMW = _ETmissMW_el;
      }
      else if (abs(lepid)==13) {
	Etmiss   = _ETmiss_mu;
	EtmissMW = _ETmissMW_mu;
      }

      double missET = _pmiss.Et();
      // was here by default
      // double MWT = sqrt(2.*fabs(_plep.x()*_pmiss.x()+_plep.y()*_pmiss.y())); 

      // is the one that is used in the plots
      
      MTW = sqrt(2.*_plep.pT()*_pneus[_take].pT()*(1-cos((_plep+_pneus[_take]).phi())));

      //      std::cout << "Hier 1 : " << MTW << "\t" << missET << std::endl;

      if (missET<Etmiss*GeV) return false;
      if (MTW+missET<EtmissMW*GeV) return false;
      return true;
    }

    bool FilterJets(const Jets & njets,Jets & bjets,Jets & ljets, Jets & isojets,
		    ParticleVector & leptons, ParticleVector & isoleptons, double weight) {
      bool removed(false);
      foreach (Jet jet, njets){
	if (jet.momentum().pT()<_pt_j || fabs(jet.momentum().eta())>_eta_j)
	  continue; // minimum pT and maximum eta jet cuts
	bool close_elec(false);
	// remove hardest jet close to an electron
	foreach (Particle lep, leptons){
	  if (abs(lep.pdgId())==11){
	    if (deltaR(lep.momentum(), jet.momentum())<_dR_e_j){
	      close_elec=true;
	    }
	  }
	}
	if (close_elec && !removed){
	  removed=true;
	  continue;
	}
	isojets.push_back(jet);
      }
      // isolate leptons from jets
      foreach (Particle lep, leptons){ 
	bool close(false);
	foreach (Jet jet, isojets){
	  if (deltaR(lep.momentum(), jet.momentum())<_dR_l_j) {
	    close=true;
	  }
	}
	if (close) continue;
	isoleptons.push_back(lep);
      }
      foreach (Jet jet,isojets) {
	if (jet.containsBottom()) bjets.push_back(jet);
	else ljets.push_back(jet);
      }
      if (bjets.size()<2 || ljets.size()<2) return false;
      if (isoleptons.size()!=1) return false;
      return true;
    }
    
    bool SetJetPt(const Jets & alljets,Jets & njets){
      foreach (Jet jet,alljets) {
	if (jet.momentum().pT()<_pt_j) continue;
	njets.push_back(jet);
      }
      return true;
    }

    bool FillNumLep(const ParticleVector & allleptons, string num,
		    const double & weight){
      // fill number of leptons in event
      int n_e(0);
      int n_m(0);
      foreach (Particle lep, allleptons){
	if (abs(lep.pdgId())==11)
	  n_e++;
	else if (abs(lep.pdgId())==13)
	  n_m++;
      }
      _histos["n_elec_"+num] -> fill(n_e, weight);
      _histos["n_muon_"+num] -> fill(n_m, weight);
      _histos["n_lep_"+num]  -> fill(allleptons.size(), weight);
      return true;
    }
    
    bool FilterLeptons(const ParticleVector & prim_leptons,
		       ParticleVector & leptons,
		       const ParticleVector & visibles,
		       const ParticleVector & charged,
		       const vector<DressedLepton> & dressedelectrons) {
      // isolate leptons
      ParticleVector isocalo_elecs;
      ParticleVector isocalo_leps;
      foreach (DressedLepton cluster, dressedelectrons){
	if (!IsolateCalo(cluster, visibles)) continue;
	isocalo_elecs.push_back(cluster.constituentLepton());
      }
      foreach(Particle & elec, isocalo_elecs){
	isocalo_leps.push_back(elec);
      }
      foreach(Particle lep, prim_leptons){
	if (abs(lep.pdgId())==13) isocalo_leps.push_back(lep);
      }
      foreach (const Particle & lepton,isocalo_leps) {
	if (!IsolateTrack(lepton,charged)) continue;
	leptons.push_back(lepton);
      }
      return true;
    }

    bool IsolateCalo(DressedLepton cluster, const ParticleVector & visibles){
      double ptsum(0);
      FourMomentum ept(cluster.constituentLepton().momentum());
      double dressedept(ept.pT());
      foreach(Particle photon, cluster.constituentPhotons()){
	if (deltaR(photon.momentum(),ept)<_R_isoC_el)
	  dressedept+=photon.momentum().pT();
      }
      foreach(Particle part, visibles){
	if (deltaR(part.momentum(), ept)<_R_isoC_el)
	  ptsum+=part.momentum().pT();
      }
      if (ptsum>_pt_isoC_el+dressedept)
	return false;
      return true;
    }

    bool IsolateTrack(const Particle & lep,const ParticleVector & tracks) {
      double ptlep = lep.momentum().pT(), etalep = lep.momentum().eta();
      double ptiso(0.), dRiso(0.), pttrack(0.1), dR;
      if (abs(lep.pdgId())==11) {
	if (ptlep<_pt_el || fabs(etalep)>_eta_el) return false;
	ptiso = _pt_isoT_el/ptlep;
	dRiso = _R_isoT_el;
      }
      else if (abs(lep.pdgId())==13) {
	if (ptlep<_pt_mu || fabs(etalep)>_eta_mu) return false;
	ptiso = _pt_isoT_mu;
	dRiso = _R_isoT_mu/ptlep;
      }
      double pT_total =0.;
      foreach (Particle track,tracks) {
	dR = deltaR(lep.momentum(),track.momentum());
	if (dR<dRiso && dR>1.e-8){
	  pT_total += track.momentum().pT();
	}
      }
      if(pT_total/ptlep > ptiso) return false;
       return true;
    }
    
  
  public:    
    void finalize() {
      double scalefactor(crossSection()/sumOfWeights());

      //      std::cout << crossSection() << "\t" << sumOfWeights() << "\t" << scalefactor << std::endl;

      for (std::map<std::string, Histo1DPtr >::iterator hit=_histos.begin(); hit!=_histos.end();hit++){

	if(hit->first != "counterH")

	  //	  std::cout << (hit->second) -> integral() << std::endl;

	  scale(hit->second,scalefactor);

      }
    }
  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_ttH_TruthSel);

}
