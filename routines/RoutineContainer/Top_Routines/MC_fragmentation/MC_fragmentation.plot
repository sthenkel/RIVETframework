## BEGIN PLOT /MC_fragmentation/*
NormalizeToSum=1
LegendXPos=0.55
LegendYPos=
RatioPlotYMin=0.90
RatioPlotYMax=1.10
## END PLOT

## BEGIN PLOT /MC_fragmentation/charged_N
XLabel=stable charged particule multiplicity
YLabel=bin sum.
Title=stable ch. particles pt
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/charged_pT
XLabel=pt [GeV] 
YLabel=bin sum.
Title= stable ch. particles pt
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/charged_sumpT
XLabel=sum pt [GeV]
YLabel=bin sum.
Title= stable ch. particles sum pt
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT


## BEGIN PLOT /MC_fragmentation/bhadjet_N
XLabel=B-hadron matched jet multiplicity
YLabel=bin sum.
Title=N B-had matched jets
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/bhadjet_pT
XLabel=pT [GeV]
YLabel=bin sum.
Title=pT B-had matched jets
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/bhadron_N
XLabel=B-hadron multiplicity
YLabel=bin sum.
Title=N B-hadron
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/bhadron_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=B-hadron pt
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/gamma_N
XLabel=Gamma multiplicity
YLabel=bin sum.
Title=N gamma
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/gamma_pT
XLabel=pt [GeV]
YLabel=bin sum.
Title=Gamma pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/gamma_sumpT
XLabel=sum pt [GeV]
YLabel=bin sum.
Title=Gamma sum pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/muon_N
XLabel=Muon multiplicity
YLabel=bin sum.
Title=N muons
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/muon_pT
XLabel=pT [GeV]
YLabel=bin sum.
Title=Muon pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/muon_sumpT
XLabel=sum pT [GeV]
YLabel=bin sum.
Title=Muon sum pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/neutrino_N
XLabel=Neutrino multiplicity
YLabel=bin sum.
Title=N neutrino
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/neutrino_pT
XLabel=pT [GeV]
YLabel=bin sum.
Title=Neutrino pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT

## BEGIN PLOT /MC_fragmentation/neutrino_sumpT
XLabel=sum pT [GeV]
YLabel=bin sum.
Title=Neutrino sum pT
#LogY=1
#XMin=0
#XMax=100
#Rebin=1
## END PLOT