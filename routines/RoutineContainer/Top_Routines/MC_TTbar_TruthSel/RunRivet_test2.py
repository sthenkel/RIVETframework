
#include("GeneratorUtils/StdEvgenSetup.py")

theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()


InputFolder = "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/"
FileList = os.listdir(InputFolder)
NumberOfFiles = 10
for file in FileList[:NumberOfFiles]:
    if not "1.pool.root" in file:
        continue
    inputFile = InputFolder+file
    if os.path.exists(inputFile):
        svcMgr.EventSelector.InputCollections.append(inputFile)

rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
rivet.HistoFile = "myanalysis"
#rivet.MCEventKey
#rivet.CrossSection = 1
job += rivet

#svcMgr += THistSvc()
#svcMgr.THistSvc.Output = ["Rivet DATAFILE='myanalysis.root'  OPT='RECREATE'"]

