theApp.EvtMax = -1
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence

from GaudiSvc.GaudiSvcConf import THistSvc

#for grid submission, the InputCollection serves as dummy for inDS
svcMgr.EventSelector.InputCollections =[
    "/afs/cern.ch/user/s/sthenkel/eos/atlas/user/s/sthenkel/13TeV/MC/TOP_EVNT/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.evgen.EVNT.e3698/EVNT.06550123._024498.pool.root.1"
    ]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()

OutputRoot  = "Output_test.root"
OutputYoda  = "Output_test"

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i("Rivet")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_TTbar_TruthSel' ]
rivet.RunName = ""
rivet.HistoFile = OutputYoda
#rivet.MCEventKey
#rivet.CrossSection = 1
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"]
