#include "Rivet/Analysis.hh"
//#include "Rivet/RivetAIDA.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/Beam.hh"

// definitions for objects stolen from:
// https://rivet.hepforge.org/trac/browser/src/Analyses/ATLAS_2014_I1304688.cc

// toDo:
// - Add PseudoTop Definitions as defined in: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/ParticleLevelTopDefinitions
// - Add ttbar distributions (multiplicity, E, Et, pt, etc…) - Andrea  -----> done
// - tau polarization
// - spin correlation: delta phi zwischen den beiden leptonen wenn eine dilepton selektion angefragt wird, auch delta eta ll  --> done
// Add Overlap removal ---> Andrea, Done
// Add second leading lepton plots for dilepton!!!  ---> Andrea, done
// Add Whelicity plots for lepton+jets ----> Andrea, done
// Add top reconstruction for dilepton events! --> noch nicht verstanden warum Verteilungen asymmetrisch

// from: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/ParticleLevelTopDefinitions:
/*Single lepton channels (electron, muon):
  Exactly one selected electron or muon with |eta| < 2.4 and pT > 30 GeV
  Not any other lepton (electron or muon) with |eta| < 2.5 and pT > 15 GeV
  Neutrino sum pt > 30 GeV
  mT(W), defined as sqrt(2*pt(l)*pt(v)*(1-cos(phi(l)-phi(v)))), > 30 GeV
  At least two b-tagged jets in the region |eta| < 2.4 and pT > 30 GeV
  At least four jets in the region |eta| < 2.4 and pT > 30 GeV

 Dilepton channels (electron, muon):
  At least two selected leptons (e-e, e-mu, mu-mu) with |eta| < 2.4 and pT > 30 GeV
  For same-flavour channels neutrino sum pt > 60 GeV
  At least two b-tagged jets in the region |eta| < 2.4 and pT > 30 GeV
*/

/*
  Note Andrea toDo (25022015):
  * fix cosThetaStar, takes at the moment just one Wlep solution, not both of them!



 */


namespace Rivet {

  using namespace Cuts;

  class MC_TTbar_TruthSel : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    // WARNING: ISOCUTS and OVERLAPREMOVAL not yet implemented!!!

    /// Constructor
    MC_TTbar_TruthSel()
      : Analysis("MC_TTbar_TruthSel"),
	_ETmiss(20.), _ETmissMW(60.), _Njets(4),_Ntags(2),_Nlept(1),
	_pt_j(25.), _eta_j(2.5), _R_j(0.4), _MT(172.5), _MW(80.4),
	_reco_sig_MTlep(11.08), _reco_sig_MThad(9.864), _reco_sig_MW(10.57)
    {
      setNeedsCrossSection(false);

      std::cout << "READ STEERING FILE" << std::endl;

      ReadSteeringFile("Steering_TTbar_Truth");

      std::cout << Channel.c_str() << std::endl;

      _pt_el  = 25.0; // 30 before
      _eta_el =  2.5;
      _pt_mu  = 25.0; // 30 before
      _eta_mu =  2.5;


      if(Channel == "Semileptonic"){

	_Nlept  = 1;
	_Ntags  = 2;
	_Njets  = 4;
	_ETmiss = 30.0;
	_MTW    = 30.0;


      }
      else if(Channel == "Dileptonic"){

	_Nlept  = 2;
	_Ntags  = 2;
	_Njets  = 2;
	_ETmiss = 0.0; // before: 60 GeV

      }
      else if(Channel == "Allhadronic"){

	_Nlept  = 0;
        _Ntags  = 2;
        _Njets  = 6;

      }
      else{

	std::cout << "ERROR::This Channel is unknown, check the Steering file!!! ---> EXIT." << std::endl;

	exit(0);

      }


    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // definitions for objects stolen from:
      // https://rivet.hepforge.org/trac/browser/src/Analyses/ATLAS_2014_I1304688.cc

      // Eta ranges
      Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
      Cut eta_lep  = etaIn(-2.5, 2.5);

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);

      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");
      DressedLeptons dressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 30.0*GeV), true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons vetodressedelectrons(photons, electrons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
      addProjection(vetodressedelectrons, "vetodressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, true, eta_full, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");

      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");
      vector<pair<double, double> > eta_muon;
      DressedLeptons dressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 30.0*GeV), true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons vetodressedmuons(photons, muons, 0.1, true, eta_lep & (pT >= 15.0*GeV), true);
      addProjection(vetodressedmuons, "vetodressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, true, eta_full, true);
      addProjection(ewdressedmuons, "ewdressedmuons");

      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4);
      jets.useInvisibles();
      addProjection(jets, "jets");

      IdentifiedFinalState taus;
      taus.acceptIdPair(PID::TAU);
      addProjection(taus, "Taus");

      // book histograms

      // jets:

      // phi distributions at the moment from -2pi to 2pi since not clear yet, why the distributions changed in the new rivet versions

      // define asymmetric bins for pt and mass distributions until 1000.0
      std::vector< double > binedges;
      binedges.push_back(0.0);     binedges.push_back(50.0);  binedges.push_back(100.0);  binedges.push_back(150.0);
      binedges.push_back(200.0);   binedges.push_back(250.0); binedges.push_back(350.0);  binedges.push_back(800.0);
      binedges.push_back(1500.0);

      std::vector< double > binedges2;
      binedges2.push_back(-4.50); binedges2.push_back(-3.50); binedges2.push_back(-2.50); binedges2.push_back(-1.75);
      binedges2.push_back(-1.25); binedges2.push_back(-0.75); binedges2.push_back(-0.25); binedges2.push_back(0.25);
      binedges2.push_back(0.75);  binedges2.push_back(1.25);  binedges2.push_back(1.75);  binedges2.push_back(2.50);
      binedges2.push_back(3.50);  binedges2.push_back(4.50);

      std::vector< double > binedges3;
      binedges3.push_back(0.0);   binedges3.push_back(50.0);  binedges3.push_back(100.0);  binedges3.push_back(150.0);
      binedges3.push_back(200.0); binedges3.push_back(250.0); binedges3.push_back(350.0);  binedges3.push_back(400.0);
      binedges3.push_back(500.0); binedges3.push_back(700.0);

      std::vector< double > binedges_top;
      binedges_top.push_back(120.0);  binedges_top.push_back(140.0); binedges_top.push_back(150.0); binedges_top.push_back(155.0); binedges_top.push_back(160.0);
      binedges_top.push_back(165.0);  binedges_top.push_back(170.0); binedges_top.push_back(175.0); binedges_top.push_back(180.0); binedges_top.push_back(185.0);
      binedges_top.push_back(190.0);  binedges_top.push_back(195.0); binedges_top.push_back(205.0); binedges_top.push_back(225.0);

      std::vector< double > binedges_W;
      binedges_W.push_back(33.0);     binedges_W.push_back(53.0);     binedges_W.push_back(63.0);     binedges_W.push_back(68.0);
      binedges_W.push_back(73.0);     binedges_W.push_back(78.0);     binedges_W.push_back(83.0);     binedges_W.push_back(88.0);
      binedges_W.push_back(93.0);     binedges_W.push_back(98.0);     binedges_W.push_back(108.0);    binedges_W.push_back(128.0);

      std::vector< double > binedges_ttbar;
      binedges_ttbar.push_back(250.0); binedges_ttbar.push_back(275.0); binedges_ttbar.push_back(300.0); binedges_ttbar.push_back(325.0);
      binedges_ttbar.push_back(350.0); binedges_ttbar.push_back(400.0); binedges_ttbar.push_back(450.0); binedges_ttbar.push_back(500.0);
      binedges_ttbar.push_back(575.0); binedges_ttbar.push_back(650.0); binedges_ttbar.push_back(725.0); binedges_ttbar.push_back(900.0);

      _h_jet_pT     = bookHisto1D("jet_pT",     binedges);
      _h_jet_eta    = bookHisto1D("jet_eta",    binedges2);
      _h_jet_phi    = bookHisto1D("jet_phi",    50, -6.3, 6.3);

      _h_bjet_pT    = bookHisto1D("bjet_pT",    binedges);
      _h_bjet_eta   = bookHisto1D("bjet_eta",   binedges2);
      _h_bjet_phi   = bookHisto1D("bjet_phi",   50, -6.3, 6.3);

      _h_ljet_pT    = bookHisto1D("ljet_pT",    binedges);
      _h_ljet_eta   = bookHisto1D("ljet_eta",   binedges2);
      _h_ljet_phi   = bookHisto1D("ljet_phi",   50, -6.3, 6.3);

      _h_jet_1_pT   = bookHisto1D("jet_1_pT",   binedges);
      _h_jet_1_eta  = bookHisto1D("jet_1_eta",  binedges2);
      _h_jet_2_pT   = bookHisto1D("jet_2_pT",   binedges);
      _h_jet_2_eta  = bookHisto1D("jet_2_eta",  binedges2);
      _h_jet_3_pT   = bookHisto1D("jet_3_pT",   binedges);
      _h_jet_3_eta  = bookHisto1D("jet_3_eta",  binedges2);
      _h_jet_4_pT   = bookHisto1D("jet_4_pT",   binedges);
      _h_jet_4_eta  = bookHisto1D("jet_4_eta",  binedges2);

      _h_bjet_1_pT  = bookHisto1D("bjet_1_pT",  binedges);
      _h_bjet_1_eta = bookHisto1D("bjet_1_eta", binedges2);
      _h_bjet_2_pT  = bookHisto1D("bjet_2_pT",  binedges);
      _h_bjet_2_eta = bookHisto1D("bjet_2_eta", binedges2);

      _h_ljet_1_pT  = bookHisto1D("ljet_1_pT",  binedges);
      _h_ljet_1_eta = bookHisto1D("ljet_1_eta", binedges2);
      _h_ljet_2_pT  = bookHisto1D("ljet_2_pT",  binedges);
      _h_ljet_2_eta = bookHisto1D("ljet_2_eta", binedges2);

      _h_njet_10    = bookHisto1D("njet_10",    31, -0.5, 30.5);
      _h_njet_25    = bookHisto1D("njet_25",    21, -0.5, 20.5);
      _h_nbjet_25   = bookHisto1D("nbjet_25",   21, -0.5, 20.5);
      _h_nljet_25   = bookHisto1D("nljet_25",   21, -0.5, 20.5);

      _h_M_ttbar    = bookHisto1D("M_ttbar",      binedges_ttbar);
      _h_pT_ttbar   = bookHisto1D("pT_ttbar",     binedges);
      _h_eta_ttbar  = bookHisto1D("eta_ttbar",    binedges2);
      _h_phi_ttbar  = bookHisto1D("phi_ttbar",    100, -6.3, 6.3);


      if(Channel != "Allhadronic"){

	//leptons:
	_h_elec_pT    = bookHisto1D("elec_pT",   binedges);
	_h_elec_eta   = bookHisto1D("elec_eta",  binedges2);
	_h_elec_phi   = bookHisto1D("elec_phi",  100, -6.3,  6.3);
	_h_elec_N     = bookHisto1D("elec_N",     11, -0.5, 10.5);

	_h_mu_pT      = bookHisto1D("mu_pT",     binedges);
	_h_mu_eta     = bookHisto1D("mu_eta",    binedges2);
	_h_mu_phi     = bookHisto1D("mu_phi",    100, -6.3,  6.3);
	_h_mu_N       = bookHisto1D("mu_N",       11, -0.5, 10.5);

	// leading lepton pt, eta, phi
	_h_lep1_pt      = bookHisto1D("lep1_pT",  binedges);
        _h_lep1_eta     = bookHisto1D("lep1_eta", binedges2);
        _h_lep1_phi     = bookHisto1D("lep1_phi", 100, -6.3, 6.3);


	// second leading lepton pt, eta, phi
	if(Channel == "Dileptonic"){
	  _h_lep2_pt       = bookHisto1D("lep2_pT",  binedges);
	  _h_lep2_eta      = bookHisto1D("lep2_eta", binedges2);
	  _h_lep2_phi      = bookHisto1D("lep2_phi", 100, -6.3, 6.3);
	  _h_dilep_channel = bookHisto1D("dilep_channel", 3, -0.5, 2.5);
	}

	_h_tau_pT     = bookHisto1D("tau_pT",     binedges);
	_h_tau_eta    = bookHisto1D("tau_eta",    binedges2);
	_h_tau_phi    = bookHisto1D("tau_phi",    100, -6.3,  6.3);
	_h_tau_N      = bookHisto1D("tau_N",       11, -0.5, 10.5);

	// MET
	_h_MET        = bookHisto1D("MET",        binedges);

	if(Channel == "Semileptonic"){

	  _h_MTW        = bookHisto1D("MTW",        150,    0,  150);
	  _h_chi2_hadW  = bookHisto1D("chi2_hadW",    20,    0.,  50.);  // abs(MW^2-(reconstructed MW)^2)/sig_MW^2
	  _h_chi2_hadT  = bookHisto1D("chi2_hadT",    20,    0.,  50.);  // abs(Mt^2-(reconstructed Mt)^2)/sig_had_Mt^2
	  _h_chi2_lepT  = bookHisto1D("chi2_lepT",    20,    0.,  50.);  // abs(Mt^2-(reconstructed Mt)^2)/sig_lep_Mt^2
	  _h_M_hadW     = bookHisto1D("M_hadW",      binedges_W);   // mass of hadronic W
	  _h_M_lepW     = bookHisto1D("M_lepW",      binedges_W);
	  _h_M_hadT     = bookHisto1D("M_hadT",      binedges_top);  // mass of hadronic t
	  _h_M_lepT     = bookHisto1D("M_lepT",      binedges_top);  // mass of leptonic t
	  _h_pT_lepT    = bookHisto1D("pT_lepT",     binedges);
	  _h_pT_hadT    = bookHisto1D("pT_hadT",     binedges);
	  _h_pT_lepW    = bookHisto1D("pT_lepW",     binedges);
	  _h_pT_hadW    = bookHisto1D("pT_hadW",     binedges);
	  _h_eta_lepT   = bookHisto1D("eta_lepT",    binedges2);
	  _h_eta_hadT   = bookHisto1D("eta_hadT",    binedges2);
	  _h_eta_lepW   = bookHisto1D("eta_lepW",    binedges2);
	  _h_eta_hadW   = bookHisto1D("eta_hadW",    binedges2);

	  _h_eta_T1     = bookHisto1D("eta_T1",      binedges2);
	  _h_pT_T1      = bookHisto1D("pT_T1",       binedges3);
	  _h_eta_T2     = bookHisto1D("eta_T2",      binedges2);
          _h_pT_T2      = bookHisto1D("pT_T2",       binedges3);

	  _h_semilep_channel = bookHisto1D("semilep_channel", 3, -0.5, 2.5);

	}

	_h_cos_theta  = bookHisto1D("cos_theta",    15, -1.0, 1.0);

	if(Channel == "Dileptonic"){

	  _h_DeltaPhi   = bookHisto1D("delta_phi_ll", 100, 0.0, 6.3);
	  _h_DeltaEta   = bookHisto1D("delta_eta_ll", 100, 0.0, 5.0);
	  _h_M_dilW     = bookHisto1D("M_dilW",       binedges_W);
	  _h_M_dilTop   = bookHisto1D("M_dilTop",     binedges_top);
	  _h_pt_dilW    = bookHisto1D("pt_dilW",      binedges);
	  _h_pt_dilTop  = bookHisto1D("pt_dilTop",    binedges);
	  _h_eta_dilW   = bookHisto1D("eta_dilW",     binedges2);
	  _h_eta_dilTop = bookHisto1D("eta_dilTop",   binedges2);
	  _h_phi_dilW   = bookHisto1D("phi_dilW",     100, -6.3, 6.3);
          _h_phi_dilTop = bookHisto1D("phi_dilTop",   100, -6.3, 6.3);

	}

      }
      else{

	_h_M_hadW     = bookHisto1D("M_hadW",      binedges_W);   // mass of hadronic W
	_h_M_hadT     = bookHisto1D("M_hadT",      binedges_top);  // mass of hadronic t
	_h_pT_hadT    = bookHisto1D("pT_hadT",     binedges);
	_h_pT_hadW    = bookHisto1D("pT_hadW",     binedges);
	_h_eta_hadT   = bookHisto1D("eta_hadT",    binedges2);
	_h_eta_hadW   = bookHisto1D("eta_hadW",    binedges2);


      }

    } // end init


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double   weight = event.weight();

      //      const PdgIdPair beams = Rivet::beamIds(event);

      //      std::cout << beams.first << "\t" << beams.second << std::endl;

      FourMomentum lepton, gamma;
      FourMomentum p_miss(0.,0.,0.,0.);

      // Get the selected objects, using the projections.
      _dressedelectrons     = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
      _vetodressedelectrons = applyProjection<DressedLeptons>(event, "vetodressedelectrons").dressedLeptons();
      _dressedmuons         = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
      _vetodressedmuons     = applyProjection<DressedLeptons>(event, "vetodressedmuons").dressedLeptons();
      _neutrinos            = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();
      const Jets alljets_10 = applyProjection<FastJets>(event, "jets").jetsByPt(10.0, MAXDOUBLE, -2.5, 2.5);

      //      const FinalState&       taus = applyProjection<FinalState>(event, "Taus");

      //      const Particles taus = applyProjection<TauFinder>(event, "Taus").particlesByPt();

      _neu_all = applyProjection<FinalState>(event, "neutrinos").particles();

      // select only good electrons
      ParticleVector good_elecs;
      SelectGoodElectrons(_dressedelectrons, good_elecs);

      ParticleVector good_muons;
      SelectGoodMuons(_dressedmuons, good_muons);

      ParticleVector good_leptons;
      good_leptons = good_elecs+good_muons;

      if(!SelectGoodJets(alljets_10, good_bjets, good_ljets, good_jets))
        vetoEvent;

      // also stolen from ttbar+jets routine above
      // Check overlap of jets/leptons.
      unsigned int i,j;
      _jet_ntag = 0;
      bool _overlap = false;
      for (i = 0; i < good_jets.size(); i++) {
        const Jet& jet = good_jets[i];
        // If dR(el,jet) < 0.4 skip the event
        foreach (const Particle& el, good_elecs) {
          if (deltaR(jet, el) < 0.4) _overlap = true;
        }
        // If dR(mu,jet) < 0.4 skip the event
        foreach (const Particle& mu, good_muons) {
          if (deltaR(jet, mu) < 0.4) _overlap = true;
        }
        // If dR(jet,jet) < 0.5 skip the event
        for (j = 0; j < good_jets.size(); j++) {
          const Jet& jet2 = good_jets[j];
          if (i == j) continue; // skip the diagonal
          if (deltaR(jet, jet2) < 0.5) _overlap = true;
        }
        // Count the number of b-tags
        if (!jet.bTags().empty()) _jet_ntag += 1;
      }


      // Remove events with object overlap
      if (_overlap) vetoEvent;

      int nGoodEle = good_elecs.size();
      int nGoodMu  = good_muons.size();

      if(nGoodEle+nGoodMu != _Nlept)
        vetoEvent;

      // Calculate and fill missing Et histos

      if(_Nlept != 0){

	lepton = good_leptons[0];

	foreach(const Particle& p, _neutrinos) p_miss+=p.momentum();

	_met_et = p_miss.Et()/GeV;

	_h_elec_N -> fill(_dressedelectrons.size(), weight);
	_h_mu_N   -> fill(_dressedmuons.size(),     weight);
	//  _h_tau_N  -> fill(taus.size(),              weight);

      }

      // when single lepton event: MET coming from the leptonic side, used for reconstruction
      if(nGoodEle+nGoodMu == 1){

	_plep   = lepton;

	// Evaluate basic event selection
	unsigned int ejets_bits = 0, mujets_bits = 0;
	bool pass_ejets  = _ejets(ejets_bits);
	bool pass_mujets = _mujets(mujets_bits);

	MTW = _transMass(good_leptons[0].momentum().pT()/GeV, good_leptons[0].phi(), p_miss.Et()/GeV, p_miss.phi());

	if(!pass_ejets && !pass_mujets)
	  vetoEvent;

        if (!GetWlep(lepton, p_miss, weight))
          vetoEvent;

	if(!GetWhad(good_ljets,weight))
          vetoEvent;

        if(!ReconstructTops(good_bjets, _Wlep_fin[0], weight))
          vetoEvent;

	FillCosThetaStar(_tlep, _Wlep_fin[0], good_leptons[0], weight);

	if(_Wlep_fin[1].pz() != 0){

	  if(!ReconstructTops(good_bjets, _Wlep_fin[1], weight))
	    vetoEvent;

	  FillCosThetaStar(_tlep, _Wlep_fin[1], good_leptons[0], weight);

	}

	if(pass_ejets)
	  _h_semilep_channel -> fill(0, weight);
	if(pass_mujets)
          _h_semilep_channel -> fill(1, weight);


	_h_lep1_pt  -> fill(good_leptons[0].momentum().pT()/GeV,  weight);
        _h_lep1_eta -> fill(good_leptons[0].momentum().eta()/GeV, weight);
	_h_lep1_phi -> fill(good_leptons[0].momentum().phi()/GeV, weight);

        _h_MTW -> fill(MTW, weight);

      }
      else if(nGoodEle+nGoodMu == 2){

	unsigned int ee_bits = 0, mumu_bits = 0, emu_bits = 0;

	bool p_ee   = _pass_ee(ee_bits);
	bool p_mumu = _pass_mumu(mumu_bits);
        bool p_emu  = _pass_emu(emu_bits);

	if(!p_ee && !p_mumu && !p_emu)
	  vetoEvent;

	if(p_ee)
          _h_dilep_channel -> fill(0, weight);
        if(p_mumu)
          _h_dilep_channel -> fill(1, weight);
	if(p_emu)
          _h_dilep_channel -> fill(2, weight);

        // fill leading and subleading lepton

        _h_lep1_pt  -> fill(good_leptons[0].momentum().pT()/GeV,  weight);
        _h_lep1_eta -> fill(good_leptons[0].momentum().eta()/GeV, weight);
        _h_lep1_phi -> fill(good_leptons[0].momentum().phi()/GeV, weight);

        _h_lep2_pt  -> fill(good_leptons[1].momentum().pT()/GeV,  weight);
        _h_lep2_eta -> fill(good_leptons[1].momentum().eta()/GeV, weight);
        _h_lep2_phi -> fill(good_leptons[1].momentum().phi()/GeV, weight);

        // calculate delta phi and eta of leptons

        float delta_eta = deltaEta(good_leptons[0], good_leptons[1]);
        float delta_phi = deltaPhi(good_leptons[0], good_leptons[1]);

        _h_DeltaEta -> fill(delta_eta, weight);
        _h_DeltaPhi -> fill(delta_phi, weight);

	// now run the event reconstruction!

	ReconstructDileptonW(good_leptons, _neutrinos, weight);

      }
      else if(nGoodEle+nGoodMu == 0){

	if(good_ljets.size() < 4)
	  vetoEvent;
	if(good_bjets.size() < 2)
	  vetoEvent;

	//	std::cout << "allhad reco " << weight << std::endl;

	ReconstructAllhadW(good_ljets,    weight);
	ReconstructTopsAllhad(good_bjets, weight);

      }


      if (nGoodEle > 0)
        {
          lepton=good_elecs[0].momentum();
          _h_elec_pT  -> fill(lepton.pT()/GeV, weight);
          _h_elec_eta -> fill(lepton.eta(),    weight);
	  _h_elec_phi -> fill(lepton.phi(),    weight);
	}
      if (nGoodMu > 0)
	{
	  lepton=good_muons[0].momentum();
	  _h_mu_pT  -> fill(lepton.pT()/GeV, weight);
	  _h_mu_eta -> fill(lepton.eta(),    weight);
	  _h_mu_phi -> fill(lepton.phi(),    weight);
	}

      int n10 = alljets_10.size();
      int n25 = good_jets.size();

      _h_njet_10->fill(n10,weight);
      _h_njet_25->fill(n25,weight);

      int nGoodJets = good_jets.size();
      int nBJets    = good_bjets.size();
      int nLJets    = good_ljets.size();

      _h_nbjet_25   -> fill(nBJets, weight);
      _h_nljet_25   -> fill(nLJets, weight);

      if(good_jets.size() > 0){
	_h_jet_1_pT  -> fill(good_jets[0].momentum().pT()/GeV, weight);
	_h_jet_1_eta -> fill(good_jets[0].momentum().eta(),    weight);
      }
      if(good_jets.size() > 1){
	_h_jet_2_pT  -> fill(good_jets[1].momentum().pT()/GeV, weight);
	_h_jet_2_eta -> fill(good_jets[1].momentum().eta(),    weight);
      }
      if(good_jets.size() > 2){
	_h_jet_3_pT  -> fill(good_jets[2].momentum().pT()/GeV, weight);
	_h_jet_3_eta -> fill(good_jets[2].momentum().eta(),    weight);
      }
      if(good_jets.size() > 3){
	_h_jet_4_pT  -> fill(good_jets[3].momentum().pT()/GeV, weight);
	_h_jet_4_eta -> fill(good_jets[3].momentum().eta(),    weight);
      }

      if(good_bjets.size() > 0){
        _h_bjet_1_pT  -> fill(good_bjets[0].momentum().pT()/GeV, weight);
        _h_bjet_1_eta -> fill(good_bjets[0].momentum().eta(),    weight);
      }
      if(good_bjets.size() > 1){
        _h_bjet_2_pT  -> fill(good_bjets[1].momentum().pT()/GeV, weight);
        _h_bjet_2_eta -> fill(good_bjets[1].momentum().eta(),    weight);
      }

      if(good_ljets.size() > 0){
        _h_ljet_1_pT  -> fill(good_ljets[0].momentum().pT()/GeV, weight);
        _h_ljet_1_eta -> fill(good_ljets[0].momentum().eta(),    weight);
      }
      if(good_ljets.size() > 1){
        _h_ljet_2_pT  -> fill(good_ljets[1].momentum().pT()/GeV, weight);
        _h_ljet_2_eta -> fill(good_ljets[1].momentum().eta(),    weight);
      }


      for(int iJet = 0; iJet < nGoodJets; ++iJet){

	_h_jet_pT  -> fill(good_jets[iJet].momentum().pT()/GeV, weight);
        _h_jet_eta -> fill(good_jets[iJet].momentum().eta(),    weight);
	_h_jet_phi -> fill(good_jets[iJet].momentum().phi(),    weight);

      }

      for(int iJet = 0;iJet < nBJets; ++iJet){

        _h_bjet_pT  -> fill(good_bjets[iJet].momentum().pT()/GeV, weight);
        _h_bjet_eta -> fill(good_bjets[iJet].momentum().eta(),    weight);
	_h_bjet_phi -> fill(good_bjets[iJet].momentum().phi(),    weight);

      }

      for(int iJet = 0;iJet < nLJets; ++iJet){

        _h_ljet_pT  -> fill(good_ljets[iJet].momentum().pT()/GeV, weight);
        _h_ljet_eta -> fill(good_ljets[iJet].momentum().eta(),    weight);
        _h_ljet_phi -> fill(good_ljets[iJet].momentum().phi(),    weight);

      }


      if(Channel != "Allhadronic"){

	_h_MET -> fill(_met_et, weight);

      }
      else if(Channel == "Semileptonic"){

	_h_MTW -> fill(MTW,     weight);

      }

    }


    /// Normalise histograms etc., after the run
    void finalize() {
      //double normfac=crossSection()/sumOfWeights();
      double normfac=1;

      std::cout << "NORMALIZE !!!!!! " << normfac << std::endl;

      scale(_h_jet_pT,     normfac);
      scale(_h_jet_eta,    normfac);
      scale(_h_jet_phi,    normfac);
      scale(_h_bjet_pT,    normfac);
      scale(_h_bjet_eta,   normfac);
      scale(_h_bjet_phi,   normfac);
      scale(_h_ljet_pT,    normfac);
      scale(_h_ljet_eta,   normfac);
      scale(_h_ljet_phi,   normfac);
      scale(_h_jet_1_pT,   normfac);
      scale(_h_jet_1_eta,  normfac);
      scale(_h_jet_2_pT,   normfac);
      scale(_h_jet_2_eta,  normfac);
      scale(_h_jet_3_pT,   normfac);
      scale(_h_jet_3_eta,  normfac);
      scale(_h_jet_4_pT,   normfac);
      scale(_h_jet_4_eta,  normfac);
      scale(_h_bjet_1_pT,  normfac);
      scale(_h_bjet_1_eta, normfac);
      scale(_h_bjet_2_pT,  normfac);
      scale(_h_bjet_2_eta, normfac);
      scale(_h_ljet_1_pT,  normfac);
      scale(_h_ljet_1_eta, normfac);
      scale(_h_ljet_2_pT,  normfac);
      scale(_h_ljet_2_eta, normfac);
      scale(_h_njet_10,    normfac);
      scale(_h_njet_25,    normfac);
      scale(_h_nbjet_25,   normfac);
      scale(_h_nljet_25,   normfac);

      scale(_h_pT_ttbar,   normfac);
      scale(_h_eta_ttbar,  normfac);
      scale(_h_phi_ttbar,  normfac);
      scale(_h_M_ttbar,    normfac);
      scale(_h_cos_theta,  normfac);

      if(Channel != "Allhadronic"){

	scale(_h_elec_pT,    normfac);
	scale(_h_elec_eta,   normfac);
	scale(_h_elec_phi,   normfac);
	scale(_h_elec_N,     normfac);
	scale(_h_mu_pT,      normfac);
	scale(_h_mu_eta,     normfac);
	scale(_h_mu_phi,     normfac);
	scale(_h_mu_N,       normfac);

	scale(_h_lep1_pt,    normfac);
        scale(_h_lep1_eta,   normfac);
        scale(_h_lep1_phi,   normfac);

	if(Channel == "Dileptonic"){
	  scale(_h_lep2_pt,      normfac);
	  scale(_h_lep2_eta,     normfac);
	  scale(_h_lep2_phi,     normfac);
	}

	scale(_h_tau_pT,     normfac);
	scale(_h_tau_eta,    normfac);
	scale(_h_tau_phi,    normfac);
	scale(_h_tau_N,      normfac);
	scale(_h_MET,        normfac);

	if(Channel == "Semileptonic"){
	  scale(_h_MTW,             normfac);
	  scale(_h_chi2_hadW,       normfac);
	  scale(_h_chi2_hadT,       normfac);
	  scale(_h_chi2_lepT,       normfac);
	  scale(_h_M_hadW,          normfac);
	  scale(_h_M_hadT,          normfac);
	  scale(_h_M_lepW,          normfac);
	  scale(_h_M_lepT,          normfac);
	  scale(_h_pT_lepT,         normfac);
	  scale(_h_eta_lepT,        normfac);
	  scale(_h_pT_hadT,         normfac);
	  scale(_h_eta_hadT,        normfac);
	  scale(_h_pT_lepW,         normfac);
	  scale(_h_eta_lepW,        normfac);
	  scale(_h_pT_hadW,         normfac);
	  scale(_h_eta_hadW,        normfac);
	  scale(_h_semilep_channel, normfac);
	  scale(_h_eta_T1,          normfac);
	  scale(_h_pT_T1,           normfac);
	  scale(_h_eta_T2,          normfac);
          scale(_h_pT_T2,           normfac);
	}

	scale(_h_cos_theta,  normfac);

	if(Channel == "Dileptonic"){
	  scale(_h_DeltaEta,      normfac);
	  scale(_h_DeltaPhi,      normfac);
	  scale(_h_M_dilW,        normfac);
	  scale(_h_M_dilTop,      normfac);
	  scale(_h_pt_dilW,       normfac);
          scale(_h_pt_dilTop,     normfac);
	  scale(_h_eta_dilW,      normfac);
          scale(_h_eta_dilTop,    normfac);
	  scale(_h_phi_dilW,      normfac);
          scale(_h_phi_dilTop,    normfac);
	  scale(_h_dilep_channel, normfac);
	}
      }
      else{

	std::cout << "scale all had events!!!" << std::endl;

	scale(_h_M_hadW,          normfac);
	scale(_h_M_hadT,          normfac);
	scale(_h_pT_hadT,         normfac);
	scale(_h_eta_hadT,        normfac);
	scale(_h_pT_hadW,         normfac);
	scale(_h_eta_hadW,        normfac);


      }

    }


    //@}


  private:

    std::string Channel;

    Jets good_bjets, good_ljets, good_jets;

    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _vetodressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _vetodressedmuons;
    Particles _neutrinos;
    //  Jets _jets;
    unsigned int _jet_ntag;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi;

    double _pt_el, _eta_el, _pt_mu, _eta_mu, _eta_C, _pt_lep, _eta_lep;
    double _pt_isoT_mu, _R_isoT_mu, _pt_isoT_el, _R_isoT_el;
    double _pt_isoC_mu, _R_isoC_mu, _pt_isoC_el, _R_isoC_el;
    double _ETmiss, _ETmissMW;
    int    _Njets,_Ntags,_Nlept;
    double _pt_j, _eta_j, _R_j, _R_j_mu;
    double _MT, _MW, _dR_e_j, _dR_l_j;
    double _reco_sig_MTlep, _reco_sig_MThad, _reco_sig_MW;
    double MTW,_MTW;

    size_t       _b_thad, _b_tlep, _l_W1, _l_W2, _take;
    FourMomentum _plep, _pmiss, _Wlep[2], _Wlep_fin[2], _Wlepwin, _Whad, _tlep, _thad,_ttbar,_Whad1,_Whad2;
    FourMomentum _pneus[2];
    ParticleVector _neu_all;

    double _transMass(double ptLep, double phiLep, double met, double phiMet) {
      return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
    }

    bool SelectGoodElectrons(vector<DressedLepton> &ele, ParticleVector &good_ele){

      good_ele.clear();

      foreach(DressedLepton electron, ele){

	if(electron.momentum().pT() < _pt_el)
	  continue;

	if(electron.momentum().eta() > _eta_el)
	  continue;

	good_ele.push_back(electron);

      }

      return true;

    }

    bool SelectGoodMuons(vector<DressedLepton> &mu, ParticleVector &good_mu){

      good_mu.clear();

      foreach(DressedLepton muon, mu){

        if(muon.momentum().pT() < _pt_mu)
          continue;

        if(muon.momentum().eta() > _eta_mu)
          continue;

	good_mu.push_back(muon);

      }

      return true;

    }

    bool SelectGoodJets(const Jets & alljets, Jets &bjets, Jets &ljets, Jets &all_good){

      bjets.clear();
      ljets.clear();
      all_good.clear();

      foreach (Jet jet, alljets){
        if (jet.momentum().pT()<_pt_j || fabs(jet.momentum().eta())>_eta_j)
	  continue; // minimum pT and maximum eta jet cuts

	all_good.push_back(jet);

	if (jet.containsBottom()) bjets.push_back(jet);
	else ljets.push_back(jet);

      }

      // cut on number of jets and bjets!

      int Ngood = all_good.size();
      int Nb    = bjets.size();

      if(Ngood < _Njets)
	return false;

      if(Nb   < _Ntags)
	return false;

      return true;

    }

    bool passMETcut(float met_val, float cut){

      if(met_val < cut)
	return false;

      return true;

    }

    // from ttbb code of ttH group, written by Durham

    bool GetWlep(FourMomentum lepton, FourMomentum p_miss, float weight) {

      // from pseudo top: Define the leptonic W by combining the lepton with the ETmiss and solving for pz assuming the W mass (highest pz from two-fold ambiguity)

      double pz1  = 0.0;
      double pz2  = 0.0;
      double px_c = lepton.px(); double py_c = lepton.py(); double pz_c = lepton.pz(); double Ec   = lepton.E();

      double mLep = 0.0;

      double px_nu = p_miss.px();
      double py_nu = p_miss.py();
      double alpha = _MW*_MW - mLep*mLep + 2*(px_c*px_nu + py_c*py_nu);

      double a = pz_c*pz_c - Ec*Ec;
      double b = alpha* pz_c;
      double c = - Ec*Ec* (px_nu*px_nu + py_nu*py_nu) + alpha*alpha/4.;

      //      std::cout << px_c << "\t" << px_nu << std::endl;

      double discriminant = b*b - 4*a*c;
      if (discriminant < 0.)
	return false;

      double pz_offset = - b / (2*a);

      double squareRoot = sqrt(discriminant);
      if(squareRoot < 1.e-6)
	pz1 = pz_offset;
      else{

	pz1 = pz_offset + squareRoot / (2*a);
	pz2 = pz_offset - squareRoot / (2*a);

      }

      double Energy1 = sqrt(px_nu*px_nu+py_nu*py_nu+pz1*pz1);
      double Energy2 = sqrt(px_nu*px_nu+py_nu*py_nu+pz2*pz2);

      FourMomentum nu1;  nu1.setPx(px_nu);  nu1.setPy(py_nu);  nu1.setPz(pz1);  nu1.setE(Energy1);
      FourMomentum nu2;  nu2.setPx(px_nu);  nu2.setPy(py_nu);  nu2.setPz(pz2);  nu2.setE(Energy2);

      _Wlep_fin[0] = lepton+nu1;

      _h_M_lepW   -> fill(_Wlep_fin[0].mass(),weight);
      _h_pT_lepW  -> fill(_Wlep_fin[0].pT(),  weight);
      _h_eta_lepW -> fill(_Wlep_fin[0].eta(), weight);

      if(squareRoot > 1.e-6){

	_Wlep_fin[1] = lepton+nu2;

	_h_M_lepW   -> fill(_Wlep_fin[1].mass(), weight);
	_h_pT_lepW  -> fill(_Wlep_fin[1].pT(),   weight);
	_h_eta_lepW -> fill(_Wlep_fin[1].eta(),  weight);

      }

      return true;

    }

    bool GetWhad(const Jets & ljets, float weight) {
      if (ljets.size()<2) {
        return false;
      }

      // make Whad from light jets which give the closest mass to the W mass (not 100% pseudo top definition!!!)

      double minchi2(1.e12),chi2;

      for (size_t i=0; i<ljets.size()-1; i++) {
        for (size_t j=i+1; j<ljets.size(); j++) {

	  FourMomentum Whelp = ljets[i].momentum() + ljets[j].momentum();

          chi2 = fabs(_MW*_MW - Whelp.mass()*Whelp.mass())/ sqr(_reco_sig_MW);

	  //	  std::cout << _MW << "\t" << (ljets[i].momentum() + ljets[j].momentum()).mass() << "\t" << chi2  << std::endl;

          if (chi2<minchi2) {

            _l_W1 = i; _l_W2 = j; minchi2 = chi2;

          }
        }
      }

      // std::cout << "\t" << std::endl;

      _Whad = ljets[_l_W1].momentum()+ljets[_l_W2].momentum();
      _h_chi2_hadW -> fill(minchi2,weight);
      _h_M_hadW    -> fill(_Whad.mass(), weight);
      _h_pT_hadW   -> fill(_Whad.pT(),   weight);
      _h_eta_hadW  -> fill(_Whad.eta(),  weight);

      return true;
    }

    bool ReconstructTops(const Jets & bjets, const FourMomentum Wlep, const double & weight) {
      if (bjets.size()!=2) {
        return false;
      }

      // not 100% pseudo top definition yet!!!

      double minchi2(1.e12),chi2lep,chi2had;
      double minchi2lep(1.e12),minchi2had(1.e12);
      for (size_t i=0;i<bjets.size();i++) {
        for (size_t j=0;j<bjets.size();j++) {

          if (i==j) continue;

	  chi2lep = fabs(_MT*_MT-sqr((bjets[i].momentum()+Wlep).mass()))/sqr(_reco_sig_MTlep);
          chi2had = fabs(_MT*_MT-sqr((bjets[j].momentum()+_Whad).mass()))/sqr(_reco_sig_MThad);

          if (chi2had*chi2lep<minchi2) {
            _b_tlep    = i; _b_thad = j;
            minchi2    = chi2lep*chi2had;
            minchi2lep = chi2lep;
            minchi2had = chi2had;
            _Wlepwin   = Wlep;
          }
        }
      }
      _tlep  = bjets[_b_tlep].momentum()+_Wlepwin;
      _thad  = bjets[_b_thad].momentum()+_Whad;
      _ttbar = _tlep+_thad;

      _h_chi2_lepT -> fill(minchi2lep,    weight);
      _h_chi2_hadT -> fill(minchi2had,    weight);
      _h_M_lepT    -> fill(_tlep.mass(),  weight);
      _h_M_hadT    -> fill(_thad.mass(),  weight);
      _h_pT_lepT   -> fill(_tlep.pT(),    weight);
      _h_eta_lepT  -> fill(_tlep.eta(),   weight);
      _h_pT_hadT   -> fill(_thad.pT(),    weight);
      _h_eta_hadT  -> fill(_thad.eta(),   weight);
      _h_pT_ttbar  -> fill(_ttbar.pT(),   weight);
      _h_eta_ttbar -> fill(_ttbar.eta(),  weight);
      _h_phi_ttbar -> fill(_ttbar.phi(),  weight);
      _h_M_ttbar   -> fill(_ttbar.mass(), weight);

      if(_tlep.pT() > _thad.pT()){

	_h_pT_T1   -> fill(_tlep.pT(),    weight);
	_h_eta_T1  -> fill(_tlep.eta(),   weight);
	_h_pT_T2   -> fill(_thad.pT(),    weight);
        _h_eta_T2  -> fill(_thad.eta(),   weight);

      }
      else{

	_h_pT_T2   -> fill(_tlep.pT(),    weight);
        _h_eta_T2  -> fill(_tlep.eta(),   weight);
        _h_pT_T1   -> fill(_thad.pT(),    weight);
        _h_eta_T1  -> fill(_thad.eta(),   weight);

      }

      return true;

    }


    bool ReconstructTopsAllhad(const Jets & bjets, const double & weight) {

      if (bjets.size()!=2) {
        return false;
      }

      // not 100% pseudo top definition yet!!!

      double minchi2(1.e12),chi2_1,chi2_2;
      //      double minchi2lep(1.e12),minchi2had(1.e12);

      int          _b_1 = -1;
      int          _b_2 = -1;
      FourMomentum _Whelp1,_Whelp2;

      for (size_t i=0;i<bjets.size();i++) {
        for (size_t j=0;j<bjets.size();j++) {
          if (i==j) continue;

	  chi2_1 = fabs(_MT*_MT-sqr((bjets[i].momentum()+_Whad1).mass()))/sqr(_reco_sig_MThad);
          chi2_2 = fabs(_MT*_MT-sqr((bjets[j].momentum()+_Whad2).mass()))/sqr(_reco_sig_MThad);

          if (chi2_1+chi2_2<minchi2) {
            _b_1       = i; _b_2 = j;
            minchi2    = chi2_1+chi2_2;
	    _Whelp1    = _Whad1;
	    _Whelp2    = _Whad2;
          }
        }
      }

      _tlep = bjets[_b_1].momentum()+_Whelp1;
      _thad = bjets[_b_2].momentum()+_Whelp2;

      _ttbar = _tlep+_thad;

      //      std::cout << _tlep.mass() << "\t" << _thad.mass() << "\t" << _ttbar.mass() << std::endl;

      _h_M_hadT    -> fill(_tlep.mass(),  weight);
      _h_M_hadT    -> fill(_thad.mass(),  weight);
      _h_pT_hadT   -> fill(_tlep.pT(),    weight);
      _h_eta_hadT  -> fill(_tlep.eta(),   weight);
      _h_pT_hadT   -> fill(_thad.pT(),    weight);
      _h_eta_hadT  -> fill(_thad.eta(),   weight);
      _h_pT_ttbar  -> fill(_ttbar.pT(),   weight);
      _h_eta_ttbar -> fill(_ttbar.eta(),  weight);
      _h_phi_ttbar -> fill(_ttbar.phi(),  weight);
      _h_M_ttbar   -> fill(_ttbar.mass(), weight);

      return true;

    }


    //=====================================================================
    // FillCosThetaStar
    //=====================================================================
    void FillCosThetaStar(FourMomentum top, FourMomentum W, FourMomentum lep, float weight) {
      // need to go into the W rest frame here

      const Vector3 boostVec(W.px(), W.py(), W.pz());

      LorentzTransform boostTrafo;
      boostTrafo.setBoost(-W.boostVector());

      const FourMomentum boostW   = boostTrafo.transform(W);
      const FourMomentum boostTop = boostTrafo.transform(top);
      const FourMomentum boostLep = boostTrafo.transform(lep);

      double cosThetaStar = cos(boostLep.angle(-boostTop));

      _h_cos_theta->fill(cosThetaStar, weight);

    }

    // give light jets to
    void ReconstructAllhadW(const Jets &ljets, float weight){

      FourMomentum W1;
      FourMomentum W2;
      float deviation1 = 1000.0; // smallest dev.
      float deviation2 = 1000.0; // second smallest dev.

      // now look at each possible variation and store Ws that are closest to W-mass

      //      double minchi2(1.e12),chi2lep;
      // double minchi2lep(1.e12),minchi2had(1.e12);

      for (size_t i=0;i<ljets.size();i++) {
        for (size_t j=0;j<ljets.size();j++) {
          if (i==j) continue;

	  FourMomentum Whelp1 = ljets[i].momentum()+ljets[j].momentum();
	  float help_dev      = fabs(_MW-Whelp1.mass());

	  //	  std::cout << i << "\t" << j << "\t" << Whelp1.mass() << "\t" << help_dev << "\t" << W1.mass() << "\t" << W2.mass() << std::endl;

	  if(help_dev < deviation2){

	    if(help_dev == deviation1)
	      continue;
	    if(help_dev == deviation2)
	      continue;

	    // std::cout << help_dev << "\t" << deviation1 << "\t" << deviation2 << std::endl;

	    if(help_dev < deviation1){

	      W2         = W1;
	      deviation2 = deviation1;
	      W1         = Whelp1;
	      deviation1 = help_dev;

	    }
	    else{

	      W2         = Whelp1;
	      deviation2 = help_dev;

	    }

	  }
	}
      }


      //      std::cout << W1.mass() << "\t" << W2.mass() << std::endl;

      _Whad1 = W1;
      _Whad2 = W2;

      _h_M_hadW   -> fill(_Whad1.mass()/GeV,  weight);
      _h_M_hadW   -> fill(_Whad2.mass()/GeV,  weight);
      _h_eta_hadW -> fill(_Whad1.eta(),       weight);
      _h_eta_hadW -> fill(_Whad2.eta(),       weight);
      _h_pT_hadW  -> fill(_Whad1.pT()/GeV,    weight);
      _h_pT_hadW  -> fill(_Whad2.pT()/GeV,    weight);

    }



    void ReconstructDileptonW(ParticleVector lep, Particles neu, float weight){

      FourMomentum lep1 = lep[0].momentum();
      FourMomentum lep2 = lep[1].momentum();
      FourMomentum neu1;
      FourMomentum neu2;
      FourMomentum Whelp1;
      FourMomentum Whelp2;

      int counter = 0;
      foreach(const Particle& p, _neutrinos){
	if(counter == 0)
	  neu1 = p.momentum();
	if(counter == 1)
          neu2 = p.momentum();
	counter += 1;
      }

      // now look at each possible variation and find solution which is closest the W mass!!!

      Whelp1 = lep1+neu1;
      Whelp2 = lep2+neu2;

      float delta1 = fabs((lep1+neu1).mass() - 80.4*GeV) + fabs((lep2+neu2).mass() - 80.4*GeV);
      float delta2 = fabs((lep1+neu2).mass() - 80.4*GeV) + fabs((lep2+neu1).mass() - 80.4*GeV);

      if(delta1 < delta2){
	Whelp1 = lep1+neu1;
	Whelp2 = lep2+neu2;
      }
      else{
	Whelp1 = lep1+neu2;
	Whelp2 = lep2+neu1;
      }

      _Wlep[0] = Whelp1;
      _Wlep[1] = Whelp2;

      _h_M_dilW   -> fill(_Wlep[0].mass()/GeV,  weight);
      _h_M_dilW   -> fill(_Wlep[1].mass()/GeV,  weight);
      _h_pt_dilW  -> fill(_Wlep[0].pT()/GeV, weight);
      _h_pt_dilW  -> fill(_Wlep[1].pT()/GeV, weight);
      _h_eta_dilW -> fill(_Wlep[0].eta(),    weight);
      _h_eta_dilW -> fill(_Wlep[1].eta(),    weight);
      _h_phi_dilW -> fill(_Wlep[0].phi(),    weight);
      _h_phi_dilW -> fill(_Wlep[1].phi(),    weight);

      ReconstructDileptonTop(weight);

    }


    void ReconstructDileptonTop(float weight){

      FourMomentum TopHelp1;
      FourMomentum TopHelp2;
      FourMomentum b1 = good_bjets[0].momentum();
      FourMomentum b2 = good_bjets[1].momentum();
      FourMomentum W1 = _Wlep[0];
      FourMomentum W2 = _Wlep[1];

      float delta1 = fabs((W1+b1).mass() - 172.5*GeV) + fabs((W2+b2).mass() - 172.5*GeV);
      float delta2 = fabs((W1+b2).mass() - 172.5*GeV) + fabs((W2+b1).mass() - 172.5*GeV);

      if(delta1 < delta2){
	TopHelp1 = W1+b1;
	TopHelp2 = W2+b2;
      }
      else{
	TopHelp1 = W1+b2;
        TopHelp2 = W2+b1;
      }

      _h_M_dilTop   -> fill(TopHelp1.mass()/GeV,  weight);
      _h_M_dilTop   -> fill(TopHelp2.mass()/GeV,  weight);
      _h_pt_dilTop  -> fill(TopHelp1.pt()/GeV, weight);
      _h_pt_dilTop  -> fill(TopHelp2.pt()/GeV, weight);
      _h_eta_dilTop -> fill(TopHelp1.eta(),    weight);
      _h_eta_dilTop -> fill(TopHelp2.eta(),    weight);
      _h_phi_dilTop -> fill(TopHelp1.phi(),    weight);
      _h_phi_dilTop -> fill(TopHelp2.phi(),    weight);

      _ttbar = TopHelp1 + TopHelp2;

      _h_pT_ttbar  -> fill(_ttbar.pT(),   weight);
      _h_eta_ttbar -> fill(_ttbar.eta(),  weight);
      _h_phi_ttbar -> fill(_ttbar.phi(),  weight);
      _h_M_ttbar   -> fill(_ttbar.mass(), weight);

    }

    // these functions are also stolen from the ttbar+jets routine above
    bool _ejets(unsigned int& cutBits) {
      // 1. More than zero good electrons
      cutBits += 1; if (_dressedelectrons.size() == 0) return false;
      // 2. No additional electrons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedelectrons.size() > 1) return false;
      // 3. No muons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedmuons.size() > 0) return false;
      // 4. total neutrino pT > 30 GeV
      cutBits += 1 << 3; if (_met_et <= _ETmiss*GeV) return false;
      // 5. MTW > 35 GeV
      cutBits += 1 << 4; if (MTW <= _MTW*GeV) return false;
      // 6. At least two b-tagged jets
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least four good jets
      cutBits += 1 << 6; if (good_jets.size() < 4) return false;
      cutBits += 1 << 7;
      return true;
    }

    bool _mujets(unsigned int& cutBits) {
      // 1. More than zero good muons
      cutBits += 1; if (_dressedmuons.size() == 0) return false;
      // 2. No additional muons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedmuons.size() > 1) return false;
      // 3. No electrons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedelectrons.size() > 0) return false;
      // 4. total neutrino pT > 30 GeV
      cutBits += 1 << 3; if (_met_et <= _ETmiss*GeV) return false;
      cutBits += 1 << 4; if (MTW <= _MTW*GeV) return false;
      // 6. At least two b-tagged jets
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least four good jets
      cutBits += 1 << 6; if (good_jets.size() < 4) return false;
      cutBits += 1 << 7;
      return true;
    }

    // not optimal yet but will be beautified later!
    // If leptons have the same flavor, require m(l+l-) > 20 GeV and | m(l+l-) - Mz | > 10GeV

    bool _pass_ee(unsigned int& cutBits){

      // 1. Exactly 2 good electrons
      cutBits += 1; if (_dressedelectrons.size() != 2) return false;
      // 2. No additional electrons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedelectrons.size() > 2) return false;
      // 3. No muons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedmuons.size() > 0) return false;
      // 4. Selected Leptons need to have opposite sign
      cutBits += 1 << 3; if (_dressedelectrons[0].charge() == _dressedelectrons[1].charge()) return false;
      // 5. total neutrino pT > 60 GeV
      cutBits += 1 << 4; if (_met_et <= _ETmiss*GeV) return false;
      // 6. At least one b-tagged jet
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least two good jets
      cutBits += 1 << 6; if (good_jets.size() < 2) return false;

      float m_inv = (_dressedelectrons[0].momentum()+_dressedelectrons[1].momentum()).mass();

      // 8. Invariant dilepton mass > 20 GeV
      cutBits += 1 << 7; if (m_inv < 20*GeV) return false;
      // 9. Invariant dilepton mass not in Z-mass window
      cutBits += 1 << 8; if (fabs(m_inv - 91.2*GeV) < 10*GeV) return false;
      cutBits += 1 << 9;
      return true;
    }

    bool _pass_mumu(unsigned int& cutBits){

      // 1. Exactly 2 good muons
      cutBits += 1; if (_dressedmuons.size() != 2) return false;
      // 2. No additional electrons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedmuons.size() > 2) return false;
      // 3. No muons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedelectrons.size() > 0) return false;
      // 4. Selected Leptons need to have opposite sign
      cutBits += 1 << 3; if (_dressedmuons[0].charge() == _dressedmuons[1].charge()) return false;
      // 5. total neutrino pT > 60 GeV
      cutBits += 1 << 4; if (_met_et <= _ETmiss*GeV) return false;
      // 6. At least one b-tagged jet
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least two good jets
      cutBits += 1 << 6; if (good_jets.size() < 2) return false;

      float m_inv = (_dressedmuons[0].momentum()+_dressedmuons[1].momentum()).mass();

      // 8. Invariant dilepton mass > 20 GeV
      cutBits += 1 << 7; if (m_inv < 20*GeV) return false;
      // 9. Invariant dilepton mass not in Z-mass window
      cutBits += 1 << 8; if (fabs(m_inv - 91.2*GeV) < 10*GeV) return false;
      cutBits += 1 << 9;
      return true;
    }

    bool _pass_emu(unsigned int& cutBits){
      // 1. Exactly 1 good muon
      cutBits += 1; if (_dressedmuons.size()          != 1) return false;
      // 2. Exactly 1 good electron
      cutBits += 1 << 1; if (_dressedelectrons.size() != 1) return false;
      // 3. No additional electrons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedmuons.size() > 1) return false;
      // 4. No muons passing the veto selection
      cutBits += 1 << 3; if (_vetodressedelectrons.size() > 1) return false;
      // 5. Selected Leptons need to have opposite sign
      cutBits += 1 << 4; if (_dressedmuons[0].charge() == _dressedelectrons[0].charge()) return false;
      // 6. total neutrino pT >
      cutBits += 1 << 5; if (_met_et <= _ETmiss*GeV) return false;
      // 7. At least one b-tagged jet
      cutBits += 1 << 6; if (_jet_ntag < 2) return false;
      // 8. At least two good jets
      cutBits += 1 << 7; if (good_jets.size() < 2) return false;
      cutBits += 1 << 8;
      return true;
    }



    void ReadSteeringFile(std::string inputFile)
    {
      std::ifstream f;
      string s;
      f.open(inputFile.c_str());

      if(!f.is_open())
	{

	  std::cout << "ERROR::Steering file is missing in run folder!!! ---> EXIT." << std::endl;
	  exit(0);

	}

      string sub1;
      string sub2;

      while (!f.eof())
	{
	  getline(f, s);
	  std::cout << s << std::endl;

	  std::stringstream iss(s);

	  iss >> sub1;
	  iss >> sub2;

	  Channel = sub2;

	}

      f.close();

    }

    /// @Name Histograms
    //@{
    Histo1DPtr  _h_jet_pT;
    Histo1DPtr  _h_jet_eta;
    Histo1DPtr  _h_jet_phi;
    Histo1DPtr  _h_bjet_pT;
    Histo1DPtr  _h_bjet_eta;
    Histo1DPtr  _h_bjet_phi;
    Histo1DPtr  _h_ljet_pT;
    Histo1DPtr  _h_ljet_eta;
    Histo1DPtr  _h_ljet_phi;
    Histo1DPtr  _h_jet_1_pT;
    Histo1DPtr  _h_jet_1_eta;
    Histo1DPtr  _h_jet_2_pT;
    Histo1DPtr  _h_jet_2_eta;
    Histo1DPtr  _h_jet_3_pT;
    Histo1DPtr  _h_jet_3_eta;
    Histo1DPtr  _h_jet_4_pT;
    Histo1DPtr  _h_jet_4_eta;
    Histo1DPtr  _h_bjet_1_pT;
    Histo1DPtr  _h_bjet_1_eta;
    Histo1DPtr  _h_bjet_2_pT;
    Histo1DPtr  _h_bjet_2_eta;
    Histo1DPtr  _h_ljet_1_pT;
    Histo1DPtr  _h_ljet_1_eta;
    Histo1DPtr  _h_ljet_2_pT;
    Histo1DPtr  _h_ljet_2_eta;
    Histo1DPtr  _h_njet_10;
    Histo1DPtr  _h_njet_25;
    Histo1DPtr  _h_nbjet_25;
    Histo1DPtr  _h_nljet_25;
    Histo1DPtr  _h_elec_pT;
    Histo1DPtr  _h_elec_eta;
    Histo1DPtr  _h_elec_phi;
    Histo1DPtr  _h_elec_N;
    Histo1DPtr  _h_mu_pT;
    Histo1DPtr  _h_mu_eta;
    Histo1DPtr  _h_mu_phi;
    Histo1DPtr  _h_mu_N;
    Histo1DPtr  _h_tau_pT;
    Histo1DPtr  _h_tau_eta;
    Histo1DPtr  _h_tau_phi;
    Histo1DPtr  _h_tau_N;
    Histo1DPtr  _h_MET;
    Histo1DPtr  _h_MTW;
    Histo1DPtr  _h_chi2_hadW;
    Histo1DPtr  _h_chi2_hadT;
    Histo1DPtr  _h_chi2_lepT;
    Histo1DPtr  _h_M_hadW;
    Histo1DPtr  _h_M_hadT;
    Histo1DPtr  _h_M_lepW;
    Histo1DPtr  _h_M_lepT;
    Histo1DPtr  _h_pT_lepT;
    Histo1DPtr  _h_eta_lepT;
    Histo1DPtr  _h_pT_hadT;
    Histo1DPtr  _h_eta_hadT;
    Histo1DPtr  _h_pT_lepW;
    Histo1DPtr  _h_eta_lepW;
    Histo1DPtr  _h_pT_hadW;
    Histo1DPtr  _h_eta_hadW;
    Histo1DPtr  _h_pT_ttbar;
    Histo1DPtr  _h_eta_ttbar;
    Histo1DPtr  _h_phi_ttbar;
    Histo1DPtr  _h_M_ttbar;
    Histo1DPtr  _h_cos_theta;
    Histo1DPtr  _h_semilep_channel;
    Histo1DPtr  _h_pT_T1;
    Histo1DPtr  _h_eta_T1;
    Histo1DPtr  _h_pT_T2;
    Histo1DPtr  _h_eta_T2;

    // only for dilepton

    Histo1DPtr  _h_DeltaPhi;
    Histo1DPtr  _h_DeltaEta;
    Histo1DPtr  _h_lep1_pt;
    Histo1DPtr  _h_lep1_eta;
    Histo1DPtr  _h_lep1_phi;
    Histo1DPtr  _h_lep2_pt;
    Histo1DPtr  _h_lep2_eta;
    Histo1DPtr  _h_lep2_phi;
    Histo1DPtr  _h_M_dilW;
    Histo1DPtr  _h_pt_dilW;
    Histo1DPtr  _h_eta_dilW;
    Histo1DPtr  _h_phi_dilW;
    Histo1DPtr  _h_M_dilTop;
    Histo1DPtr  _h_pt_dilTop;
    Histo1DPtr  _h_eta_dilTop;
    Histo1DPtr  _h_phi_dilTop;
    Histo1DPtr  _h_dilep_channel;

  };



  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_TTbar_TruthSel> plugin_MC_TTbar_TruthSel;


}

