# BEGIN PLOT /ATLAS_2015_I1345452/d01.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d01-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the muon channel vs. hadronic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d02.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d02-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the electron channel vs. hadronic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d03.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d03-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ in the muon channel vs. hadronic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d04.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d04-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ in the electron channel vs. hadronic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d05.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d05-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l})$ [GeV]
Title=$\sigma(t\bar{t})$ in the muon channel vs. leptonic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d06.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d06-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l})$ [GeV]
Title=$\sigma(t\bar{t})$ in the electron channel vs. leptonic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d07.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d07-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ in the muon channel vs. leptonic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d08.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d08-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ in the electron channel vs. leptonic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d09.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d09-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the muon channel vs. pseudo-top-quark system $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d10.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d10-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the electron channel vs. pseudo-top-quark system $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d11.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d11-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h}\hat{t}_{\mathrm l})|$
Title=$\sigma(t\bar{t})$ in the muon channel vs. pseudo-top-quark system $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d12.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d12-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h}\hat{t}_{\mathrm l})|$
Title=$\sigma(t\bar{t})$ in the electron channel vs. pseudo-top-quark system $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d13.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}m(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d13-x01-y02
XLabel=$m (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the muon channel vs. pseudo-top-quark system mass
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d14.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}m(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d14-x01-y02
XLabel=$m (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ in the electron channel vs. pseudo-top-quark system mass
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d15.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d15-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm h})$ [GeV]
Title=$t\bar{t}$ cross-section after combination vs. hadronic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d16.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d16-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ after combination vs. hadronic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d17.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d17-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l})$ [GeV]
Title=$t\bar{t}$ cross-section after combination vs. leptonic pseudo-top-quark $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d18.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d18-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h})|$
Title=$\sigma(t\bar{t})$ after combination vs. leptonic pseudo-top-quark $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d19.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{dp_{\mathrm T}(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d19-x01-y02
XLabel=$p_{\mathrm T} (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ after combination vs. pseudo-top-quark system $p_{\mathrm T}$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d20.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}|y(\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})|}$ [pb]
RatioPlotYMin=0.5
RatioPlotYMax=1.4
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d20-x01-y02
XLabel=$|y (\hat{t}_{\mathrm h}\hat{t}_{\mathrm l})|$
Title=$\sigma(t\bar{t})$ after combination vs. pseudo-top-quark system $|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d21.
LegendXPos=0.69
YLabel=$\frac{\mathrm{d}\sigma^{\mathrm{fid}}}{\mathrm{d}m(\hat{t}_{\mathrm l} \hat{t}_{\mathrm h})}$ [$\frac{\mathrm pb}{\mathrm{GeV}}$]
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYLabel=Expected/Data
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/d21-x01-y02
XLabel=$m (\hat{t}_{\mathrm l}\hat{t}_{\mathrm h})$ [GeV]
Title=$\sigma(t\bar{t})$ after combination vs. pseudo-top-quark system mass
# END PLOT


# BEGIN PLOT /ATLAS_2015_I1345452/jet1
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/jet2
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/mtt
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/pttt
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/muon
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/elec
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/th
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=5
RatioPlotYLabel=Var/Nom
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1345452/tl
LegendXPos=0.69
RatioPlotYMin=0.5
RatioPlotYMax=1.5
Rebin=10
RatioPlotYLabel=Var/Nom
# END PLOT

