#!/bin/bash

echo -e "\033[34;5m Blue: numbers on arXiv\033[0m"
echo -e "\033[31;5m Red : numbers that should go in publication\033[0m"

file=ResultsSummary.txt

cs_central_bddp_common=$(grep BDDP $file | grep central | grep -v wgtq| awk '{print 1000.*$7/$6}')
cs_central_bddp_ubc=$(grep BDDP $file | grep central | grep -v wgtq|  awk '{print 1000.*$8/$6}')
cs_central_bddp_osu=$(grep BDDP $file | grep central | grep -v wgtq |  awk '{print 1000.*$9/$6}')
cs_central_bddp_desy=$(grep BDDP $file | grep central | grep -v wgtq|  awk '{print 1000.*$10/$6}')

cs_up_bddp_common=$(grep BDDP $file | grep up | grep -v wgtq | awk -v cs=$cs_central_bddp_common '{print cs-1000.*$7/$6}')
cs_up_bddp_ubc=$(grep BDDP $file | grep up | grep -v wgtq | awk -v cs=$cs_central_bddp_ubc '{print cs-1000.*$8/$6}')
cs_up_bddp_osu=$(grep BDDP $file | grep up | grep -v wgtq | awk -v cs=$cs_central_bddp_osu '{print cs-1000.*$9/$6}')
cs_up_bddp_desy=$(grep BDDP $file | grep up | grep -v wgtq | awk -v cs=$cs_central_bddp_desy '{print cs-1000.*$10/$6}')

cs_down_bddp_common=$(grep BDDP $file | grep down | grep -v wgtq | awk -v cs=$cs_central_bddp_common '{print 1000.*$7/$6-cs}')
cs_down_bddp_ubc=$(grep BDDP $file | grep down | grep -v wgtq | awk -v cs=$cs_central_bddp_ubc '{print 1000.*$8/$6-cs}')
cs_down_bddp_osu=$(grep BDDP $file | grep down | grep -v wgtq | awk -v cs=$cs_central_bddp_osu '{print 1000.*$9/$6-cs}')
cs_down_bddp_desy=$(grep BDDP $file | grep down | grep -v wgtq | awk -v cs=$cs_central_bddp_desy '{print 1000.*$10/$6-cs}')

echo -e "\033[0;34m===== aMC@NLO+Pythia8 (BDDP) ====="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_bddp_common $cs_down_bddp_common $cs_up_bddp_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_bddp_ubc $cs_down_bddp_ubc $cs_up_bddp_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_bddp_osu $cs_down_bddp_osu $cs_up_bddp_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_bddp_desy $cs_down_bddp_desy $cs_up_bddp_desy
echo -e "\033[0m"

cs_central_bddp_wgtq1_common=$(grep BDDP $file | grep central | grep wgtq1 | awk '{print 1000.*$7/$6}')
cs_central_bddp_wgtq1_ubc=$(grep BDDP $file | grep central | grep wgtq1 | awk '{print 1000.*$8/$6}')
cs_central_bddp_wgtq1_osu=$(grep BDDP $file | grep central | grep wgtq1 | awk '{print 1000.*$9/$6}')
cs_central_bddp_wgtq1_desy=$(grep BDDP $file | grep central | grep wgtq1 | awk '{print 1000.*$10/$6}')

cs_up_bddp_wgtq1_common=$(grep BDDP $file | grep up | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_common '{print cs-1000.*$7/$6}')
cs_up_bddp_wgtq1_ubc=$(grep BDDP $file | grep up | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_ubc '{print cs-1000.*$8/$6}')
cs_up_bddp_wgtq1_osu=$(grep BDDP $file | grep up | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_osu '{print cs-1000.*$9/$6}')
cs_up_bddp_wgtq1_desy=$(grep BDDP $file | grep up | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_desy '{print cs-1000.*$10/$6}')

cs_down_bddp_wgtq1_common=$(grep BDDP $file | grep down | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_common '{print 1000.*$7/$6-cs}')
cs_down_bddp_wgtq1_ubc=$(grep BDDP $file | grep down | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_ubc '{print 1000.*$8/$6-cs}')
cs_down_bddp_wgtq1_osu=$(grep BDDP $file | grep down | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_osu '{print 1000.*$9/$6-cs}')
cs_down_bddp_wgtq1_desy=$(grep BDDP $file | grep down | grep  wgtq | awk -v cs=$cs_central_bddp_wgtq1_desy '{print 1000.*$10/$6-cs}')


echo -e "\033[0;31m==== aMC@NLO+Pythia8 (BDDP wgtq1) ===="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_bddp_wgtq1_common $cs_down_bddp_wgtq1_common $cs_up_bddp_wgtq1_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_bddp_wgtq1_ubc $cs_down_bddp_wgtq1_ubc $cs_up_bddp_wgtq1_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_bddp_wgtq1_osu $cs_down_bddp_wgtq1_osu $cs_up_bddp_wgtq1_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_bddp_wgtq1_desy $cs_down_bddp_wgtq1_desy $cs_up_bddp_wgtq1_desy
echo -e "\033[0m"

cs_central_ht4_common=$(grep aMC $file | grep HT | grep -v wgtq | grep central | awk '{print 1000.*$7/$6}')
cs_central_ht4_ubc=$(grep aMC $file | grep HT | grep -v wgtq | grep central | awk '{print 1000.*$8/$6}')
cs_central_ht4_osu=$(grep aMC $file | grep HT | grep -v wgtq  | grep central | awk '{print 1000.*$9/$6}')
cs_central_ht4_desy=$(grep aMC $file | grep HT | grep -v wgtq  | grep central | awk '{print 1000.*$10/$6}')

cs_up_ht4_common=$(grep aMC $file | grep HT | grep -v wgtq  | grep up | awk -v cs=$cs_central_ht4_common '{print cs-1000.*$7/$6}')
cs_up_ht4_ubc=$(grep aMC $file | grep HT | grep -v wgtq  | grep up | awk -v cs=$cs_central_ht4_ubc '{print cs-1000.*$8/$6}')
cs_up_ht4_osu=$(grep aMC $file | grep HT | grep -v wgtq  | grep up | awk -v cs=$cs_central_ht4_osu '{print cs-1000.*$9/$6}')
cs_up_ht4_desy=$(grep aMC $file | grep HT | grep -v wgtq  | grep up | awk -v cs=$cs_central_ht4_desy '{print cs-1000.*$10/$6}')

cs_down_ht4_common=$(grep aMC $file | grep HT | grep -v wgtq  | grep down | awk -v cs=$cs_central_ht4_common '{print 1000.*$7/$6-cs}')
cs_down_ht4_ubc=$(grep aMC $file | grep HT | grep -v wgtq  | grep down | awk -v cs=$cs_central_ht4_ubc '{print 1000.*$8/$6-cs}')
cs_down_ht4_osu=$(grep aMC $file | grep HT | grep -v wgtq  | grep down | awk -v cs=$cs_central_ht4_osu '{print 1000.*$9/$6-cs}')
cs_down_ht4_desy=$(grep aMC $file | grep HT | grep -v wgtq  | grep down | awk -v cs=$cs_central_ht4_desy '{print 1000.*$10/$6-cs}')


echo -e "\033[0;34m===== aMC@NLO+Pythia8 (Ht/4) ====="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_ht4_common $cs_down_ht4_common $cs_up_ht4_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_ht4_ubc $cs_down_ht4_ubc $cs_up_ht4_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_ht4_osu $cs_down_ht4_osu $cs_up_ht4_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_ht4_desy $cs_down_ht4_desy $cs_up_ht4_desy
echo -e "\033[0m"

cs_central_ht4_wgtq1_common=$(grep aMC $file | grep HT | grep wgtq | grep central | awk '{print 1000.*$7/$6}')
cs_central_ht4_wgtq1_ubc=$(grep aMC $file | grep HT | grep wgtq | grep central | awk '{print 1000.*$8/$6}')
cs_central_ht4_wgtq1_osu=$(grep aMC $file | grep HT | grep wgtq  | grep central | awk '{print 1000.*$9/$6}')
cs_central_ht4_wgtq1_desy=$(grep aMC $file | grep HT | grep wgtq  | grep central | awk '{print 1000.*$10/$6}')

cs_up_ht4_wgtq1_common=$(grep aMC $file | grep HT | grep wgtq  | grep up | awk -v cs=$cs_central_ht4_wgtq1_common '{print cs-1000.*$7/$6}')
cs_up_ht4_wgtq1_ubc=$(grep aMC $file | grep HT | grep wgtq  | grep up | awk -v cs=$cs_central_ht4_wgtq1_ubc '{print cs-1000.*$8/$6}')
cs_up_ht4_wgtq1_osu=$(grep aMC $file | grep HT | grep wgtq  | grep up | awk -v cs=$cs_central_ht4_wgtq1_osu '{print cs-1000.*$9/$6}')
cs_up_ht4_wgtq1_desy=$(grep aMC $file | grep HT | grep wgtq  | grep up | awk -v cs=$cs_central_ht4_wgtq1_desy '{print cs-1000.*$10/$6}')

cs_down_ht4_wgtq1_common=$(grep aMC $file | grep HT | grep wgtq  | grep down | awk -v cs=$cs_central_ht4_wgtq1_common '{print 1000.*$7/$6-cs}')
cs_down_ht4_wgtq1_ubc=$(grep aMC $file | grep HT | grep wgtq  | grep down | awk -v cs=$cs_central_ht4_wgtq1_ubc '{print 1000.*$8/$6-cs}')
cs_down_ht4_wgtq1_osu=$(grep aMC $file | grep HT | grep wgtq  | grep down | awk -v cs=$cs_central_ht4_wgtq1_osu '{print 1000.*$9/$6-cs}')
cs_down_ht4_wgtq1_desy=$(grep aMC $file | grep HT | grep wgtq  | grep down | awk -v cs=$cs_central_ht4_wgtq1_desy '{print 1000.*$10/$6-cs}')


echo -e "\033[0;31m===== aMC@NLO+Pythia8 (Ht/4 wgtq1) ====="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_ht4_wgtq1_common $cs_down_ht4_wgtq1_common $cs_up_ht4_wgtq1_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_ht4_wgtq1_ubc $cs_down_ht4_wgtq1_ubc $cs_up_ht4_wgtq1_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_ht4_wgtq1_osu $cs_down_ht4_wgtq1_osu $cs_up_ht4_wgtq1_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_ht4_wgtq1_desy $cs_down_ht4_wgtq1_desy $cs_up_ht4_wgtq1_desy
echo -e "\033[0m"


cs_central_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq | grep -v dist | grep central | awk '{print 1000.*$7/$6}')
cs_central_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq | grep -v dist | grep central | awk '{print 1000.*$8/$6}')
cs_central_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep -v dist| grep central | awk '{print 1000.*$9/$6}')
cs_central_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep -v dist | grep central | awk '{print 1000.*$10/$6}')

cs_up_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_common '{print cs-1000.*$7/$6}')
cs_up_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_ubc '{print cs-1000.*$8/$6}')
cs_up_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_osu '{print cs-1000.*$9/$6}')
cs_up_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep up | grep -v dist | awk -v cs=$cs_central_ht4_desy '{print cs-1000.*$10/$6}')

cs_down_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_common '{print 1000.*$7/$6-cs}')
cs_down_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_ubc '{print 1000.*$8/$6-cs}')
cs_down_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_osu '{print 1000.*$9/$6-cs}')
cs_down_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep -v wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_desy '{print 1000.*$10/$6-cs}')


echo -e "\033[0;34m===== Powhel+Pythia8 (Ht/2) ====="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_ht4_common $cs_down_ht4_common $cs_up_ht4_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_ht4_ubc $cs_down_ht4_ubc $cs_up_ht4_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_ht4_osu $cs_down_ht4_osu $cs_up_ht4_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_ht4_desy $cs_down_ht4_desy $cs_up_ht4_desy
echo -e "\033[0m"

cs_central_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep wgtq | grep -v dist | grep central | awk '{print 1000.*$7/$6}')
cs_central_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep wgtq | grep -v dist | grep central | awk '{print 1000.*$8/$6}')
cs_central_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep -v dist| grep central | awk '{print 1000.*$9/$6}')
cs_central_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep -v dist | grep central | awk '{print 1000.*$10/$6}')

cs_up_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_common '{print cs-1000.*$7/$6}')
cs_up_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_ubc '{print cs-1000.*$8/$6}')
cs_up_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep up | grep -v dist| awk -v cs=$cs_central_ht4_osu '{print cs-1000.*$9/$6}')
cs_up_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep up | grep -v dist | awk -v cs=$cs_central_ht4_desy '{print cs-1000.*$10/$6}')

cs_down_ht4_common=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_common '{print 1000.*$7/$6-cs}')
cs_down_ht4_ubc=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_ubc '{print 1000.*$8/$6-cs}')
cs_down_ht4_osu=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_osu '{print 1000.*$9/$6-cs}')
cs_down_ht4_desy=$(grep Powhel $file | grep 'HT/2' | grep wgtq  | grep down | grep -v dist| awk -v cs=$cs_central_ht4_desy '{print 1000.*$10/$6-cs}')


echo -e "\033[0;31m===== Powhel+Pythia8 (Ht/2 wgtq1) ====="
printf "common            : %.0f + %.0f - %.0f\n" $cs_central_ht4_common $cs_down_ht4_common $cs_up_ht4_common
printf "ttb    (l+jets)   : %.0f + %.0f - %.0f\n" $cs_central_ht4_ubc $cs_down_ht4_ubc $cs_up_ht4_ubc
printf "ttb    (dilepton) : %.0f + %.0f - %.0f\n" $cs_central_ht4_osu $cs_down_ht4_osu $cs_up_ht4_osu
printf "ttbb   (dilepton) : %.1f + %.1f - %.1f\n" $cs_central_ht4_desy $cs_down_ht4_desy $cs_up_ht4_desy
echo -e "\033[0m"


sigma=$(grep wgtq3 $file | grep ATTBAR  | awk '{print $4}')
cs_central_wgtq3_common=$(grep wgtq3 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$7/$6}')
cs_central_wgtq3_ubc=$(grep wgtq3 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$8/$6}')
cs_central_wgtq3_osu=$(grep wgtq3 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$9/$6}')
cs_central_wgtq3_desy=$(grep wgtq3 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$10/$6}')

echo "===== Pythia8 (wgtq3) ====="
printf "common            : %.0f\n" $cs_central_wgtq3_common 
printf "ttb    (l+jets)   : %.0f\n" $cs_central_wgtq3_ubc 
printf "ttb    (dilepton) : %.0f\n" $cs_central_wgtq3_osu 
printf "ttbb   (dilepton) : %.1f\n" $cs_central_wgtq3_desy

sigma=$(grep wgtq5 $file | grep ATTBAR  | awk '{print $4}')
cs_central_wgtq5_common=$(grep wgtq5 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$7/$6}')
cs_central_wgtq5_ubc=$(grep wgtq5 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$8/$6}')
cs_central_wgtq5_osu=$(grep wgtq5 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$9/$6}')
cs_central_wgtq5_desy=$(grep wgtq5 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$10/$6}')

echo "===== Pythia8 (wgtq5) ====="
printf "common            : %.0f\n" $cs_central_wgtq5_common 
printf "ttb    (l+jets)   : %.0f\n" $cs_central_wgtq5_ubc 
printf "ttb    (dilepton) : %.0f\n" $cs_central_wgtq5_osu 
printf "ttbb   (dilepton) : %.1f\n" $cs_central_wgtq5_desy 

sigma=$(grep wg6 $file | grep ATTBAR  | awk '{print $4}')
cs_central_wg6_common=$(grep wg6 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$7/$6}')
cs_central_wg6_ubc=$(grep wg6 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$8/$6}')
cs_central_wg6_osu=$(grep wg6 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$9/$6}')
cs_central_wg6_desy=$(grep wg6 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$10/$6}')

echo "===== Pythia8 (wgtq6 sgtq=0.25) ====="
printf "common            : %.0f\n" $cs_central_wg6_common 
printf "ttb    (l+jets)   : %.0f\n" $cs_central_wg6_ubc 
printf "ttb    (dilepton) : %.0f\n" $cs_central_wg6_osu 
printf "ttbb   (dilepton) : %.1f\n" $cs_central_wg6_desy

sigma=$(grep wgtq4 $file | grep ATTBAR  | awk '{print $4}')
cs_central_wgtq4_common=$(grep wgtq4 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$7/$6}')
cs_central_wgtq4_ubc=$(grep wgtq4 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$8/$6}')
cs_central_wgtq4_osu=$(grep wgtq4 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$9/$6}')
cs_central_wgtq4_desy=$(grep wgtq4 $file | grep ATTBAR  | awk -v var=$sigma '{print var*1000.*$10/$6}')

echo "===== Pythia8 (wgtq4) ====="
printf "common            : %.0f\n" $cs_central_wgtq4_common 
printf "ttb    (l+jets)   : %.0f\n" $cs_central_wgtq4_ubc 
printf "ttb    (dilepton) : %.0f\n" $cs_central_wgtq4_osu 
printf "ttbb   (dilepton) : %.1f\n" $cs_central_wgtq4_desy 


exit 0
