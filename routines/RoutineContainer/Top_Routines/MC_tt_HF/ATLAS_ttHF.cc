// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include <iostream>
#include <map>
#include <bitset>

const std::string selection_names[4] = {"ljets_ttb","dilep_ttb","dilep_ttbb_DESY","dilep_ttbb_CERN"};

namespace Rivet {
  
  using namespace Cuts;
  
  /// @brief ATLAS 8 TeV ttHF cross-section measurements
  /// @author S. Argyropoulos <spyridon.argyropoulos@cern.ch>
  /// @author M. Danninger <matthias.danninger@cern.ch>
  
  class ATLAS_ttHF : public Analysis {
  public:
    ATLAS_ttHF():
    Analysis("ATLAS_ttHF"),
    m_jets(0),
    m_bjets(0),
    m_jet_n(0),
    m_jet_ntag(0),
    m_overlap(false)
    {}
    
  //-----------------------------------------------------------
  
    void init() {
    
      // Eta ranges
      Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
      Cut eta_lep  = etaIn(-2.5, 2.5);
    
      // All final state particles
      FinalState fs(eta_full);
    
      // Get photons to dress leptons
      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);
    
      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");
      DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep & (pT >= 25.0*GeV), true, true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");

    
      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");
      DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep & (pT >= 25.0*GeV), true, true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
      addProjection(ewdressedmuons, "ewdressedmuons");
    
      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");
    
      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs,FastJets::ANTIKT, 0.4);
      jets.useInvisibles();
      addProjection(jets, "jets");
    
      inithistos();
    }
  
    //-----------------------------------------------------------
  
    void analyze(const Event& event) {
          
      Cut jet_cuts = etaIn(-2.5, 2.5) & (pT >= 20.0*GeV);

      // Reset counters
      m_jet_ntag = 0;
      m_overlap  = false;
      m_bjets.clear();
    
      // Get the selected objects, using the projections.
      m_dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
    
      m_dressedmuons = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
      
      m_neutrinos = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();
    
      m_jets = applyProjection<FastJets>(event, "jets").jetsByPt(jet_cuts); // 20GeV truth threshold
  
      // Check overlap of jets/leptons.
      foreach (const Jet &jet, m_jets) {
        // if dR(el,jet) < 0.4 skip the event
        foreach(const DressedLepton &el, m_dressedelectrons) {
          if(deltaR(jet.momentum(), el.momentum()) < 0.4) m_overlap=true;
        }
        // if dR(mu,jet) < 0.4 skip the event
        foreach (const DressedLepton &mu, m_dressedmuons) {
          if(deltaR(jet.momentum(), mu.momentum()) < 0.4) m_overlap=true;
        }
        // Count the number of b-tags
        // We have to check that the ghost-matched B hadrons have pT > 5 GeV
	// By default jet.bTags() returns all B hadrons without cuts
	bool BhadronInJet = false;
	foreach ( const Particle &p, jet.bTags() ) {
	  if ( p.pT() >= 5.0*GeV ) {
	    BhadronInJet = true;
	    break;
	  } 
	}   
	if (BhadronInJet) {
	  m_bjets.push_back(jet);
	  ++m_jet_ntag;
	}   
      }
    
      const double weight = event.weight();
      
      // Cut that is needed for Powhel ttbb sample - Should remove at some point
      /*if ( fabs(weight) > 1.E4 ) { 
	m_histos["vetoedHighWeight"]->fill(1.0);
        vetoEvent;
      }*/
      
      m_histos["weight_dist"]->fill(weight);
      m_histos["bjet_mult_nocuts"]->fill(m_jet_ntag, weight);
      m_histos["weights"]->fill(0, weight);
      m_histos["weights2"]->fill(1, weight); // What's the reason for filling this histogram?

      // Remove events with object overlap
      if (m_overlap) vetoEvent;

      // Evaluate basic event selection
      unsigned int ejets_bits   = 0, mujets_bits    = 0, emu_ttb_bits  = 0, common_bits = 0; // Not used anywhere
      unsigned int ee_ttbb_bits = 0, mumu_ttbb_bits = 0, emu_ttbb_bits = 0;                  // We should remove them
      bool pass_common    = _common_ttb(common_bits);
      bool pass_ejets     = _ejets(ejets_bits);
      bool pass_mujets    = _mujets(mujets_bits);
      bool pass_emu_ttb   = _emu_ttb(emu_ttb_bits);
      bool pass_ee_ttbb   = _ee_ttbb(ee_ttbb_bits);
      bool pass_mumu_ttbb = _mumu_ttbb(mumu_ttbb_bits);
      bool pass_emu_ttbb  = _emu_ttbb(emu_ttbb_bits);

      
      // basic event selection requirements
      if (pass_common) {
        m_histos["weights"]->fill(1, weight);
      }
      if (pass_ejets || pass_mujets){
        m_histos["weights"]->fill(2, weight);
        fill_histos(m_bjets, selection_names[0], weight);
      }
      if (pass_emu_ttb){
        m_histos["weights"]->fill(3, weight);
        fill_histos(m_bjets, selection_names[1], weight);
      }
      if ( (pass_emu_ttbb || pass_ee_ttbb || pass_mumu_ttbb) && m_jet_ntag >= 4 ){ // ttbb DESY
        m_histos["weights"]->fill(4, weight);
        fill_histos(m_bjets, selection_names[2], weight);
      }
      if ( (pass_emu_ttbb || pass_ee_ttbb || pass_mumu_ttbb) && m_jet_ntag >= 4 ){  // ttbb CERN 
        m_histos["weights"]->fill(5, weight);                                       // Should be merged with DESY
        fill_histos(m_bjets, selection_names[3], weight);
      }
      if ( (pass_emu_ttbb || pass_ee_ttbb || pass_mumu_ttbb) && m_jets.size() >= 4 ){ // ttjj analysis
        m_histos["weights"]->fill(6, weight);
      }
    }
  
    
    //-----------------------------------------------------------
    
    void finalize() {
      //double norm = crossSection()/sumOfWeights();
    }
    
  private:
    //-----------------------------------------------------------
    /// @name Cut helper functions
    //@{

    // tt+HF common phase space selection
    bool _common_ttb(unsigned int& cutBits) {

      // 1. More than zero good muons && more than 0 good electrons                                                     
      cutBits += 1; if ( m_dressedmuons.size()+m_dressedelectrons.size() == 0 ) return false;
      // 4. At least three b-tagged jet                                                                                        
      cutBits += 1 << 1; if (m_jet_ntag < 3) return false;
      cutBits += 1 << 2;
      return true;
    }
    
    
    // Event selection for l+jets ttb e-channel
    bool _ejets(unsigned int& cutBits) {
      
      // 1. More than zero good electrons
      cutBits += 1; if (m_dressedelectrons.size() == 0) return false;
      // 2. No additional electrons passing the veto selection
      cutBits += 1 << 1; if (m_dressedelectrons.size() > 1) return false;
      // 3. No muons passing the veto selection
      cutBits += 1 << 2; if (m_dressedmuons.size() > 0) return false;
      // 4. At least three b-tagged jet
      cutBits += 1 << 3; if (m_jet_ntag < 3) return false;
      // 5. At least five good jets
      cutBits += 1 << 4; if (m_jets.size() < 5) return false;
      cutBits += 1 << 5;
      return true;
    }
    
    
    // Event selection for l+jets ttb mu-channel
    bool _mujets(unsigned int& cutBits) {
      
      // 1. More than zero good muons
      cutBits += 1; if (m_dressedmuons.size() == 0) return false;
      // 2. No additional muons passing the veto selection
      cutBits += 1 << 1; if (m_dressedmuons.size() > 1) return false;
      // 3. No electrons passing the veto selection
      cutBits += 1 << 2; if (m_dressedelectrons.size() > 0) return false;
      // 4. At least three b-tagged jet
      cutBits += 1 << 3; if (m_jet_ntag < 3) return false;
      // 5. At least five good jets
      cutBits += 1 << 4; if (m_jets.size() < 5) return false;
      cutBits += 1 << 5;
      return true;
    }
    

    // Event selection for ttb-dilepton ((emu-channel))
    bool _emu_ttb(unsigned int& cutBits) {

      // 1. More than zero good muons && more than 0 good electrons 
      cutBits += 1; if(m_dressedmuons.size() == 0 || m_dressedelectrons.size() == 0) return false;
      // 2. Exactly 1 electron and 1 muon passing the veto selection
      cutBits += 1 <<2; if(m_dressedmuons.size() != 1 || m_dressedelectrons.size() != 1) return false;
      // 3. Opposite sign leptons
      cutBits += 1 <<3; if( m_dressedmuons.at(0).charge()*m_dressedelectrons.at(0).charge() > 0 ) return false;
      // 4. At least three b-tagged jet                                                                             
      cutBits += 1 << 3; if (m_jet_ntag < 3) return false;
      cutBits += 1 << 4;
      return true;
    }
  
    
    // Event selection for ttbb-dilepton (ee-channel)
    bool _ee_ttbb(unsigned int& cutBits) {
      // 1. More than zero good electrons
      cutBits+=1; if(m_dressedelectrons.size() == 0) return false;
      // 2. Exactly 2 electrons
      cutBits+=1<<1; if(m_dressedelectrons.size() != 2) return false;
      // 3. No muons passing the veto selection
      cutBits+=1<<2; if(m_dressedmuons.size() > 0) return false;
      // 4. Opposite sign electrons
      cutBits+=1<<3; if( m_dressedelectrons.at(0).charge()*m_dressedelectrons.at(1).charge() > 0 ) return false;
      // 5. Dilepton mass cuts
      cutBits+=1<<4;
      if( _diLeptonMass(m_dressedelectrons.at(0),m_dressedelectrons.at(1)) <= 15.0 ||
         (_diLeptonMass(m_dressedelectrons.at(0),m_dressedelectrons.at(1)) > 81.0 &&
          _diLeptonMass(m_dressedelectrons.at(0),m_dressedelectrons.at(1)) < 101.0   ) ) return false;
      // 6. At least 4 b-tagged jets 
      cutBits+=1<<5; if(m_jet_ntag < 2) return false;
      cutBits+=1<<5;
      return true;
    }
    
    
    // Event selection for ttbb-dilepton (mumu-channel)
    bool _mumu_ttbb(unsigned int& cutBits) {
      
      // 1. More than zero good muons
      cutBits+=1; if(m_dressedmuons.size() == 0) return false;
      // 2. Exactly 2 muons passing the veto selection
      cutBits+=1<<1; if(m_dressedmuons.size() != 2) return false;
      // 3. No electrons passing the veto selection
      cutBits+=1<<2; if(m_dressedelectrons.size() > 0) return false;
      // 4. Opposite sign muons
      cutBits+=1<<3; if( m_dressedmuons.at(0).charge()*m_dressedmuons.at(1).charge() > 0 ) return false;
      // 5. Dilepton mass cuts
      cutBits+=1<<4;
      if( _diLeptonMass(m_dressedmuons.at(0),m_dressedmuons.at(1)) <= 15.0 ||
         (_diLeptonMass(m_dressedmuons.at(0),m_dressedmuons.at(1)) > 81.0 &&
          _diLeptonMass(m_dressedmuons.at(0),m_dressedmuons.at(1)) < 101.0   ) ) return false;
      // 6. At least 4 b-tagged jet 
      cutBits+=1<<5; if(m_jet_ntag < 2) return false;
      cutBits+=1<<5;
      return true;
    }
    
    
    // Event selection for ttbb-dilepton (emu-channel)
    bool _emu_ttbb(unsigned int &cutBits){
      
      // 1. More than zero good muons && more than 0 good electrons
      cutBits+=1; if(m_dressedmuons.size() == 0 || m_dressedelectrons.size() == 0) return false;
      // 2. Exactly 1 electron and 1 muon passing the veto selection
      cutBits+=1<<2; if(m_dressedmuons.size() != 1 || m_dressedelectrons.size() != 1) return false;
      // 3. Opposite sign leptons
      cutBits+=1<<3; if( m_dressedmuons.at(0).charge()*m_dressedelectrons.at(0).charge() > 0 ) return false;
      // 4. Dilepton mass cut
      cutBits+=1<<4;
      if( _diLeptonMass(m_dressedmuons.at(0),m_dressedelectrons.at(0)) <= 15.0 ) return false;
      // 5. At least 4 b-tagged jet 
      cutBits+=1<<5; if(m_jet_ntag < 2) return false;
      cutBits+=1<<5;
      return true;
    }

  
    /// @name Physics object helper functions
    //@{
    
    double _diLeptonMass(const DressedLepton& lep1, const DressedLepton& lep2) {
      return add(lep1.momentum(),lep2.momentum()).mass();
    }
    
    double _dijetMass(const Jet& jet1, const Jet& jet2) {
      return add(jet1.momentum(),jet2.momentum()).mass();
    }
    
    //@}
    
    
   
  private:
    
    /// @name Objects that are used by the event selection decisions
    //@{
    std::vector<DressedLepton> m_dressedelectrons;
    std::vector<DressedLepton> m_vetodressedelectrons;
    std::vector<DressedLepton> m_dressedmuons;
    std::vector<DressedLepton> m_vetodressedmuons;
    Particles      m_neutrinos;
    Jets           m_jets;
    Jets           m_bjets;
    unsigned int   m_jet_n;
    unsigned int   m_jet_ntag;
    bool           m_overlap;
    
    std::map<std::string, Histo1DPtr > m_histos;
    
    void inithistos() {
      std::string name; 
      
      // Comments: 
      // 1. Why not use the Histo1DPtr directly?
      // 2. Why do we need weights2 - it seems to be equal to the first bin of weights
      // 3. Propose to change "weights" to "selectedEvents" or something similar
      
      m_histos["vetoedHighWeight"] = bookHisto1D("vetoedHighWeight", 1, 0.5, 1.5);
      m_histos["weight_dist"]      = bookHisto1D("weight_dist", 100, -1E5, 1E5); // weight distribution
      m_histos["weights2"]         = bookHisto1D("weights2", 1, 0.5, 1.5); // weights passing cuts
      m_histos["weights"]          = bookHisto1D("weights",8,  -1.5, 6.5); // weights passing cuts
      m_histos["bjet_mult_nocuts"] = bookHisto1D("bjet_mult_nocuts", 9, -0.5, 8.5);
      
      // histograms divided up by analysis
      for (int sel=0; sel < 4; ++sel) {   // loop over all selections
        name = "weight_dist_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, -1E4, 1E4);
        name = "bjet_mult_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 9, -0.5, 8.5);
        name = "bjet1_pt_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 500.0);
        name = "bjet2_pt_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 500.0);
        name = "bjet3_pt_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 500.0);
        name = "bjet4_pt_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 500.0); 
	name = "bjet12_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0);   
	name = "bjet13_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0);      
	name = "bjet14_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0);      
	name = "bjet23_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0);      
	name = "bjet24_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0);      
	name = "bjet34_mass_"+selection_names[sel];
        m_histos[name]                = bookHisto1D(name, 100, 0.0, 100.0); 
	name = "NBhadronsInJet_EventsWithGT3Bjets_"+selection_names[sel];
	m_histos[name]              = bookHisto1D(name, 5, -0.5, 4.5);
	name = "softest_bjet_pt_EventsWithGT3Bjets_"+selection_names[sel];
	m_histos[name]              = bookHisto1D(name, 50, 0.0, 50.0);	  	  
	if ( sel < 2 ) { // Only for selections with >= 3 b-jets
	  name = "NBhadronsInJet_EventsWith3Bjets_"+selection_names[sel];
	  m_histos[name]              = bookHisto1D(name, 5, -0.5, 4.5);
	  name = "softest_bjet_pt_EventsWith3Bjets_"+selection_names[sel];
	  m_histos[name]              = bookHisto1D(name, 50, 0.0, 50.0);
	}     
      }
    } // end inithistos
    
    void fill_histos(const Jets &bjets, const std::string sel_name, const double weight) {
     
      // We should tidy this up a bit
         
      std::string name = "bjet_mult_"+sel_name;
      m_histos[name]->fill(bjets.size(), weight);
      name = "weight_dist_"+sel_name;
      m_histos[name]->fill(weight);      
      
      if ( bjets.size() > 0 ) {
        name = "bjet1_pt_"+sel_name;
        m_histos[name]->fill(bjets.at(0).pT(), weight); 
      }
      if ( bjets.size() > 1 ) {
        name = "bjet2_pt_"+sel_name;
        m_histos[name]->fill(bjets.at(1).pT(), weight); 
        name = "bjet12_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(0),bjets.at(1)), weight);
      }
      if ( bjets.size() > 2 ) {
        name = "bjet3_pt_"+sel_name;
        m_histos[name]->fill(bjets.at(2).pT(), weight); 
	name = "bjet13_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(0),bjets.at(2)), weight);
	name = "bjet23_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(1),bjets.at(2)), weight);
        
	// Histograms for checking the rate of b-jets and bb-jets 
	// usefull for checking the pt->0 divergence in diagrams 
	// with a massless b-quark in the t-channel
	if ( bjets.size() == 3 ) {
	  name = "NBhadronsInJet_EventsWith3Bjets_"+sel_name;
	  foreach (const Jet& bjet, bjets) {
	    unsigned int nBhadrons = 0;	    
            foreach ( const Particle &p, bjet.bTags() ) {
	      if ( p.pT() >= 5.0*GeV ) {
	        ++nBhadrons;
	      } 
	    } 	    
	    m_histos[name]->fill(nBhadrons, weight);
	  }
	  name = "softest_bjet_pt_EventsWith3Bjets_"+sel_name;
	  m_histos[name]->fill(bjets.at(2).pT(), weight); 
	}
      }
      if ( bjets.size() > 3 ) {
        name = "bjet4_pt_"+sel_name;
        m_histos[name]->fill(bjets.at(3).pT(), weight);	 
        name = "softest_bjet_pt_EventsWithGT3Bjets_"+sel_name;
	m_histos[name]->fill(bjets.at(bjets.size()-1).pT(), weight);	
	name = "bjet14_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(0),bjets.at(3)), weight);
	name = "bjet24_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(1),bjets.at(3)), weight);
	name = "bjet34_mass_"+sel_name;
	m_histos[name]->fill(_dijetMass(bjets.at(2),bjets.at(3)), weight);

	// Histograms for checking the rate of b-jets and bb-jets 
	// usefull for checking the pt->0 divergence in diagrams 
	// with a massless b-quark in the t-channel
	name = "NBhadronsInJet_EventsWithGT3Bjets_"+sel_name;
	foreach (const Jet& bjet, bjets) {
	  unsigned int nBhadrons = 0;	    
          foreach ( const Particle &p, bjet.bTags() ) {
	    if ( p.pT() >= 5.0*GeV ) {
	      ++nBhadrons;
	    } 
	  } 	    
	  m_histos[name]->fill(nBhadrons, weight);
	}
        		
      }   
         
    }
    
    
  };
    
    
  // Declare the class as a hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_ttHF);
}
