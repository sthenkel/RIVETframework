# BEGIN PLOT /ATLAS_2015_I1404878/d.
LeftMargin=1.9
LegendXPos=0.6
YLabelSep=8.0
RatioPlotYMin=0.8
RatioPlotYMax=1.2
RatioPlotYLabel=Output_rivet/Particle_level
Title=combined lepton channels
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1404878/d01-x01-y01
XLabel=$p_{\text{T}}^{t,\text{had}}$ [GeV]
YLabel=$\dfrac{\text{d}\sigma^\text{fid}}{\text{d}p_{\text{T}}^{t,\text{had}}}$ [$\dfrac{\text{pb}}{\text{GeV}}$]
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1404878/d01-x01-y02
XLabel=$p_{\text{T}}^{t\bar{t}}$ [GeV]
YLabel=$\dfrac{\text{d}\sigma^\text{fid}}{\text{d}p_{\text{T}^{t\bar{t}}}}$ [$\dfrac{\text{pb}}{\text{GeV}}$]
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1404878/d01-x01-y06
XLabel=$|p_{\text{out}}^{t\bar{t}}|$ [GeV]
YLabel=$\dfrac{\text{d}\sigma^\text{fid}}{\text{d}|p_{\text{out}}^{t\bar{t}}|}$ [$\dfrac{\text{pb}}{\text{GeV}}$]
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1404878/d01-x01-y08
XLabel=$\Delta \phi_{t\bar{t}}$
YLabel=$\dfrac{\text{d}\sigma^\text{fid}}{\text{d}\Delta \phi_{t\bar{t}}}$ [pb]
# END PLOT


