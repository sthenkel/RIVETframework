#include "Rivet/Analysis.hh"
//#include "Rivet/RivetAIDA.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/Beam.hh"

namespace Rivet {

  using namespace Cuts;

  class MC_TTbar_TestSel : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    // WARNING: ISOCUTS and OVERLAPREMOVAL not yet implemented!!!

    /// Constructor
    MC_TTbar_TestSel()
      : Analysis("MC_TTbar_TestSel"),
	_ETmiss(20.), _ETmissMW(60.), _Njets(4),_Ntags(2),_Nlept(1),
	_pt_j(25.), _eta_j(2.5), _R_j(0.4), _MT(172.5), _MW(80.4),
	_reco_sig_MTlep(11.08), _reco_sig_MThad(9.864), _reco_sig_MW(10.57)
    {
      setNeedsCrossSection(false);

      _pt_el  = 25.0;
      _eta_el =  2.5;
      _pt_mu  = 25.0;
      _eta_mu =  2.5;

      _Nlept  = 1;
      _Ntags  = 2;
      _Njets  = 2;
      _ETmiss = 0.0;
      _MTW    = 0.0;

    }


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Eta ranges
      Cut eta_full = etaIn(-5.0, 5.0) & (pT >= 1.0*MeV);
      Cut eta_lep  = etaIn(-2.5, 2.5);

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);

      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");

      DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep & (pT >= 15.0*GeV), true, true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons vetodressedelectrons(photons, electrons, 0.1, eta_lep & (pT >= 15.0*GeV), true, true);
      addProjection(vetodressedelectrons, "vetodressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");

      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");

      vector<pair<double, double> > eta_muon;
      DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep & (pT >= 15.0*GeV), true, true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons vetodressedmuons(photons, muons, 0.1, eta_lep & (pT >= 15.0*GeV), true, true);
      addProjection(vetodressedmuons, "vetodressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
      addProjection(ewdressedmuons, "ewdressedmuons");

      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4);
      jets.useInvisibles();
      addProjection(jets, "jets");


      // book histograms
      // define asymmetric bins for pt and mass distributions until 1000.0
      std::vector< double > binedges;
      binedges.push_back(0.0);     binedges.push_back(50.0);  binedges.push_back(100.0);  binedges.push_back(150.0);
      binedges.push_back(200.0);   binedges.push_back(250.0); binedges.push_back(350.0);  binedges.push_back(800.0);
      binedges.push_back(1500.0);

      std::vector< double > binedges2;
      binedges2.push_back(-4.50); binedges2.push_back(-3.50); binedges2.push_back(-2.50); binedges2.push_back(-1.75);
      binedges2.push_back(-1.25); binedges2.push_back(-0.75); binedges2.push_back(-0.25); binedges2.push_back(0.25);
      binedges2.push_back(0.75);  binedges2.push_back(1.25);  binedges2.push_back(1.75);  binedges2.push_back(2.50);
      binedges2.push_back(3.50);  binedges2.push_back(4.50);

      _h_jet_pT     = bookHisto1D("jet_pT",     binedges);
      _h_jet_eta    = bookHisto1D("jet_eta",    binedges2);
      _h_jet_phi    = bookHisto1D("jet_phi",    50, -6.3, 6.3);

      _h_bjet_pT    = bookHisto1D("bjet_pT",    binedges);
      _h_bjet_eta   = bookHisto1D("bjet_eta",   binedges2);
      _h_bjet_phi   = bookHisto1D("bjet_phi",   50, -6.3, 6.3);

      _h_ljet_pT    = bookHisto1D("ljet_pT",    binedges);
      _h_ljet_eta   = bookHisto1D("ljet_eta",   binedges2);
      _h_ljet_phi   = bookHisto1D("ljet_phi",   50, -6.3, 6.3);

      _h_jet_1_pT   = bookHisto1D("jet_1_pT",   binedges);
      _h_bjet_1_pT  = bookHisto1D("bjet_1_pT",  binedges);
      _h_njet_25    = bookHisto1D("njet_25",    21, -0.5, 20.5);

      _h_elec_pT    = bookHisto1D("elec_pT",   binedges);
      _h_elec_eta   = bookHisto1D("elec_eta",  binedges2);
      _h_elec_phi   = bookHisto1D("elec_phi",  100, -6.3,  6.3);
      _h_elec_N     = bookHisto1D("elec_N",     11, -0.5, 10.5);

      _h_mu_pT      = bookHisto1D("mu_pT",     binedges);
      _h_mu_eta     = bookHisto1D("mu_eta",    binedges2);
      _h_mu_phi     = bookHisto1D("mu_phi",    100, -6.3,  6.3);
      _h_mu_N       = bookHisto1D("mu_N",       11, -0.5, 10.5);

                                                                                                                                                                                                                        
      _h_MET        = bookHisto1D("MET",        binedges);
      _h_MTW        = bookHisto1D("MTW",        150,    0,  150);



    } // end init


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const double   weight = event.weight();

      FourMomentum lepton, gamma;
      FourMomentum p_miss(0.,0.,0.,0.);

      // Get the selected objects, using the projections.
      
      _dressedelectrons     = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());
      _vetodressedelectrons = applyProjection<DressedLeptons>(event, "vetodressedelectrons").dressedLeptons();
      _dressedmuons         = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());
      _vetodressedmuons     = applyProjection<DressedLeptons>(event, "vetodressedmuons").dressedLeptons();
      _neutrinos            = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();
      const Jets alljets_10 = applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);

      _neu_all = applyProjection<FinalState>(event, "neutrinos").particles();

      // select only good electrons
      ParticleVector good_elecs;
      SelectGoodElectrons(_dressedelectrons, good_elecs);

      ParticleVector good_muons;
      SelectGoodMuons(_dressedmuons, good_muons);

      ParticleVector good_leptons;
      good_leptons = good_elecs+good_muons;

      if(!SelectGoodJets(alljets_10, good_bjets, good_ljets, good_jets, good_cjets))
        vetoEvent;

      // Check overlap of jets/leptons.
      unsigned int i,j;
      _jet_ntag = 0;
      bool _overlap = false;
      for (i = 0; i < good_jets.size(); i++) {
        const Jet& jet = good_jets[i];
        // If dR(el,jet) < 0.4 skip the event
        foreach (const Particle& el, good_elecs) {
          if (deltaR(jet, el) < 0.4) _overlap = true;
        }
        // If dR(mu,jet) < 0.4 skip the event
        foreach (const Particle& mu, good_muons) {
          if (deltaR(jet, mu) < 0.4) _overlap = true;
        }
        // If dR(jet,jet) < 0.5 skip the event
        for (j = 0; j < good_jets.size(); j++) {
          const Jet& jet2 = good_jets[j];
          if (i == j) continue; // skip the diagonal
          if (deltaR(jet, jet2) < 0.5) _overlap = true;
        }
        // Count the number of b-tags
        if (!jet.bTags().empty()) _jet_ntag += 1;
      }

      // Remove events with object overlap
      if (_overlap) vetoEvent;

      int nGoodEle = good_elecs.size();
      int nGoodMu  = good_muons.size();

      if(nGoodEle+nGoodMu != _Nlept)
        vetoEvent;

      // Calculate and fill missing Et histos

      if(_Nlept != 0){

	lepton = good_leptons[0];

	foreach(const Particle& p, _neutrinos) p_miss+=p.momentum();

	_met_et = p_miss.Et()/GeV;

	_h_elec_N -> fill(_dressedelectrons.size(), weight);

	_h_mu_N   -> fill(_dressedmuons.size(),     weight);

      }

      // when single lepton event: MET coming from the leptonic side, used for reconstruction
      if(nGoodEle+nGoodMu == 1){

	_plep = lepton;

	// Evaluate basic event selection
	unsigned int ejets_bits = 0, mujets_bits = 0;
	bool pass_ejets  = _ejets(ejets_bits);
	bool pass_mujets = _mujets(mujets_bits);

	MTW = _transMass(good_leptons[0].momentum().pT()/GeV, good_leptons[0].phi(), p_miss.Et()/GeV, p_miss.phi());

	if(!pass_ejets && !pass_mujets)
	  vetoEvent;

        _h_MTW -> fill(MTW, weight);

      }

      if (nGoodEle > 0)
        {
          lepton=good_elecs[0].momentum();
          _h_elec_pT  -> fill(lepton.pT()/GeV, weight);
          _h_elec_eta -> fill(lepton.eta(),    weight);
	  _h_elec_phi -> fill(lepton.phi(),    weight);
	}
      if (nGoodMu > 0)
	{
	  lepton=good_muons[0].momentum();
	  _h_mu_pT  -> fill(lepton.pT()/GeV, weight);
	  _h_mu_eta -> fill(lepton.eta(),    weight);
	  _h_mu_phi -> fill(lepton.phi(),    weight);
	}

      int n25 = good_jets.size();

      _h_njet_25->fill(n25,weight);

      int nGoodJets = good_jets.size();
      int nBJets    = good_bjets.size();
      int nLJets    = good_ljets.size();

      if(good_jets.size() > 0){
	_h_jet_1_pT  -> fill(good_jets[0].momentum().pT()/GeV, weight);
      }

      for(int iJet = 0; iJet < nGoodJets; ++iJet){

	_h_jet_pT  -> fill(good_jets[iJet].momentum().pT()/GeV, weight);
        _h_jet_eta -> fill(good_jets[iJet].momentum().eta(),    weight);
	_h_jet_phi -> fill(good_jets[iJet].momentum().phi(),    weight);

      }

      for(int iJet = 0;iJet < nBJets; ++iJet){

        _h_bjet_pT  -> fill(good_bjets[iJet].momentum().pT()/GeV, weight);
        _h_bjet_eta -> fill(good_bjets[iJet].momentum().eta(),    weight);
	_h_bjet_phi -> fill(good_bjets[iJet].momentum().phi(),    weight);

      }

      for(int iJet = 0;iJet < nLJets; ++iJet){

        _h_ljet_pT  -> fill(good_ljets[iJet].momentum().pT()/GeV, weight);
        _h_ljet_eta -> fill(good_ljets[iJet].momentum().eta(),    weight);
        _h_ljet_phi -> fill(good_ljets[iJet].momentum().phi(),    weight);

      }

      _h_MET -> fill(_met_et, weight);
      _h_MTW -> fill(MTW,     weight);

    }
    


    
    /// Normalise histograms etc., after the run
    void finalize() {
      //double normfac=crossSection()/sumOfWeights();
      double normfac=1;

      std::cout << "NORMALIZE !!!!!! " << normfac << std::endl;

      scale(_h_jet_pT,     normfac);
      scale(_h_jet_eta,    normfac);
      scale(_h_jet_phi,    normfac);
      scale(_h_bjet_pT,    normfac);
      scale(_h_bjet_eta,   normfac);
      scale(_h_bjet_phi,   normfac);
      scale(_h_ljet_pT,    normfac);
      scale(_h_ljet_eta,   normfac);
      scale(_h_ljet_phi,   normfac);
      scale(_h_jet_1_pT,   normfac);
      scale(_h_bjet_1_pT,  normfac);
      scale(_h_njet_25,    normfac);
      scale(_h_elec_pT,    normfac);
      scale(_h_elec_eta,   normfac);
      scale(_h_elec_phi,   normfac);
      scale(_h_elec_N,     normfac);
      scale(_h_mu_pT,      normfac);
      scale(_h_mu_eta,     normfac);
      scale(_h_mu_phi,     normfac);
      scale(_h_mu_N,       normfac);
      scale(_h_MET,        normfac);
      scale(_h_MTW,        normfac);

    }


    //@}


  private:

    std::string Channel;

    Jets good_bjets, good_ljets, good_jets, good_cjets;

    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _vetodressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _vetodressedmuons;
    Particles _neutrinos;
    //  Jets _jets;
    unsigned int _jet_ntag;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi;

    double _pt_el, _eta_el, _pt_mu, _eta_mu, _eta_C, _pt_lep, _eta_lep;
    double _pt_isoT_mu, _R_isoT_mu, _pt_isoT_el, _R_isoT_el;
    double _pt_isoC_mu, _R_isoC_mu, _pt_isoC_el, _R_isoC_el;
    double _ETmiss, _ETmissMW;
    int    _Njets,_Ntags,_Nlept;
    double _pt_j, _eta_j, _R_j, _R_j_mu;
    double _MT, _MW, _dR_e_j, _dR_l_j;
    double _reco_sig_MTlep, _reco_sig_MThad, _reco_sig_MW;
    double MTW,_MTW;

    size_t       _b_thad, _b_tlep, _l_W1, _l_W2, _take;
    FourMomentum _plep, _pmiss;
    FourMomentum _pneus[2];
    ParticleVector _neu_all;

    double _transMass(double ptLep, double phiLep, double met, double phiMet) {
      return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
    }

    bool SelectGoodElectrons(vector<DressedLepton> &ele, ParticleVector &good_ele){

      good_ele.clear();

      foreach(DressedLepton electron, ele){

	if(electron.momentum().pT() < _pt_el)
	  continue;

	if(electron.momentum().eta() > _eta_el)
	  continue;

	good_ele.push_back(electron);

      }

      return true;

    }

    bool SelectGoodMuons(vector<DressedLepton> &mu, ParticleVector &good_mu){

      good_mu.clear();

      foreach(DressedLepton muon, mu){

        if(muon.momentum().pT() < _pt_mu)
          continue;

        if(muon.momentum().eta() > _eta_mu)
          continue;

	good_mu.push_back(muon);

      }

      return true;

    }

    bool SelectGoodJets(const Jets & alljets, Jets &bjets, Jets &ljets, Jets &all_good, Jets &cjets){

      bjets.clear();
      ljets.clear();
      cjets.clear();
      all_good.clear();

      foreach (Jet jet, alljets){
        if (jet.momentum().pT()<_pt_j || fabs(jet.momentum().eta())>_eta_j)
	  continue; // minimum pT and maximum eta jet cuts

	all_good.push_back(jet);

	if (jet.containsBottom()) bjets.push_back(jet);
	else ljets.push_back(jet);

	if (jet.containsCharm()) cjets.push_back(jet);

      }

      // cut on number of jets and bjets!

      int Ngood = all_good.size();
      int Nb    = bjets.size();

      if(Ngood < _Njets)
	return false;

      if(Nb   < _Ntags)
	return false;

      return true;

    }

    bool passMETcut(float met_val, float cut){

      if(met_val < cut)
	return false;

      return true;

    }


    // these functions are also stolen from the ttbar+jets routine above
    bool _ejets(unsigned int& cutBits) {
      // 1. More than zero good electrons
      cutBits += 1; if (_dressedelectrons.size() == 0) return false;
      // 2. No additional electrons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedelectrons.size() > 1) return false;
      // 3. No muons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedmuons.size() > 0) return false;
      // 4. total neutrino pT > 30 GeV
      cutBits += 1 << 3; if (_met_et <= _ETmiss*GeV) return false;
      // 5. MTW > 35 GeV
      cutBits += 1 << 4; if (MTW <= _MTW*GeV) return false;
      // 6. At least two b-tagged jets
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least four good jets
      cutBits += 1 << 6; if (good_jets.size() < 4) return false;
      cutBits += 1 << 7;
      return true;
    }

    bool _mujets(unsigned int& cutBits) {
      // 1. More than zero good muons
      cutBits += 1; if (_dressedmuons.size() == 0) return false;
      // 2. No additional muons passing the veto selection
      cutBits += 1 << 1; if (_vetodressedmuons.size() > 1) return false;
      // 3. No electrons passing the veto selection
      cutBits += 1 << 2; if (_vetodressedelectrons.size() > 0) return false;
      // 4. total neutrino pT > 30 GeV
      cutBits += 1 << 3; if (_met_et <= _ETmiss*GeV) return false;
      cutBits += 1 << 4; if (MTW <= _MTW*GeV) return false;
      // 6. At least two b-tagged jets
      cutBits += 1 << 5; if (_jet_ntag < 2) return false;
      // 7. At least four good jets
      cutBits += 1 << 6; if (good_jets.size() < 4) return false;
      cutBits += 1 << 7;
      return true;
    }

    /// @Name Histograms
    //@{
    Histo1DPtr  _h_jet_pT;
    Histo1DPtr  _h_jet_eta;
    Histo1DPtr  _h_jet_phi;
    Histo1DPtr  _h_bjet_pT;
    Histo1DPtr  _h_bjet_eta;
    Histo1DPtr  _h_bjet_phi;
    Histo1DPtr  _h_ljet_pT;
    Histo1DPtr  _h_ljet_eta;
    Histo1DPtr  _h_ljet_phi;
    Histo1DPtr  _h_jet_1_pT;
    Histo1DPtr  _h_bjet_1_pT;
    Histo1DPtr  _h_njet_25;
    Histo1DPtr  _h_elec_pT;
    Histo1DPtr  _h_elec_eta;
    Histo1DPtr  _h_elec_phi;
    Histo1DPtr  _h_elec_N;
    Histo1DPtr  _h_mu_pT;
    Histo1DPtr  _h_mu_eta;
    Histo1DPtr  _h_mu_phi;
    Histo1DPtr  _h_mu_N;
    Histo1DPtr  _h_MET;
    Histo1DPtr  _h_MTW;


  };



  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_TTbar_TestSel> plugin_MC_TTbar_TestSel;


}

