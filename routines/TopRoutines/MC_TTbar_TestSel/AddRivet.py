## job option script to run Rivet inside Athena

from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import glob
import os
import AthenaPoolCnvSvc.ReadAthenaPool

# change following line to use folder with your input file
svcMgr.EventSelector.InputCollections = glob.glob("/afs/cern.ch/work/s/sthenkel/public/forRIVETTutorial/*root*")

# limit number of events
from AthenaCommon.AppMgr import theApp
theApp.EvtMax = -1 #1000000

## Now set up Rivet
from Rivet_i.Rivet_iConf import Rivet_i
rivet=Rivet_i("Rivet")

# pick analysis
rivet.AnalysisPath = os.getcwd()
rivet.Analyses    += ["MC_TTbar_TestSel"] 
rivet.CrossSection = 1

rivet.HistoFile = "RIVET_Test_ttbar_Selection.yoda"

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
topSequence += rivet
